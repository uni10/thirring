string f_name(string typ, int D, double g, double m);
string tdvp_f_name(string typ, int D=1, double delg_init=1.0, double del_g=1.0, double m_init=1.0, double m=1.0, double t=1.0);

string f_name(string typ, int D, double g, double m){
	string s;
	stringstream ss(s);
	if(typ=="dir"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m;
	}
	if(typ=="data"){
		ss<<"%DATA_DIR/thirring_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/data";
	}
	if(typ=="A_L"){
		ss<<"%DATA_DIR/thirring_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/"<<"AL_D"<<D<<"g"<<g<<"m"<<m<<"tens";	
	}
	if(typ=="C"){
		ss<<"%DATA_DIR/thirring_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/"<<"C_D"<<D<<"g"<<g<<"m"<<m<<"tens";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/thirring_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/";
	}
	else{;}
return ss.str();
}

string tdvp_f_name(string typ, int D, double delg_init, double del_g, double m_init, double m, double t){
	string s;
	stringstream ss(s);
	if(Dy_bond_bool==true){
	if(typ=="dir0"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond";
	}
	if(typ=="dir1"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m;
	}
	if(typ=="dir2"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved";
	}
	if(typ=="dir3"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/Fermion_cor";
	}
	if(typ=="dir_tau"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau;
	}
	if(typ=="A"){ 
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved"<<"/Dy_bond_del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_t"<<t<<"A_tens";
	}
	if(typ=="A_tau"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau
		<<"/Dy_bond_del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_tau"<<tau<<"A_tens";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	if(typ=="others_tau"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	else{;}
	}
	else{
	if(typ=="dir0"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond";
	}

	if(typ=="dir1"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m;
	}
	if(typ=="dir2"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved";
	}
	if(typ=="dir3"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/Fermion_cor";
	}
	if(typ=="dir_tau"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau;
	}
	if(typ=="A"){ 
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved"<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_t"<<t<<"A_tens";
	}
	if(typ=="A_tau"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau
		<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_tau"<<tau<<"A_tens";
	}
	if(typ=="data"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/t"<<t
 		<<"/data";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	if(typ=="others_tau"){
		ss<<"%DATA_DIR/thirring_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	else{;}

	}

return ss.str();
}

