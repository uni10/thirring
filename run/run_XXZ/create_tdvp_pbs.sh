My_path=`pwd | sed 's/\/run_XXZ//' | sed 's/\/run//'  `
TDVP_XXZ_path=TDVP/XXZ_TDVP/real_Thir

Dy_bond_bool=`grep '^Dy_bond_bool:' para_set | sed 's/^.*://'`
D_max=`grep '^D_max:' para_set | sed 's/^.*://'`

save_step=`grep '^im_save_step:' para_set | sed 's/^.*://'`
del_t=`grep '^im_del_t:' para_set | sed 's/^.*://'`
im_length=`grep '^im_length:' para_set | sed 's/^.*://'`

tau=`grep '^tau:' para_set | sed 's/^.*://'`
tau=$(echo "scale=1; $tau")
tau=$(echo "scale=1; ${tau}/1" |bc -l |sed -e 's/^\./0./' | sed -e 's/^-\./-0./' |sed -e 's/\.0//' )
tau_init=$tau
bond=`grep '^bond:' para_set | sed 's/^.*://'`

delta_start_list=`grep '^delta_start_list:' para_set | sed 's/^.*://'`
delta_end_list=`grep '^delta_end_list:' para_set | sed 's/^.*://'`
m_start_list=`grep '^m_start_list:' para_set | sed 's/^.*://'`
m_end_list=`grep '^m_end_list:' para_set | sed 's/^.*://'`

queue=`grep '^tdvp_queue:' para_set | sed 's/^.*://'`
ppn_num=`grep '^tdvp_ppn_num:' para_set | sed 's/^.*://'`

if [ $Dy_bond_bool == 'true' ] || [ $Dy_bond_bool == '1' ]; then
	Bond_str1="Dy_bond/Dmax"$D_max""
	Bond_str2=""
else
	Bond_str1="Fix_bond/BondDim$bond"
	Bond_str2="D${bond}_"
fi

function dir_create_func(){
dir_path="./run_dir/tdvp_run_dir/$Bond_str1/quench_"${Bond_str2}"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end""
	if ! test -e ./run_dir/tdvp_run_dir/Dy_bond; then
		mkdir ./run_dir/tdvp_run_dir/Dy_bond
	fi
	if ! test -e ./run_dir/tdvp_run_dir/Fix_bond; then
		mkdir ./run_dir/tdvp_run_dir/Fix_bond
	fi
	if ! test -e ./run_dir/tdvp_run_dir/${Bond_str1}; then
		mkdir ./run_dir/tdvp_run_dir/${Bond_str1}
	fi
	if ! test -e ./log_dir/tdvp_log/Dy_bond; then
		mkdir ./log_dir/tdvp_log/Dy_bond
	fi
	if ! test -e ./log_dir/tdvp_log/Fix_bond; then
		mkdir ./log_dir/tdvp_log/Fix_bond
	fi
	if ! test -e ./log_dir/tdvp_log/$Bond_str1; then
		mkdir ./log_dir/tdvp_log/$Bond_str1
	fi
	if ! test -e ./log_dir/tdvp_log/$Bond_str1/TDVP_m"${m_start}"to"${m_end}"_del"${delta_start}"to"${delta_end}"; then
		mkdir ./log_dir/tdvp_log/$Bond_str1/TDVP_m"${m_start}"to"${m_end}"_del"${delta_start}"to"${delta_end}"
	fi
	if ! test -e $dir_path; then
		mkdir $dir_path
		cp -r $My_path/$TDVP_XXZ_path/Diagrams $dir_path
	fi
}

printf "\n\n"
echo "======================Create corresponding pbs files for TDVP algorithm ========================="
printf "\n\n"


for m_start  in ${m_start_list}
do
for m_end  in ${m_end_list}
do
for delta_start in ${delta_start_list}
do
for delta_end in ${delta_end_list}
do
	dir_create_func
	echo "Create imaginary time pbs files."
	cat ./run_dir/tdvp_run_dir/submit_template.pbs |sed 's+%My_path+'"${My_path}"'+g' | sed 's/%queue/'"$queue"'/' \
	| sed 's/%Bond_str2/'"$Bond_str2"'/g' | sed 's+%Bond_str+'"$Bond_str1"'+' | sed 's/%m_i/'"$m_start"'/g' \
	| sed 's/%m_f/'"$m_end"'/g' | sed 's/%del_i/'"$delta_start"'/g' | sed 's/%del_f/'"$delta_end"'/g' \
	| sed 's/tau%tau/''im''/g' | sed 's/%ppn_num/'"$ppn_num"'/'\
	> $dir_path/"$Bond_str2"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"im.pbs
	cat ./run_dir/tdvp_run_dir/submit_template.pbs |sed 's+%My_path+'"${My_path}"'+g'  | sed 's/%queue/'"$queue"'/' \
	| sed 's/%Bond_str2/'"$Bond_str2"'/g' | sed 's+%Bond_str+'"$Bond_str1"'+' | sed 's/%m_i/'"$m_start"'/g' \
	| sed 's/%m_f/'"$m_end"'/g' | sed 's/%del_i/'"$delta_start"'/g' | sed 's/%del_f/'"$delta_end"'/g' \
	| sed 's/tau%tau/''minus_im''/g' | sed 's/%ppn_num/'"$ppn_num"'/'\
	> $dir_path/"$Bond_str2"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"minus_im.pbs
	for ((i=0; i<$im_length; i=i+1))
	do
		echo "Create real time evolution .pbs file for tau=$tau".
		cat ./run_dir/tdvp_run_dir/submit_template.pbs | sed 's+%My_path+'"${My_path}"'+g' | sed 's/%queue/'"$queue"'/' \
		| sed 's/%Bond_str2/'"$Bond_str2"'/g' | sed 's+%Bond_str+'"$Bond_str1"'+' | sed 's/%m_i/'"$m_start"'/g' \
		| sed 's/%m_f/'"$m_end"'/g' | sed 's/%del_i/'"$delta_start"'/g' | sed 's/%del_f/'"$delta_end"'/g' \
		| sed 's/%tau/'"$tau"'/g' | sed 's/%ppn_num/'"$ppn_num"'/'\
		> $dir_path/"$Bond_str2"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"tau"$tau".pbs
		tau=$(echo "scale=1; ${tau}+${save_step}*${del_t}/1" |bc -l |sed -e 's/^\./0./' | sed -e 's/^-\./-0./' | sed -e 's/\.0//' )
	done
	tau=$tau_init
done
done
done
done
