double S_meas(UniTensor l, double& Sf);
double Sz_meas(UniTensor& A, UniTensor& l, UniTensor& r);
double Sx_meas(UniTensor& A, UniTensor& l, UniTensor& r);
Matrix Chi_cond_mat();
double single_meas(UniTensor A, Matrix OP_mat, UniTensor l, UniTensor r);
double T_meas(UniTensor A2TC, UniTensor A1, UniTensor l);
vector<double> Ts_meas(UniTensor A2TC, UniTensor A1, UniTensor l, UniTensor r);

vector<UniTensor> bkt_hs(double g, int n);
void BKT_cor(UniTensor A, UniTensor l, UniTensor r, vector<double>& BKT_cors);
void BKT_cor_tebd(int n, int N, double del_g, UniTensor A, UniTensor l, UniTensor r, vector<double>& BKT_cors);
void bkt_update(UniTensor& upA, UniTensor& upB, UniTensor h, double& trun_err);
void BKT_cors_save(int i, vector<double>& BKT_cors, ofstream& output_BKT, ofstream& output_lnBKT);
Matrix M_exp(double tau, Matrix M);
Matrix M_comp_exp(complex<double> tau, Matrix M);

void BKT_cor_tebd(int n, int N, double del_g, UniTensor A, UniTensor l, UniTensor r, vector<double>& BKT_cors){
	vector<UniTensor> As;
	vector<UniTensor> As_old;
	vector<UniTensor> hs=bkt_hs(del_g, n);
	double trun_err=0.0;
	for(int i=0; i<N; i++) As.push_back(A);
	As_old=As;

	for(int N_sub=2; N_sub<=N; N_sub+=2){
		for(int i=0; i<n ;i++){
			bkt_update(As.at(0), As.at(1), hs.at(0), trun_err); //cout<<"even0"<<endl;
			for(int ri=2; ri<N_sub/2; ri=ri+2){bkt_update(As.at(ri), As.at(ri+1), hs.at(1), trun_err);}// cout<<"even"<<ri<<endl;}
			for(int ri=1; ri<N_sub/2; ri=ri+2){bkt_update(As.at(ri), As.at(ri+1), hs.at(1), trun_err); }//cout<<"odd"<<ri<<endl;}
		}
		cout<<"r= "<<N_sub<<flush;printf("\r"," ");
		UniTensor ATC=A; ATC.cTranspose(CTYPE);
		int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
		UniTensor cor_tens=l;
		int labels_l[]={1, 3}, labels_r[]={2, 4};
		for(int i=0; i<N_sub; i++){
			cor_tens=contract(As.at(i), cor_tens);
			cor_tens=contract(cor_tens, ATC);
			cor_tens.setLabel(labels_l);
		}
		cor_tens.setLabel(labels_r); cor_tens=contract(cor_tens, r); 
		double cor_v=(real(cor_tens.at(CTYPE, 0))); 
		BKT_cors.push_back(cor_v);
		As=As_old;
	};
return;
}

void bkt_update(UniTensor& upA, UniTensor& upB, UniTensor h, double& trun_err){
	int labels_A[]={0, 1, 2}; int labels_upA[]={0, 1, 2};
	int labels_upB[]={-1, 2, 3}; int labels_h[]={0, -1, -2, -3};
	int per[]={1, -2, 3, -3};
	int d=upA.bond(0).dim(), D=upA.bond(1).dim();
	upB.setLabel(labels_upB); h.setLabel(labels_h);
	UniTensor Theta=contract(upA, upB); Theta=contract(Theta, h);
	Theta.permute(per, 2);
	vector<Matrix> SVDs=Theta.getBlock().svd();
	//SVDs.at(1)*=(1.0)/SVDs.at(1).norm();
	double trun_err_cur=0.0;
	for(int i=0; i<SVDs.at(1).row(); i++){trun_err_cur+=(pow(real(SVDs.at(1).at(CTYPE, i)),2));};

	SVDs[0].resize(D*d, D);
	SVDs[1].resize(D, D);
	SVDs[2].resize(D, D*d);

	for(int i=0; i<SVDs.at(1).row(); i++){trun_err_cur-=(pow(real(SVDs.at(1).at(CTYPE, i)),2));};
	trun_err= trun_err_cur>trun_err ? trun_err_cur : trun_err;
	
	//SVDs.at(1)*=(1.0)/SVDs.at(1).norm();


	UniTensor U(CTYPE, upA.bond()), V(CTYPE, upA.bond());
	int per_U[]={1, 0, 2}; int per_V[]={1, 2, 0};
	U.permute(per_U, 2); V.permute(per_V, 1);
	U.putBlock(SVDs.at(0)); V.putBlock(SVDs.at(1)*SVDs.at(2));
	U.permute(labels_A, 1); V.permute(labels_A, 1);
	U.setLabel(labels_A); V.setLabel(labels_A);
	upA=U; upB=V;	
return;	
}

void BKT_cor(UniTensor A, UniTensor l, UniTensor r, vector<double>& BKT_cors){
	int d=A.bond(0).dim(), D=A.bond(1).dim();
	double tau=(-2.0)*acos(-0.78);
	Bond bdi(BD_IN, d), bdo(BD_OUT, d);
	vector<Bond> bonds={bdi, bdi, bdo, bdo};
	UniTensor h(CTYPE, bonds);
	bonds.clear();
	Matrix M(d, d); M.set_zero();
	M.at(1, 2)=1.0; M.at(2, 1)=(-1.0);
	bonds={bdi, bdo};
	RtoC(M);
	UniTensor T(bonds); T.putBlock(M);
	bonds.clear();
	Bond bdi2(BD_IN, 2), bdo2(BD_OUT, 2);
	bonds={bdi2, bdo2};
	UniTensor I(bonds); I.identity();
	bonds.clear();
	M=otimes(otimes(I, T), I).getBlock()+otimes(otimes(I, I), T).getBlock();
	RtoC(M);
	M=M_exp(tau, M); 
	h.putBlock(M); 

	double bkt_v=-1.0;
	UniTensor scal_tens;
	UniTensor ATC=A; ATC.cTranspose(CTYPE);
	int labels_A[]={0, 1, 2};
	int labels_ATC2[]={3, 4, -1};
	int labels_ATC3[]={1, 4, 0};
	ATC.setLabel(labels_ATC2);

	UniTensor h1(CTYPE, T.bond());
	h1.putBlock(M_exp(tau, T.getBlock()));
	int labels_OP1[]={0, -1}; h1.setLabel(labels_OP1);
	UniTensor A_meas=contract(A, h1); A_meas=contract(A_meas, l);
	int per_Ameas[]={-1, 3, 2}; A_meas.permute(per_Ameas, 1);
	scal_tens=contract(A_meas, ATC); 
	scal_tens=contract(scal_tens, r);
	bkt_v=real(scal_tens.at(CTYPE, 0));
	BKT_cors.push_back(bkt_v);
	
	Network BKT_net("./Diagrams/BKT.net");
	ATC.setLabel(labels_ATC3);
	BKT_net.putTensor("A", A); BKT_net.putTensor("h", h); BKT_net.putTensor("ATC", ATC);
	for (int i=0; i<100; i++){
		BKT_net.putTensor("A_meas", A_meas); 
		A_meas=BKT_net.launch(); A_meas.setLabel(labels_A);
		scal_tens=contract(A_meas, ATC); scal_tens=contract(scal_tens, r);
		bkt_v=real(scal_tens.at(CTYPE, 0));
		BKT_cors.push_back(bkt_v);
	}
	

return;
}

void BKT_cors_save(int i, vector<double>& BKT_cors, ofstream& output_BKT, ofstream& output_lnBKT){
	string s; stringstream ss(s); ss<<"x"<<i*0.1;
	output_BKT<<ss.str()<<"=("; output_lnBKT<<ss.str()<<"=(";
	for(int r=0; r<BKT_cors.size(); r++){
		output_BKT<<setprecision(15)<<BKT_cors[r]<<", ";
		output_lnBKT<<setprecision(15)<<log(BKT_cors[r])<<", ";
	}
	output_BKT<<")"<<endl<<endl; output_lnBKT<<")"<<endl<<endl;
	BKT_cors.clear();
return;
}

Matrix M_exp(double tau, Matrix M){
	vector<Matrix> Eigs;
	Eigs=M.eig();
	Matrix U_inv=Eigs.at(1).inverse();
	for(int i=0; i<M.col(); i++)
		Eigs.at(0).at(CTYPE, i)=exp(tau*Eigs.at(0).at(CTYPE, i));
	Matrix expM=U_inv*Eigs.at(0)*Eigs.at(1);
return expM;
}

Matrix M_comp_exp(complex<double> tau, Matrix M){
	vector<Matrix> Eigs;
	Eigs=M.eig();
	Matrix U_inv=Eigs.at(1).inverse();
	for(int i=0; i<M.col(); i++)
		Eigs.at(0).at(CTYPE, i)=exp(tau*Eigs.at(0).at(CTYPE, i));
	Matrix expM=U_inv*Eigs.at(0)*Eigs.at(1);
return expM;
}

double T_meas(UniTensor A2TC, UniTensor A1, UniTensor l){
	double T_val;
	complex<double> eigval=complex<double>(1.0, 0);
	L_Arn_Tens_meth("N", A1, A2TC, l, eigval);
	T_val=(-0.5)*log(norm(eigval));
return T_val;
}

vector<double> Ts_meas(UniTensor A2TC, UniTensor A1, UniTensor l, UniTensor r){
	vector<double> Ts;
	double T_val=2.0;
	vector<complex<double>> eigval_s;
	vector<UniTensor> ls, rs; 
	ls.push_back(l);  
	rs.push_back(r); 
	LR_Arn("N", T_num, A1, A2TC, ls, rs, eigval_s);
	
	for(int i=0; i<T_num; i++){
		T_val=(-0.5)*log(norm(eigval_s.at(i))); Ts.push_back(T_val);
	}
return Ts;
}

double S_meas(UniTensor l, double& Sf){
	double s=0.0;
	int D=l.bond(0).dim();
	Matrix S=l.getBlock();
	vector<Matrix> SVDs;
	SVDs=S.svd();
	S=SVDs.at(1);
	S*=(1.0/S.norm());
	for(int i=0; i<D; i++)s-=(pow(real(S.at(CTYPE,i)),2.0)*2.0*log(real(S.at(CTYPE,i))));
	Sf=pow(norm(S.at(CTYPE, D-1)), 0.5);
return s;
}

double single_meas(UniTensor A, Matrix OP_mat, UniTensor l, UniTensor r){
	int d=A.bond(0).dim(); int D=A.bond(1).dim();
	double meas_val=1.0;
	int labels_A[]={0, 1, 2}; int labels_ATC[]={3, 4, -1};
	int labels_OP[]={0, -1};
	A.setLabel(labels_A);
	vector<Bond> bonds={A.bond(0), A.bond(0)};
	UniTensor OP(bonds);
	OP.permute(1);
	OP.putBlock(OP_mat); RtoC(OP);
	OP.setLabel(labels_OP);
	UniTensor ATC=A; ATC.cTranspose(CTYPE);
	ATC.setLabel(labels_ATC);
	UniTensor OP_Tens=contract(A, OP);
	OP_Tens=contract(OP_Tens, l);
	OP_Tens=contract(OP_Tens, r);
	OP_Tens=contract(OP_Tens, ATC);
	meas_val=0.5*real(OP_Tens.at(CTYPE, 0));
return meas_val;
}

Matrix Chi_cond_mat(){
	double elem[]={0.0, 1.0, -1.0, 0.0};
	Matrix Cc_mat(4, 4, true);
	Cc_mat.setElem(elem);
return Cc_mat;
}

Matrix Szt_mat(){
	double elem[]={1.0, 0.0, 0.0, -1.0};
	Matrix Szt_mat(4, 4, true);
	Szt_mat.setElem(elem);
return Szt_mat;
}
	
Matrix Sxt_mat(){
	double elem[]={0, 0.5, 0.5, 0, 0.5, 0, 0, 0.5, 0.5, 0, 0, 0.5, 0, 0.5, 0.5, 0};
	Matrix Sxt_mat(4, 4, false);
	Sxt_mat.setElem(elem);
return Sxt_mat;
}

vector<UniTensor> bkt_hs(double g, int n){
	int d=4;
	double pi=3.14159265358979;
        double tau=(double)((2*pi-2.0*acos(g))/n);
	Bond bdi(BD_IN, d), bdo(BD_OUT, d);
	vector<Bond> bonds={bdi, bdi, bdo, bdo};
	UniTensor h(CTYPE, bonds);
	bonds.clear();

	vector<UniTensor> hs={h, h};
	Matrix M(d, d); M.set_zero();
	M.at(1, 2)=1.0; M.at(2, 1)=(-1.0);
	RtoC(M);
	bonds={bdi, bdo};
	UniTensor T(bonds); T.putBlock(M);
	bonds.clear();

	Bond bdi2(BD_IN, 2), bdo2(BD_OUT, 2);
	bonds={bdi2, bdo2};
	UniTensor I(bonds); I.identity();
	bonds.clear();
	M=otimes(otimes(T, I), I).getBlock()+otimes(otimes(I, T), I).getBlock()+otimes(otimes(I, I), T).getBlock();
	RtoC(M); M=M_exp(tau, M); 
	hs.at(0).putBlock(M);
	M=otimes(otimes(I, T), I).getBlock()+otimes(otimes(I, I), T).getBlock();
	RtoC(M); M=M_exp(tau, M); 
	h.putBlock(M); 
	hs.at(1).putBlock(M);
return hs;
}

void ff_cor(int N, UniTensor A, UniTensor l, UniTensor r,  vector<double>& ff_cors){
	double val; double pi=3.14159265358979;
	int d=A.bond(0).dim(), D=A.bond(1).dim();
	UniTensor ATC=A; ATC.cTranspose(CTYPE);
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);

	Bond bdi(BD_IN, 2) , bdo(BD_OUT, 2);
	vector<Bond>bonds={bdi, bdo};
	UniTensor Sp(CTYPE, bonds), Sm(CTYPE, bonds), eSz(CTYPE, bonds);
	bonds.clear();
	Matrix Sp_mat(CTYPE, 2, 2), Sm_mat(CTYPE, 2, 2), eSz_mat(CTYPE, 2, 2);
	Sp_mat.set_zero(); Sm_mat.set_zero(); eSz_mat.set_zero();
	Sp_mat.at(CTYPE, 1)=1.0; Sm_mat.at(CTYPE, 2)=1.0;
	Sp.putBlock(Sp_mat); Sm.putBlock(Sm_mat);
	eSz_mat.at(CTYPE, 0)=1.0; eSz_mat.at(CTYPE, 3)=-1.0;
	eSz_mat=M_comp_exp((-1.0)*image*pi, eSz_mat);
	eSz.putBlock(eSz_mat);

	Bond bdpi(BD_IN, d) , bdpo(BD_OUT, d);
	bonds={bdpi, bdpo};
	UniTensor SpSm(CTYPE, bonds);
	bonds.clear();
	SpSm.putBlock(otimes(Sp, Sm).getBlock());

	int labels_ATC2[]={3, 4, -1}; ATC.setLabel(labels_ATC2);
	int labels_MPO[]={0, -1}; SpSm.setLabel(labels_MPO);
	UniTensor val_tens=contract(A, SpSm); val_tens=contract(val_tens, l);
	val_tens=contract(val_tens, ATC);val_tens=contract(val_tens, r);
	val=real(val_tens.at(CTYPE, 0));
	ff_cors.push_back(val);

	UniTensor Sp_eSz(CTYPE, SpSm.bond());
	Sp_eSz.putBlock(otimes(Sp, eSz).getBlock());
	Sp_eSz.setLabel(labels_MPO);
	UniTensor eSz_Sm(CTYPE, SpSm.bond());
	eSz_Sm.putBlock(otimes(eSz, Sm).getBlock());
	eSz_Sm.setLabel(labels_MPO);
	UniTensor eSz_eSz(CTYPE, SpSm.bond());
	eSz_eSz.putBlock(otimes(eSz, eSz).getBlock());
	eSz_eSz.setLabel(labels_MPO);

	int labels_l[]={1, 3}; int labels_r[]={2, 4};
	UniTensor val_temp=contract(A, l);
	val_temp=contract(val_temp, Sp_eSz);
	val_temp=contract(val_temp, ATC);
	val_temp.permute(labels_r, 1); val_temp.setLabel(labels_l);
	UniTensor r_edge=contract(A, r);
	r_edge=contract(r_edge, eSz_Sm);
	r_edge=contract(r_edge, ATC);

	val_tens=contract(val_temp, r_edge);
	val=real(val_tens.at(CTYPE, 0));
	ff_cors.push_back(val);

	for(int i=2; i<(N+1)/2; i++){
		val_temp=contract(A, val_temp); val_temp=contract(val_temp, eSz_eSz); val_temp=contract(val_temp, ATC);
		val_temp.permute(labels_r, 1); val_temp.setLabel(labels_l);
		val_tens=contract(val_temp, r_edge);
		val=real(val_tens.at(CTYPE, 0));
		ff_cors.push_back(val);
	}
return;
}

