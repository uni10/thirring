UniTensor MPO_Thir(double a, double lambda, double g_sq, double m, double mu, double S_tar);
Matrix getMs_sub(Matrix MPO_mat, int col, int row, int num);
void MPO_sub(UniTensor& MPO, vector<UniTensor>& M_subs);
void conne(UniTensor& E, UniTensor A);
UniTensor connect(string Side, UniTensor E, UniTensor A, UniTensor O);
void eigcal_sub(UniTensor A, vector<UniTensor> Ms, UniTensor& BL, UniTensor& BR, UniTensor l, UniTensor r, double& e0);
UniTensor X_MPO_make(UniTensor A, UniTensor MPO, vector<UniTensor> Ms, UniTensor l, UniTensor r, UniTensor l_root_inv, UniTensor r_root_inv , UniTensor VLTC, double& e0);
UniTensor B_MPO_make(bool sym_GF, UniTensor& A, UniTensor& MPO, vector<UniTensor>& Ms, Matrix& l_mat, Matrix& r_mat, double& e0);

Matrix Thir_sub_mat(int n, double a, double lambda, double g, double m, double mu, double S_tar){
	double beta_n=(g/a)+(pow(-1.0,n)*m+mu)-2.0*lambda*S_tar; 
	double gamma=lambda*(0.25)+0.25*(g/a)+0.5*mu; 
	double kronecker_delta = (n%2)==0? 1:1;
	double elem[]={
1,      0,      	0,      0,      	0,      0,     		0,      0,      	0,      0,      	0,      0,
0,      1,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,
1,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0,      1,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,
0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0.5,    0,      	0,      0,      	0,      0,      	1,      0,      	0,      0,      	0,      0,
0,   -0.5,      	0,      0,      	0,      0,      	0,      1,      	0,      0,      	0,      0,

0.5,	0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,
0,   -0.5,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0.5*beta_n+gamma,   0,  0,   (-1/a)*0.5, 	0,      0,      	lambda,      0,    	0.5*(g/a)*kronecker_delta, 0,1,      0,
0,(-0.5)*beta_n+gamma,	0,      0,	(-1/a)*0.5,     0,		0, (-1)*lambda,		0,  (-0.5)*(g/a)*kronecker_delta,0,      1
};
	Matrix Thir_mat(12, 12);
	Thir_mat.setElem(elem);
return Thir_mat;
}

void MPO_sub(UniTensor& MPO, vector<UniTensor>& M_subs){
	Matrix MPO_mat=MPO.getBlock();
	int num=4;
	int labels_loc[]={0, -1};
	Bond bdi(BD_IN, num); Bond bdo(BD_OUT, num);
	vector<Bond> bonds;
	bonds.push_back(bdi); bonds.push_back(bdo);
	UniTensor T(bonds);
	bonds.clear();
	T.setLabel(labels_loc);
	T.putBlock(getMs_sub(MPO_mat, 0, 0, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 1, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 2, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 3, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 4, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 1, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 2, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 3, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 4, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 5, num)); M_subs.push_back(T);
	RtoC(T);
return;
}

Matrix getMs_sub(Matrix MPO_mat, int col, int row, int num){
	Matrix M(num, num);
	for(int i=0; i<num; i++){for(int j=0; j<num; j++){
		M.at(i,j)=MPO_mat.at(row*num+i,col*num+j);
	}}
return M;
}


UniTensor MPO_Thir(double a, double lambda, double g_sq, double m, double mu, double S_tar){
	Matrix I12(12,12); I12.identity();
	Matrix Thir_sub_mat1=Thir_sub_mat(1, a, lambda, g_sq, m, mu, S_tar);
	Matrix Thir_sub_mat2=Thir_sub_mat(2, a, lambda, g_sq, m, mu, S_tar);
	Bond bdiv(BD_IN, 6); Bond bdip(BD_IN, 2); Bond bdov(BD_OUT, 6); Bond bdop(BD_OUT, 2);
	vector<Bond> bonds;
	bonds.push_back(bdiv); bonds.push_back(bdip); bonds.push_back(bdov); bonds.push_back(bdop);
	UniTensor T1(bonds); T1.putBlock(Thir_sub_mat1);
	UniTensor T2(bonds); T2.putBlock(Thir_sub_mat2);
	bonds.clear();
	int labels_or[]={0, 1, 2, 3};
	int labels_T2[]={2, -1, -2, -3};
	T2.setLabel(labels_T2);
	UniTensor T=contract(T1, T2);
	vector<int> T_comb_label1={1, -1};
	vector<int> T_comb_label2={3, -3};
	T.combineBond(T_comb_label1); T.combineBond(T_comb_label2);
	int per[]={0, 1, -2, 3};
	T.permute(per, 2); T.setLabel(labels_or); 
return T;
}
/*
void eigcal_sub(UniTensor A, vector<UniTensor> Ms, UniTensor& BL, UniTensor& BR, UniTensor l, UniTensor r, double& e0){
	int d=A.bond(0).dim(); int D=A.bond(1).dim();
	int M=6;
	int labels_ATC[]={3, 4, 0};
	int labels_a[]={0, 1}; int labels_l[]={1, 3}; int labels_r[]={2, 4};
	int labels_locMPO[]={0, -1}; 
	UniTensor ATC=A; ATC.cTranspose(CTYPE); 
	vector<UniTensor> Es;
	UniTensor E=l; Es.push_back(E); 
	Bond bdpi_sub(BD_IN, 2); Bond bdpo_sub(BD_OUT, 2);
	
	Es.push_back(connect("L", Es.at(0), A, Ms.at(8)));
	
	UniTensor C1; C1=connect("L", Es.at(0), A, Ms.at(7)); C1.permute(1);
	C1.setLabel(labels_a);
	ATC.setLabel(labels_ATC);
	Matrix mat=l.getBlock();
	E=BOP_make_BiCGSTAB(mat, A, ATC, C1, l, r);	
	Es.push_back(E);

	Es.push_back(connect("L", Es.at(0), A, Ms.at(6)));
	Es.push_back(connect("L", Es.at(0), A, Ms.at(5)));

	C1=connect("L", Es.at(4), A, Ms.at(1))+connect("L", Es.at(3), A, Ms.at(2))+connect("L", Es.at(2), A, Ms.at(3))+
	connect("L", Es.at(1), A, Ms.at(4))+connect("L", Es.at(0), A, Ms.at(9));
	C1.permute(1); C1.setLabel(labels_a);
	
	E=BOP_make_BiCGSTAB(mat, A, ATC, C1, l, r);
	Es.push_back(E); 
	
	vector<UniTensor> Fs; 
	UniTensor F=r;
	Fs.push_back(F);
	Fs.push_back(connect("R", Fs.at(0), A, Ms.at(1)));
	Fs.push_back(connect("R", Fs.at(0), A, Ms.at(2)));

	UniTensor C2;	
	C2=connect("R", Fs.at(0), A, Ms.at(3));
	C2.permute(1); C2.setLabel(labels_a);
	F=BOPT_make_BiCGSTAB(mat, A, ATC, C2, l, r);  
	Fs.push_back(F);

	Fs.push_back(connect("R", Fs.at(0), A, Ms.at(4)));

	C2=connect("R", Fs.at(0), A, Ms.at(9))+connect("R", Fs.at(1), A, Ms.at(5))+connect("R", Fs.at(2), A, Ms.at(6))+
	connect("R", Fs.at(3), A, Ms.at(7))+connect("R", Fs.at(4), A, Ms.at(8));
	C2.permute(1); C2.setLabel(labels_a);
	F=BOPT_make_BiCGSTAB(mat, A, ATC, C2, l, r);  
	Fs.push_back(F);

	r.setLabel(labels_a); l.setLabel(labels_a);
	//e0=0.5*(contract(C1, r).at(CTYPE, 0).real());
	//cout<<setprecision(15)<<e0<<endl;
	e0=0.5*(contract(C2, l).at(CTYPE, 0).real());
	//cout<<setprecision(15)<<e0<<endl;
	l.setLabel(labels_l); r.setLabel(labels_r);

	Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
	Bond bdm(BD_IN, M);
	vector<Bond> bonds; 
	bonds.push_back(bdm); bonds.push_back(bdi); bonds.push_back(bdo);
	UniTensor Bl_temp(CTYPE, bonds); 
	bonds.clear();

	BL=Bl_temp;
	complex<double> *elem_t;
	elem_t=(complex<double>*)malloc(6*(D*D)*sizeof(complex<double>));
	for(int i=0; i<M; i++){
		memcpy(elem_t+i*(D*D), Es.at(M-1-i).getElem(CTYPE), (D*D)*sizeof(complex<double>));
	}
	Matrix E_mat(CTYPE, M*D, D); E_mat.setElem(elem_t);
	free(elem_t); 
	BL.putBlock(E_mat);
	int per_BL[]={1, 0, 2}; BL.permute(per_BL, 0);
	int labels_BL[]={1, -2, 3}; BL.setLabel(labels_BL);
 	
	BR=Bl_temp;
	elem_t=(complex<double>*)malloc(6*(D*D)*sizeof(complex<double>));
	for(int i=0; i<M; i++){
		memcpy(elem_t+i*(D*D), Fs.at(i).getElem(CTYPE), (D*D)*sizeof(complex<double>));
	}
	Matrix F_mat(CTYPE, M*D, D); F_mat.setElem(elem_t);
	free(elem_t); 
	BR.putBlock(F_mat);
	int per_BR[]={1, 0, 2}; BR.permute(per_BL, 3);
	int labels_BR[]={2, -3, 4}; BR.setLabel(labels_BR);
	
}

UniTensor connect(string Side, UniTensor E, UniTensor A, UniTensor O){
	int labels_l[]={1, 3}; int labels_r[]={2, 4};
	UniTensor ATC=A; ATC.cTranspose(CTYPE);
	int labels_ATC2[]={3, 4, -1}; ATC.setLabel(labels_ATC2);
	UniTensor EOA;
	if(Side == "L"){
		E.setLabel(labels_l);
		EOA=contract(E, A); EOA=contract(EOA, O); EOA=contract(EOA, ATC);
		int per[]={2, 4}; EOA.permute(per, 1); 
	}
	else if(Side == "R"){
		E.setLabel(labels_r);
		EOA=contract(E, A); EOA=contract(EOA, O); EOA=contract(EOA, ATC);
		int per[]={1, 3}; EOA.permute(per, 1); 
	}
	int labels_a[]={0, 1}; EOA.setLabel(labels_a);
return EOA;
}
*/

UniTensor B_MPO_make(bool sym_GF, UniTensor& A, UniTensor& MPO, vector<UniTensor>& Ms, UniTensor& l, UniTensor& r, Matrix& l_mat, Matrix& r_mat, double& e0){
	e0=1.0;
	int D=A.bond(1).dim(); int d=A.bond(0).dim();
	Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
	int labels_l[]={1, 3}; int labels_r[]={2, 4};
	int labels_a[]={0, 1}; 
	l.setLabel(labels_l); r.setLabel(labels_r);
	UniTensor ATC=A; ATC.cTranspose(CTYPE);
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
        double normal=1.0;
	UniTensor l_root, r_root, l_root_inv, r_root_inv, l_GF, r_GF;
	if(sym_GF==true){
		sym_gauge(A, l, r, l_mat, r_mat, l_root, l_root_inv);
		r_root_inv=l_root_inv; r_root=l_root;
		l_GF=l, r_GF=r;
	}
	else{		
		l.putBlock(l_mat); r.putBlock(r_mat);
		complex<double> c =complex<double>(1.0, 0);
		L_Arn_Tens_meth("R", A, ATC, l, c);
		l_mat=l.getBlock();
		normal=real(c);
		A *= pow(real(c), -0.5);
		ATC *= pow(real(c), -0.5);
		R_Arn_Tens_meth("R", A, ATC, r, c); 
		r_mat=r.getBlock();
		UniTensor r_te=r; r_te.setLabel(labels_l);
		c=contract(l, r_te).at(CTYPE, 0);
		r *=(1.0/(real(c))); 
		l_root_inv=root_make(l); l_root=l_root_inv; l_root_inv.putBlock(l_root.getBlock().inverse());
		r_root_inv=root_make(r); r_root=r_root_inv; r_root_inv.putBlock(r_root.getBlock().inverse());
		l_root.setLabel(labels_a); r_root.setLabel(labels_a); 
	}
	ATC=A; ATC.cTranspose(CTYPE); ATC.setLabel(labels_ATC);
	UniTensor ATC_l_root;ATC_l_root=ATC_l_root_make(ATC, l_root);
        UniTensor VL;VL=VL_make(ATC_l_root, D, d); 
	UniTensor BL, BR; 
	eigcal_sub( A, Ms, BL, BR, l, r, e0);
	if(sym_GF==true){
		UniTensor VR=VR_make(ATC, r_root);
		err2_v=err2(A, MPO, BL, BR, VL, VR);
	}
	int labels_ATC2[]={3, 4, -1}; 
	UniTensor VLTC=VL; VLTC.cTranspose(CTYPE);
	Network B_MPO_net("./Diagrams/B_MPO.net");
	B_MPO_net.putTensor("BL", BL); B_MPO_net.putTensor("BR", BR); B_MPO_net.putTensor("A", A); 
	B_MPO_net.putTensor("MPO", MPO); B_MPO_net.putTensor("VL", VL); B_MPO_net.putTensor("VLTC", VLTC);
	B_MPO_net.putTensor("l_root_inv_1", l_root_inv); B_MPO_net.putTensor("l_root_inv_2", l_root_inv); 
	B_MPO_net.putTensor("r_root_inv_1", r_root_inv); B_MPO_net.putTensor("r_root_inv_2", r_root_inv);
	UniTensor B_MPO=B_MPO_net.launch();
	if(sym_GF==true){ l=l_GF; r=r_GF;}
	int labels_A[]={0 ,1 ,2}; B_MPO.setLabel(labels_A); 
	if(sym_GF==false){B_MPO *= pow(normal, 0.5); A*=(pow(normal, 0.5));}
return B_MPO; 
}
