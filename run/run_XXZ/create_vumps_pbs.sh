My_path=`pwd | sed 's/\/run_XXZ//' | sed 's/\/run//'  `

VUMPS_XXZ_path=VUMPS/XXZ_VUMPS/XXZ

bond=`grep '^bond:' para_set | sed 's/^.*://'`
mass_list=`grep '^m_start_list:' para_set | sed 's/^.*://'`
delta_list=`grep '^delta_start_list:' para_set | sed 's/^.*://'`

queue=`grep '^vumps_queue:' para_set | sed 's/^.*://'`
ppn_num=`grep '^vumps_ppn_num:' para_set | sed 's/^.*://'`

printf "\n\n"
echo "======================Create corresponding pbs files for VUMPS algorithm ========================="
printf "\n\n"


#####create needed direction
if ! test -e ./run_dir/vumps_run_dir/BondDim"$bond"; then
	mkdir ./run_dir/vumps_run_dir/BondDim"$bond"
fi
if ! test -e ./log_dir/vumps_log/BondDim"$bond"; then
	mkdir ./log_dir/vumps_log/BondDim"$bond"
fi
if ! test -e ./run_dir/vumps_run_dir/BondDim"$bond"/Diagrams; then
	cp -r $My_path/$VUMPS_XXZ_path/Diagrams \
	./run_dir/vumps_run_dir/BondDim"$bond"/Diagrams
fi


#####create pbs file
for mass in ${mass_list} 
do
	for delta in ${delta_list}
	do
                echo "Create VUMPS .pbs file for delta=$delta and mass=$mass."
		cat ./run_dir/vumps_run_dir/submit_template.pbs | sed 's/%queue/'"$queue"'/' \
		| sed 's+%My_path+'"${My_path}"'+g'  | sed 's/%bd/'"$bond"'/' \
		| sed 's/%m/'"$mass"'/' | sed 's/%del/'"$delta"'/' | sed 's/%ppn_num/'"$ppn_num"'/'\
		> ./run_dir/vumps_run_dir/BondDim"$bond"/submit_BondDim"$bond"_m"$mass"_del"$delta".pbs
	done
done
