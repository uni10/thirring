#this code create the needed director at path to save data for first time

My_path=`pwd | sed 's/\/run_thirring//' | sed 's/\/run//'  `
DATA_DIR=`grep '^DATA_DIR:' para_set | sed 's/^.*://'`


if ! test -e ./exe_dir; then
	mkdir ./exe_dir
fi
if ! test -e ./exe_dir/tdvp_exe_dir; then
	mkdir ./exe_dir/tdvp_exe_dir
fi
if ! test -e ./exe_dir/vumps_exe_dir; then
	mkdir ./exe_dir/vumps_exe_dir
fi
if ! test -e ./log_dir; then
	mkdir ./log_dir
fi
if ! test -e ./log_dir/tdvp_log; then
	mkdir ./log_dir/tdvp_log
fi
if ! test -e ./log_dir/vumps_log; then
	mkdir ./log_dir/vumps_log
fi

if ! test -e "$DATA_DIR"/thirring_data; then
	mkdir "$DATA_DIR"/thirring_data
fi
if ! test -e "$DATA_DIR"/thirring_data/tdvp_tens; then
	mkdir "$DATA_DIR"/thirring_data/tdvp_tens
fi
if ! test -e "$DATA_DIR"/thirring_data/GS_tens; then
	mkdir "$DATA_DIR"/thirring_data/GS_tens
fi
if ! test -e "$DATA_DIR"/thirring_data/figure; then
	mkdir "$DATA_DIR"/thirring_data/figure
fi
if ! test -e "$DATA_DIR"/thirring_data/figure/tdvp_fig; then
	mkdir "$DATA_DIR"/thirring_data/figure/tdvp_fig
fi
