void iTDVP(UniTensor& A, UniTensor A_O_point, UniTensor A_end_point, UniTensor& MPO, vector<UniTensor>& Ms,
	double del_t, const int termi_step, const int del_step, double& delg_init, double& del_g, double& m_init, double& m);

void iTDVP(UniTensor& A, UniTensor A_O_point, UniTensor A_end_point, UniTensor& MPO, vector<UniTensor>& Ms,
	double del_t, const int termi_step, const int del_step, double& delg_init, double& del_g, double& m_init, double& m){
	int d=A.bond(0).dim(); int D=A.bond(1).dim();
	int step=0;
	double t=0;
	bool Dy_test=false;

	A_expand(A_O_point, D);
	A_expand(A_end_point, D);
	vector<double> ffc_cors;

//output command
	ofstream output_ffc;
	const int str_size=11;
	string files_name[str_size]={"r", "r2", "S", "Sz", "Arnerr", "bicg_err", "Cc", "E", "Sf", "Sx", "D"};
	ofstream outputs[str_size];

	string dir;
 	stringstream ss(dir);
	ss<<"mkdir "<<tdvp_f_name("dir0");
	system(ss.str().c_str());
	ss<<(Dy_bond_bool==true?"/Dmax":"/D")<<(Dy_bond_bool==true?D_max:D);
	system(ss.str().c_str());

	dir=contract_str("mkdir ", tdvp_f_name("dir1", D, delg_init, del_g, m_init, m, t));
	system(dir.c_str());
	string plan_filename; 
	if(im_bool==false){
		system(tdvp_f_name("dir2", D, delg_init, del_g, m_init, m, t).c_str());
		system(tdvp_f_name("dir3", D, delg_init, del_g, m_init, m, t).c_str());
		plan_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau); 
	}
	else{
		dir=minus_plus==1?dir.append("/im"):dir.append("/minus_im");
		system(dir.c_str());
		plan_filename=minus_plus==1?tdvp_f_name("dir1", D, delg_init, del_g, m_init, m, t).append("/im/"):
		tdvp_f_name("dir1", D, delg_init, del_g, m_init, m, t).append("/minus_im/");
	};

	for (int i=0; i<str_size-1; i++){
		files_name[i].append("_plan_saved");
		save_func("open", outputs[i], contract_str(plan_filename, files_name[i]));
	}
	if(Dy_bond_bool==true){
		files_name[10].append("_plan_saved");
		save_func("open", outputs[10], contract_str(plan_filename, files_name[10]));	
	}

//Prepare tensor
	RtoC(A); RtoC(MPO);

	Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
        vector<Bond> bonds; bonds.push_back(bdi); bonds.push_back(bdo);
        UniTensor l(CTYPE, bonds, "l"); UniTensor r(CTYPE, bonds, "r");
	bonds.clear();
	Matrix r_mat(CTYPE, D, D);Matrix l_mat(CTYPE, D, D); 
	r_mat.randomize(); l_mat.randomize(); 

	UniTensor l_root, r_root, l_root_inv, r_root_inv;
	UniTensor A_old;
	int labels_ATC[]={3, 4, 0};
	double e0=1.0;
	sym_gauge(A, l, r, l_mat, r_mat, l_root, l_root_inv);
	UniTensor ATC_old=A_end_point; ATC_old.cTranspose(CTYPE); ATC_old.setLabel(labels_ATC);
	UniTensor A2TC_old=A_O_point; A2TC_old.cTranspose(CTYPE); A2TC_old.setLabel(labels_ATC);

//print labels
	cout<<"time"<<setw(5)<<"D"<<setprecision(15)<<setw(9)<<"<H>"<<setw(23)<<"S_end"
	<<setw(38)<<"Entaglement entropy"<<setw(15)<<"return rate"<<setw(15)<<"<Sz>"<<endl;

	while((step<termi_step)&&(step_conti==true)){
		int D_tilde=D;
	//Use RK4 to solve non-linear differential equation, create B1
		if(im_bool==true) tau=t*minus_plus;
		UniTensor B1=B_MPO_make(true, A, MPO, Ms, l, r, l_mat, r_mat, e0);
		e=e0;
	//measure
		Sz=single_meas(A, Szt_mat(), l, r);
		Sx=single_meas(A, Sxt_mat(), l, r);
		Cc=single_meas(A, Chi_cond_mat(), l, r);
		s=S_meas(l, Sf);
		T1s=Ts_meas(ATC_old, A, l, r);
		T2s=Ts_meas(A2TC_old, A, l, r);
		double meas_val[str_size]={ 0.0, 0.0, s, Sz, Arn_err, bicg_err, Cc, e, Sf, Sx, 0.0};
		for(int i=2; i<str_size; i++){
			outputs[i]<<setprecision(15)<<meas_val[i]<<endl;
		}
		if(Dy_bond_bool==1)outputs[10]<<D<<endl;
		for(int i=0; i<T_num; i++){
			outputs[1]<<setprecision(15)<<T2s.at(i)<<endl;
			outputs[0]<<setprecision(15)<<T1s.at(i)<<endl;
		}
		if(step%del_step==0){
			if ((im_bool==false)||(im_bool==0)){
				A.save(tdvp_f_name("A", D, delg_init, del_g, m_init, m, t));
	//measure fermion correlator
 				stringstream ss_ffc(plan_filename);
				ss_ffc<<plan_filename<<"Fermion_cor/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"tau"
				<<tau<<"t"<<t<<"_cor.txt";
				output_ffc.open(ss_ffc.str().c_str());
				if(!output_ffc){cout<<"Fail to open ouptut_fermion"<<endl;}
				ff_cor(N_ffc, A, l, r, ffc_cors);
				for(int r_i=0;r_i<(N_ffc+1)/2; r_i++)output_ffc<<setprecision(15)<<ffc_cors.at(r_i)<<endl;
				ffc_cors.clear();
				output_ffc.close();
			}
			if ((im_bool==true)||(im_bool==1))
				output_func_tau(D, delg_init, del_g, m_init, m, t, A);
		}
	//RK4, create B2, B3, B4
		A_old=A; A=A+(-0.5)*del_t*dir_vec*B1;
		UniTensor B2=B_MPO_make(false, A, MPO, Ms, l, r, l_mat, r_mat, e0);
		A=A_old; A=A+(-0.5)*del_t*dir_vec*B2;
		UniTensor B3=B_MPO_make(false, A, MPO, Ms, l, r, l_mat, r_mat, e0);
		A=A_old; A=A+(-1.0)*del_t*dir_vec*B3;
		UniTensor B4=B_MPO_make(false, A, MPO, Ms, l ,r, l_mat, r_mat, e0);
		A=A_old; 
		UniTensor B_t=(1.0/6.0)*(B1+2.0*B2+2.0*B3+B4);
	//Dynamical expand bond dimension
		if((Dy_bond_bool==true)&&(Sf>Sf_acc)&&(D<D_max)){Dy_test=true;};
		if((Dy_bond_bool==true)&&(Dy_test==true)){
			D_tilde=D+del_D;
			if(D_tilde>D_max)D_tilde=D_max;
			UniTensor B_tilde=B_tilde_make(D_tilde, A, B_t, Ms, MPO, l, r, l_mat, r_mat, dir_vec, del_t);
			A_expand(A, D_tilde);
			A=A+B_tilde;
			D=D_tilde;
			int labels_A[]={0, 1, 2};
			ATC_old.cTranspose(CTYPE);
			ATC_old.setLabel(labels_A); 
			A_expand(ATC_old, D_tilde);
			ATC_old.cTranspose(CTYPE);
			ATC_old.setLabel(labels_ATC);
			A2TC_old.cTranspose(CTYPE);
			A2TC_old.setLabel(labels_A); 
			A_expand(A2TC_old, D_tilde);
			A2TC_old.cTranspose(CTYPE);
			A2TC_old.setLabel(labels_ATC);	
		}
	
	//print the measurement
		cout<<setprecision(3)<<setw(5)<<(double)step*del_t<<setw(5)<<D<<setprecision(15)
		<<setw(23)<<e<<setw(23)<<Sf<<setw(23)<<s<<setw(23)<<T2s.at(0)<<setw(23)<<Sz<<endl;


	//Update
		if(Dy_test==false)
		A=A+(-1.0)*dir_vec*del_t*B_t;
		Dy_test=false;
 
	//logic
		if(step_conti==false){cout<<"Warning!! stop for some reason"<<endl;cout<<"step="<<step<<endl;}
		step++; t=(double)step*del_t;
	}
	for (int i=0; i<str_size; i++)outputs[i].close();
	output_ffc.close();
}

