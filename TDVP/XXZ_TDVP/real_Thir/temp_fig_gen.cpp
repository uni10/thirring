#include<iostream>
#include<iomanip>
#include<cmath>
#include<string.h>
#include<fstream>
#include<sstream>
#include<vector>
using namespace std;

bool Dy_bond_bool=%Dy_bond_bool;
bool pltfuc_bool=0;
bool plan_bool=%plan_bool;

int D_max=%Dmax;

string f_name(string typ, int& D, double& g, double& m);
string tdvp_f_name(string typ, int &D, double& delg_init, double& del_g, double& m_init, double& m, double t);
string contract_str(string s1, string s2);
void create_dir(int D, double delg_init, double del_g, double m_init, double m);
void plt_func(int D, double delg_init, double del_g, double m_init, double m, double del_t);
void plt_sub(double& del_t, double delg_init, double del_g, double m_init, double m, string y_label, string str_in, string str_out);
void plt_T_plan(int plan_type, int D, double delg_init, double del_g, double m_init, double m, double del_t, 
double tau_i, double tau_f, double del_tau);
void plt_T_plan(int D, double delg_init, double del_g, double m_init, double m, double del_t, double tau_i, double tau_f, double del_tau);
void plt_plan(int plan_type, bool log_bool,  string observ_name, int D, double delg_init, double del_g, double m_init, double m, double del_t, 
double tau_i, double tau_f, double del_tau, int num, string title_name);
void plt_subplot(string observ_name, int D, double delg_init, double del_g, double m_init, double m, double del_t, 
double tau_i, double tau_f, double del_tau, int num, string title_name);
void plt_T_plan_plot(int D,double delg_init, double del_g, double m_init, double m, double del_t, double tau_i, double tau_f, 
	double del_tau, double *der_gap, int der_gap_size, string file_name, int num);
void plt_S_plan(int D, double delg_init, double del_g, double m_init, double m, double del_t, 
double tau_i, double tau_f, double del_tau);
void plt_Tplan_sub(double& del_t, double delg_init, double del_g, double m_init, double m, string y_label, string str_in, string str_out);
void plt2_sub(double& del_t, double delg_init, double del_g, double m_init, double m, string y1_label, string y2_label,
	 string str_in1, string str_in2, string str_out);
void plt_sub3(double& del_t, double delg_init, double del_g, double m_init, double m, string y_label, string str_in, string str_out);
void deriv(ifstream& input, ofstream& output, string f_name, double del_t);
void rate(ifstream& input, ofstream& output, string f_name, double del_t);
string title_str(double delg_init,double del_g, double m_init, double m);
string int2str(int i);
string int2str_2(int i);



int main(){
	int D=%D;
	double delg_init=%del_i;
	double del_g=%del_f;
	double m_init=%m_i;
	double m=%m_f;
	double del_t=%del_t;
	double termi_step=%termi_step; 
	double tau_i=%tau_i, tau_f=%tau_f;
	double del_tau=%del_tau;
	int T_num=%T_num;
	double der_gap[]={%der_gap}; int der_gap_size=sizeof(der_gap)/sizeof(*der_gap);
	
	create_dir(D, delg_init, del_g, m_init, m);
	//if(pltfuc_bool==true)plt_func(D, delg_init, del_g, m_init, m, del_t);	
	if(plan_bool==true){
		plt_plan(1, false, "S", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "Entanglement entropy");	
		plt_plan(1, false, "Sz", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "<Sz>");	
		plt_plan(1, false, "Cc", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "Chiral condensate");	
		plt_plan(1, false, "r", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, T_num, "Return rate function_wrt_im");	
		plt_plan(1, false, "r2", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, T_num, "Return rate function");	
		plt_plan(1, false, "E", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "Energy density");	
		plt_plan(1, true, "Sf", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "log10 plot\\n final element for density matrix");	
		plt_plan(1, true, "Arnerr", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "log10_plot\\n Error for Arnoldi");	
		plt_plan(1, true, "bicg_err", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "log10_plot\\n Error for bicgstab");	
		plt_plan(1, false, "Sx", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "<Sx>");
		if(Dy_bond_bool==true){	plt_plan(2, false, "D", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, 1, "Bond dimension");};
		plt_subplot("r2", D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, T_num, "Spectrum of transfer matrix");	
		plt_T_plan_plot(D, delg_init, del_g, m_init, m, del_t, tau_i, tau_f, del_tau, der_gap, der_gap_size, "r2", T_num);
	}

return 0;
}

void create_dir(int D, double delg_init, double del_g, double m_init, double m){
	string s; stringstream ss(s); 
	ss<<"mkdir "<<tdvp_f_name("dir1", D, delg_init, del_g, m_init, m, 0);
	system(ss.str().c_str());
	if(Dy_bond_bool==true){	ss<<"Dmax"<<D_max;}
	else{ss<<"D"<<D;};
	system(ss.str().c_str());
	string s2; stringstream ss2(s2); 
	ss2<<"mkdir "<<tdvp_f_name("dir2", D, delg_init, del_g, m_init, m, 0);
	system(ss2.str().c_str());
return;
}
/*
void plt_func(int D, double delg_init, double del_g, double m_init, double m, double del_t){
	string str_dir, str_in, str_out;
	string str_in1, str_in2;
	str_dir=tdvp_f_name("dir1", D, delg_init, del_g, m_init, m, 0);

	ifstream input_Cc, input_T; ofstream output_T_der, output_Cc_der, output_T_rate;
	input_Cc.open(contract_str(str_dir, "Cc_saved")); input_T.open(contract_str(str_dir, "T_saved"));
	output_Cc_der.open(contract_str(str_dir, "Cc_der_saved"));
	output_T_der.open(contract_str(str_dir, "T_der_saved"));
	output_T_der.open(contract_str(str_dir, "T_der_saved"));
	output_T_rate.open(contract_str(str_dir, "r_saved"));

	deriv(input_Cc, output_Cc_der,contract_str(str_dir, "Cc_saved"), del_t);
	deriv(input_T, output_T_der,contract_str(str_dir, "T_saved"), del_t);
	input_T.close(); input_T.open(contract_str(str_dir, "T_saved"));
	rate(input_T, output_T_rate,contract_str(str_dir, "T_saved"), del_t);
	input_T.close(); output_T_der.close();
	input_Cc.close(); output_Cc_der.close();

	str_in=contract_str(str_dir, "E_saved");
	str_out=contract_str(str_dir, "E.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "energy", str_in, str_out);

	str_in=contract_str(str_dir, "S_saved");
	str_out=contract_str(str_dir, "S.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "etanglement entropy", str_in, str_out);

	str_in=contract_str(str_dir, "Cc_saved");
	str_out=contract_str(str_dir, "Cc.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "Chiral condensate", str_in, str_out);

	str_in=contract_str(str_dir, "Cc_der_saved");
	str_out=contract_str(str_dir, "Cc_der.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "derivative of Chiral condensate", str_in, str_out);

	str_in=contract_str(str_dir, "T_saved");
	str_out=contract_str(str_dir, "T.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "transition probability", str_in, str_out);

	str_in=contract_str(str_dir, "T_der_saved");
	str_out=contract_str(str_dir, "T_der.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "derivative of transition probability", str_in, str_out);

	str_in=contract_str(str_dir, "T2_saved");
	str_out=contract_str(str_dir, "T2.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "transition probability (goal)", str_in, str_out);

	str_in=contract_str(str_dir, "Sz_saved");
	str_out=contract_str(str_dir, "Sz.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "<Sz>", str_in, str_out);

	str_in=contract_str(str_dir, "err2_saved");
	str_out=contract_str(str_dir, "err2.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "error", str_in, str_out);

	str_in=contract_str(str_dir, "Sx_saved");
	str_out=contract_str(str_dir, "Sx.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "<Sx>", str_in, str_out);

	str_in1=contract_str(str_dir, "Cc_saved");
	str_in2=contract_str(str_dir, "T_saved");
	str_out=contract_str(str_dir, "Cc_T.py");
	plt2_sub(del_t, delg_init, del_g, m_init, m, "Chiral condensate", "return probability", str_in1, str_in2, str_out);

	str_in1=contract_str(str_dir, "Cc_der_saved");
	str_in2=contract_str(str_dir, "T_der_saved");
	str_out=contract_str(str_dir, "Cc_T_der.py");
	plt2_sub(del_t, delg_init, del_g, m_init, m, "derivatine of Chiral condensate", "derivative of return probability", str_in1, str_in2, str_out);

	str_in=contract_str(str_dir, "r_saved");
	str_out=contract_str(str_dir, "r.py");
	plt_sub(del_t, delg_init, del_g, m_init, m, "return rate", str_in, str_out);

	str_in=contract_str(str_dir, "rs_saved");
	str_out=contract_str(str_dir, "rs.py");
	plt_sub3(del_t, delg_init, del_g, m_init, m, "return rate", str_in, str_out);
return;
}

void plt_sub(double& del_t, double delg_init, double del_g, double m_init, double m, string y_label, string str_in, string str_out){
	ifstream input; ofstream output;
	char ch_in[100];
	strcpy(ch_in, str_in.c_str());
	input.open(ch_in);
	if(!input){cout<<"false in"<<endl;}
	int step=0; double t=0;
	double val;
	while(input.peek()!=EOF){input>>val; step++;}
	step-=1; input.close();
	input.open(ch_in);
	char ch_out[100];
	strcpy(ch_out, str_out.c_str());
	output.open(ch_out);
	if(!output){cout<<"false out"<<endl;}
	output<<"import matplotlib.pyplot as plt"<<endl;
	output<<"y=(";
	for(int i=0; i<step; i++){input>>val;output<<setprecision(15)<<val<<",";}
	output<<")"<<endl;
	output<<"x=(";
	for(int i=0; i<step; i++){t=(double)i*del_t;output<<setprecision(15)<<t<<","; }
	output<<")"<<endl;
	output<<"plt.title('"<<title_str(delg_init, del_g, m_init, m)<<"')"<<endl<<"plt.xlabel('real time t')"<<endl
	<<"plt.ylabel('"<<y_label<<"')"<<endl;
	output<<"plt.plot(x, y, c='b')"<<endl<<"plt.show()"<<endl;
return;
}
*/

void plt_plan(int plan_type, bool log_bool,  string observ_name, int D, double delg_init, double del_g, double m_init, double m, double del_t, 
double tau_i, double tau_f, double del_tau, int num, string title_name){

	string str_dir, str_in, str_out;
	str_dir=tdvp_f_name("dir2", D, delg_init, del_g, m_init, m, 0);
	int tau_tol=(tau_f-tau_i)/del_tau+1+1.0e-9;
	ofstream output_plan; 
	string input_filename=contract_str(observ_name, "_plan_saved");
	string output_filename=contract_str(observ_name, "_plan.py");
	output_plan.open(contract_str(str_dir, output_filename));
	int step=0;
	double tau;
	double val;
	string title_s; stringstream title_ss(title_s); 
	title_ss<<"D"<<D<<", m:"<<m_init<<"$\\\\rightarrow$"<<m<<", $\\Delta$:"<<delg_init<<"$\\\\rightarrow$"<<del_g<<"\\n";
	title_name=contract_str(title_ss.str(), title_name);
	output_plan<<"x=("<<endl;
	for(int i=0; i<tau_tol; i++){
		step=0;
		tau=i*del_tau+tau_i; if(fabs(tau)<1.0e-10)tau=0;
		string plan_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau);
		plan_filename=contract_str(plan_filename, input_filename);
		ifstream input_plan;
		input_plan.open(plan_filename);
		if(!input_plan){cout<<"fail to open input "<<input_filename<<" for tau= "<<tau<<endl; }
		while(input_plan.peek()!=EOF){input_plan>>val; step++;}
		step-=1; step/=num; input_plan.close();
		for(int j=0; j<step; j++)output_plan<<setprecision(15)<<tau<<",";
		output_plan<<endl;
	}

	output_plan<<")"<<endl;output_plan<<"y=("<<endl;
	for(int i=0; i<tau_tol; i++){
		step=0;
		tau=i*del_tau+tau_i;if(fabs(tau)<1.0e-10)tau=0;
		string plan_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau);
		plan_filename=contract_str(plan_filename, input_filename);
		ifstream input_plan;
		input_plan.open(plan_filename);
		if(!input_plan){cout<<"fail to open input "<<input_filename<<" for tau= "<<tau<<endl; }
		while(input_plan.peek()!=EOF){input_plan>>val; step++;}
		step-=1; step/=num; input_plan.close();
		for(int j=0; j<step; j++)output_plan<<setprecision(15)<<j*del_t<<",";
		output_plan<<endl;
	}

	output_plan<<")"<<endl;output_plan<<"z=("<<endl;
	for(int i=0; i<tau_tol; i++){
		step=0;
		tau=i*del_tau+tau_i;if(fabs(tau)<1.0e-10)tau=0;
		string plan_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau);
		plan_filename=contract_str(plan_filename, input_filename);
		ifstream input_plan;
		input_plan.open(plan_filename);
		if(!input_plan){cout<<"fail to open input "<<input_filename<<" for tau= "<<tau<<endl; }
		while(input_plan.peek()!=EOF){input_plan>>val; step++;}
		step-=1; step/=num; input_plan.close();
		input_plan.open(plan_filename);
		input_plan>>val;
		for(int i=0; i<step; i++){
			if(log_bool==true){output_plan<<setprecision(15)<<log10(val)<<",";}
			else{output_plan<<setprecision(15)<<val<<",";}
			for(int num_step=0; num_step<num; num_step++)input_plan>>val; 
		} 
		output_plan<<endl;

		input_plan.close();
	}
	output_plan<<")"<<endl;
	output_plan<<"import numpy as np"<<endl<<"import matplotlib.pyplot as plt"<<endl
	<<"X=np.reshape(x,"<<"("<<tau_tol<<","<<step<<"))"<<endl
	<<"Y=np.reshape(y,"<<"("<<tau_tol<<","<<step<<"))"<<endl
	<<"Z=np.reshape(z,"<<"("<<tau_tol<<","<<step<<"))"<<endl;
	if(plan_type==1)output_plan<<"plt.contourf(X, Y, Z, 1000, cmap=plt.cm.jet)"<<endl;
	else{output_plan<<"plt.pcolormesh(X,Y,Z)"<<endl;};
	output_plan<<"plt.colorbar()"<<endl<<"plt.title('"<<title_name<<" on complex plan')"<<endl
	<<"plt.xlabel('Im')"<<endl<<"plt.ylabel('Re')"<<endl<<"plt.show()"<<endl;


	output_plan.close();
return;
}


void plt_subplot(string observ_name, int D, double delg_init, double del_g, double m_init, double m, double del_t, 
double tau_i, double tau_f, double del_tau, int num, string title_name){

	string str_dir, str_in, str_out;
	str_dir=tdvp_f_name("dir2", D, delg_init, del_g, m_init, m, 0);
	int tau_tol=(tau_f-tau_i)/del_tau+1+1.0e-9;
	ofstream output_plot; 
	string input_filename=contract_str(observ_name, "_plan_saved");
	string output_filename=contract_str(observ_name, "_plot.py");
	output_plot.open(contract_str(str_dir, output_filename));
	int step=0;
	double tau;
	double val;
	string title_s; stringstream title_ss(title_s); 
	title_ss<<"\\n"<<"D"<<D<<", m:"<<m_init<<"$\\\\rightarrow$"<<m<<", $\\Delta$:"<<delg_init<<"$\\\\rightarrow$"<<del_g;
	title_name=contract_str(title_name, title_ss.str());
	double max_val=0, min_val=0;

	
	for(int i=0; i<tau_tol; i++){
		step=0;
		tau=i*del_tau+tau_i;if(fabs(tau)<1.0e-10)tau=0;
		output_plot<<"x"<<int2str(i)<<"=("<<endl;
		string plot_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau);
		plot_filename=contract_str(plot_filename, input_filename);
		ifstream input_plot;
		input_plot.open(plot_filename);
		if(!input_plot){cout<<"fail to open input "<<input_filename<<" for tau= "<<tau<<endl; }
		while(input_plot.peek()!=EOF){input_plot>>val; step++;}
		step-=1; step/=num; input_plot.close();
		for(int j=0; j<step; j++)output_plot<<setprecision(15)<<j*del_t<<",";
		output_plot<<")"<<endl;
	}

	for(int i=0; i<tau_tol; i++){
		step=0;
		tau=i*del_tau+tau_i;if(fabs(tau)<1.0e-10)tau=0;
		string plot_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau);
		plot_filename=contract_str(plot_filename, input_filename);
		ifstream input_plot;
		input_plot.open(plot_filename);
		if(!input_plot){cout<<"fail to open input "<<input_filename<<" for tau= "<<tau<<endl; }
		while(input_plot.peek()!=EOF){input_plot>>val; step++;}
		step-=1; step/=num; input_plot.close();
		for(int num_step=0; num_step<num; num_step++){
			input_plot.open(plot_filename);
			output_plot<<"y"<<int2str(i)<<"_"<<int2str(num_step)<<"=("<<endl;
			for(int num_step3=0; num_step3<(num_step+1); num_step3++)
				input_plot>>val; 
			for(int i=0; i<step; i++){
				output_plot<<setprecision(15)<<val<<",";
				max_val=max(val, max_val); min_val=min(val, min_val);
				for(int num_step2=0; num_step2<num; num_step2++)input_plot>>val; 
			}
			output_plot<<")"<<endl;
			input_plot.close();
		} 

	}
	output_plot<<"import numpy as np"<<endl<<"import matplotlib.pyplot as plt"<<endl;
	for(int i=0; i<tau_tol; i++){
		tau=i*del_tau+tau_i;if(fabs(tau)<1.0e-10)tau=0;
		output_plot<<"plt.figure()"<<endl;
		for(int num_step=0; num_step<num; num_step++){
			string label_name;
			if(num_step==0){label_name="lowest";} else if (num_step==1){label_name="2nd";}
			else if(num_step==2){label_name="3rd";} 
			else {label_name=int2str(num_step+1); label_name=contract_str(label_name, "th");};
			output_plot<<"plt.plot(x"<<int2str(i)<<","<<"y"<<int2str(i)<<"_"<<
			int2str(num_step)<<", label='"<<label_name<<"')"<<endl;
		}
		output_plot<<"plt.xlabel('real time t')"<<endl<<"plt.ylabel('Spectrum')"
		<<endl<<"plt.ylim("<<min_val<<","<<max_val<<")"
		<<endl<<"plt.title('"<<contract_str(title_name, " tau=")<<tau<<"')"
		<<endl<<"plt.legend(loc='upper right')"<<endl;
	}
	output_plot<<"plt.show()"<<endl;;

	output_plot.close();
return;
}

void plt_T_plan_plot(int D,double delg_init, double del_g, double m_init, double m, double del_t, double tau_i, double tau_f, 
	double del_tau, double *der_gap, int der_gap_size, string file_name, int num){

	string str_dir, str_in, str_out;
	str_dir=tdvp_f_name("dir2", D, delg_init, del_g, m_init, m, 0);
	int tau_tol=(tau_f-tau_i)/del_tau+1+1.0e-9;
	ofstream output_Tplan; 
	output_Tplan.open(contract_str(str_dir, contract_str(file_name, "_plan_scat.py")));
	output_Tplan<<"import numpy as np"<<endl<<"import matplotlib.pyplot as plt"<<endl;
	int step=0;
	double tau;
	double val, val1, val2, val3, val_old;
	string colors[]={"black","b","r","green","y"};
	vector<double> xs, ys;
	double temp;
	for(int gap_time=0; gap_time<der_gap_size; gap_time++){
	for(int i=0; i<tau_tol; i++){ 
		step=0;
		tau=i*del_tau+tau_i;if(fabs(tau)<1.0e-10)tau=0;
		string T_plan_filename=tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau);
		T_plan_filename=contract_str(T_plan_filename, file_name);
		T_plan_filename=contract_str(T_plan_filename, "_plan_saved");
		ifstream input_Tplan;
		input_Tplan.open(T_plan_filename);
		if(!input_Tplan){cout<<"fail to open T plan input"<<endl; }
		while(input_Tplan.peek()!=EOF){input_Tplan>>val; step++;}
		step-=1; step/=num; input_Tplan.close();
		input_Tplan.open(T_plan_filename);

		input_Tplan>>val1; 
		for (int num_step=0; num_step<num-1; num_step++)input_Tplan>>temp;
		input_Tplan>>val2; 
		for (int num_step=0; num_step<num-1; num_step++)input_Tplan>>temp;
		input_Tplan>>val3;
		val=val2-val1; val*=(1.0/del_t); val_old=val;
		for(int j=0; j<step-2; j++){
			val=val3-val1;
			val*=(0.5/del_t);
			if((val-val_old)<(-1.0)*der_gap[gap_time]){xs.push_back((1.0)*tau); ys.push_back(j*del_t);};
			val_old=val;
			val1=val2;val2=val3;
			if(j<step-3){for (int num_step=0; num_step<num-1; num_step++)input_Tplan>>temp;
			input_Tplan>>val3;};
		}
		val=val2-val1; val*=(1.0/del_t);
		if((val-val_old)<(-1.0)*der_gap[gap_time]){xs.push_back((1.0)*tau); ys.push_back((step-2)*del_t);};

	input_Tplan.close();
	}
	output_Tplan<<"x"<<gap_time<<"=("<<endl;
	for(int i=0; i<xs.size(); i++)output_Tplan<<xs.at(i)<<",";
	output_Tplan<<endl<<")"<<endl;
	output_Tplan<<"y"<<gap_time<<"=("<<endl;
	for(int i=0; i<xs.size(); i++)output_Tplan<<ys.at(i)<<",";
	output_Tplan<<endl<<")"<<endl;
	output_Tplan<<"plt.scatter(x"<<gap_time<<",y"<<gap_time<<",c='"<<colors[gap_time]<<"', label='gap > "
	<<der_gap[gap_time]<<"')"<<endl;
	xs.clear(); ys.clear();
	}
	
	output_Tplan<<"plt.legend(loc='lower left')"<<endl
	<<"plt.title('Critical point: $\\Delta$:"<<delg_init<<"$\\\\rightarrow$"<<del_g<<",m:"<<m_init<<"$\\\\rightarrow$"<<m<<"')"<<endl
	<<"plt.xlabel('Im')"<<endl<<"plt.ylabel('Re')"<<endl<<"plt.show()"<<endl;
	output_Tplan.close();
return;
}

void plt2_sub(double& del_t, double delg_init, double del_g, double m_init, double m, string y1_label, string y2_label,
	 string str_in1, string str_in2, string str_out){

	ifstream input1, input2; ofstream output;
	char ch_in1[100];
	strcpy(ch_in1, str_in1.c_str());
	input1.open(ch_in1);
	if(!input1){cout<<"false in"<<endl;}
	int step=0; double t=0;
	double val;
	while(input1.peek()!=EOF){input1>>val; step++;}
	step-=1; input1.close();
	input1.open(ch_in1);
	char ch_in2[100];
	strcpy(ch_in2, str_in2.c_str());
	input2.open(ch_in2);
	if(!input2){cout<<"false in"<<endl;}
	char ch_out[100];
	strcpy(ch_out, str_out.c_str());
	output.open(ch_out);
	if(!output){cout<<"false out"<<endl;}
	output<<"import matplotlib.pyplot as plt"<<endl;
	output<<"y1=(";
	for(int i=0; i<step; i++){input1>>val;output<<setprecision(15)<<val<<",";}
	output<<")"<<endl;
	output<<"y2=(";
	for(int i=0; i<step; i++){input2>>val;output<<setprecision(15)<<val<<",";}
	output<<")"<<endl;
	output<<"x=(";
	for(int i=0; i<step; i++){t=(double)i*del_t;output<<setprecision(15)<<t<<","; }
	output<<")"<<endl;
	output<<"fig, ax1=plt.subplots()"<<endl<<"plt.title('"<<title_str(delg_init, del_g, m_init, m)<<"')"<<endl<<
	"ax1.plot(x, y1, 'b-')"<<endl<<"ax1.set_xlabel('real time t')"<<endl<<"ax1.set_ylabel('"<<y1_label<<"',color='b')"<<
	endl<<"ax1.tick_params('y', colors='b')"<<endl<<	
	"ax2=ax1.twinx()"<<endl<<"ax2.plot(x, y2, 'r-')"<<endl<<"ax2.set_ylabel('"<<y2_label<<"',color='r')"<<
	endl<<"ax2.tick_params('y', colors='r')"<<endl<<
	"fig.tight_layout()"<<endl<<"plt.show()"<<endl;
return;
}

void deriv(ifstream& input, ofstream& output, string f_name, double del_t){
	int step=0; double val1, val2, val3, val;
	if(!input){cout<<"fail to open input file "<<endl; }
	while(input.peek()!=EOF){input>>val; step++;}
	step-=1; input.close();
	input.open(f_name);
	input>>val1; input>>val2;input>>val3;
	val=val2-val1; val*=(1.0/del_t);
	output<<setprecision(15)<<val<<endl;
	for(int i=0; i<step-2; i++){
		val=val3-val1;
		val*=(0.5/del_t);
		output<<setprecision(15)<<val<<endl;
		val1=val2;val2=val3;
		if(i<step-3)input>>val3;
	}
	val=val2-val1; val*=(1.0/del_t);
	output<<setprecision(15)<<val<<endl;
return;
}

void rate(ifstream& input, ofstream& output, string f_name, double del_t){
	int step=0; double val;
	if(!input){cout<<"fail to open input file "<<endl; }
	while(input.peek()!=EOF){input>>val; step++;}
	step-=1; input.close();
	input.open(f_name);
	for(int i=0; i<step; i++){
		input>>val; 
		val=(-0.5)*log(val);
		output<<setprecision(15)<<val<<endl;
	}
return;
}

string tdvp_f_name(string typ, int &D, double& delg_init, double& del_g, double& m_init, double& m, double t){
	string s;
	stringstream ss(s);
	if(Dy_bond_bool==true){
		if(typ=="dir1"){
			ss<<"%DATA_DIR/XXZ_data/figure/tdvp_fig/Dy_bond/";
		}
		if(typ=="dir2"){
			ss<<"%DATA_DIR/XXZ_data/figure/tdvp_fig/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/";
		}
		if(typ=="others_tau"){
			ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<t<<"/";
		}
		else{"wrong direction or wrong file name!";}
	}
	else{

		if(typ=="dir1"){
			ss<<"%DATA_DIR/XXZ_data/figure/tdvp_fig/Fix_bond/";
		}
		if(typ=="dir2"){
			ss<<"%DATA_DIR/XXZ_data/figure/tdvp_fig/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/";
		}
		if(typ=="others_tau"){
			ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<t<<"/";
		}
		else{"wrong direction or wrong file name!";}
	}
return ss.str();
}

string contract_str(string s1, string s2){
	string s;
	stringstream ss(s);
	ss<<s1<<s2;
return ss.str();
}
 
string title_str(double delg_init,double del_g, double m_init, double m){
	string s;
	stringstream ss(s);
	ss<<"XXZ: $\\Delta(g)$="<<delg_init<<" to "<<del_g<<" m="<<m_init<<" to "<<m;
return ss.str();
}

string int2str(int i){
	string s; stringstream ss(s); ss<<i;
return ss.str();
}
 
string int2str_2(int i){
	string s; stringstream ss(s); ss<<i;
return ss.str();
}
