My_path=`pwd | sed 's/\/run_XXZ//' | sed 's/\/run//'  `
VUMPS_XXZ_path=VUMPS/XXZ_VUMPS/XXZ
DATA_DIR=`grep '^DATA_DIR:' para_set | sed 's/^.*://'`

bond_init=`grep '^vumps_bond_init:' para_set | sed 's/^.*://'`
bond=`grep '^bond:' para_set | sed 's/^.*://'`
mass_list=`grep '^m_start_list:' para_set | sed 's/^.*://'`
delta_list=`grep '^delta_start_list:' para_set | sed 's/^.*://'`

Arnacc=`grep '^vumps_Arnacc:' para_set | sed 's/^.*://'`
bicg_acc=`grep '^vumps_bicg_acc:' para_set | sed 's/^.*://'`
Lanacc=`grep '^vumps_Lanc_acc:' para_set | sed 's/^.*://'`
Arnstep_max=`grep '^vumps_Arnstep_max:' para_set | sed 's/^.*://'`
bicg_step_max=`grep '^vumps_bicg_step_max:' para_set | sed 's/^.*://'`
Lanstep_max=`grep '^vumps_Lanstep_max:' para_set | sed 's/^.*://'`
goal_acc=`grep '^vumps_goal_acc:' para_set | sed 's/^.*://'`

printf "\n\n"
echo "======================Create corresponding exe files for VUMPS algorithm ========================="


cat $My_path/$VUMPS_XXZ_path/temp_VUMPS_Tools.cpp | sed 's+%DATA_DIR+'"$DATA_DIR"'+g' \
> $My_path/$VUMPS_XXZ_path/VUMPS_Tools.cpp

for mass in ${mass_list}
do
	for delta in ${delta_list}
	do
		printf '\n\n'
		echo "**********************************************************"
		echo "Create VUMPS exe file for delta=$delta and mass=$mass."
		echo "**********************************************************"
		cat $My_path/$VUMPS_XXZ_path/temp_Thir_VUMPS.cpp | sed 's/%m/'"$mass"'/'  \
		| sed 's/%delg/'"$delta"'/'| sed 's/%bondinit/'"$bond_init"'/' \
		| sed 's/%D/'"$bond"'/' \
		| sed 's/%Arnacc/'"$Arnacc"'/' | sed 's/%Arnstep_max/'"$Arnstep_max"'/'\
		| sed 's/%bicg_acc/'"$bicg_acc"'/' | sed 's/%bicg_step_max/'"$bicg_step_max"'/'\
		| sed 's/%Lanc_acc/'"$Lanacc"'/' | sed 's/%Lanstep_max/'"$Lanstep_max"'/'\
		| sed 's/%goal_acc/'"$goal_acc"'/' \
		> $My_path/$VUMPS_XXZ_path/Thir_VUMPS.cpp
		cd $My_path/$VUMPS_XXZ_path && make && cd $My_path/run/run_XXZ/ 
		if ! test -e ./exe_dir/vumps_exe_dir/BondDim"$bond"; then
			mkdir $My_path/run/run_XXZ/exe_dir/vumps_exe_dir/BondDim"$bond"
			cp -r $My_path/$VUMPS_XXZ_path/Diagrams ./exe_dir/vumps_exe_dir/BondDim"$bond"
		fi
		cp $My_path/$VUMPS_XXZ_path/Thir_VUMPS.e \
		`echo "$My_path/run/run_XXZ/exe_dir/vumps_exe_dir/BondDim"$bond"/Thir_VUMPS_m"$mass"_del"$delta".e"`
		cd $My_path/$VUMPS_XXZ_path && make clean && cd $My_path/run/run_XXZ/
	done
done
