My_path=`pwd | sed 's/\/run_XXZ//' | sed 's/\/run//'  `
TDVP_XXZ_path=TDVP/XXZ_TDVP/real_Thir
DATA_DIR=`grep '^DATA_DIR:' para_set | sed 's/^.*://'`

Dy_bond_bool=`grep '^Dy_bond_bool:' para_set | sed 's/^.*://'`
del_D=`grep '^del_D:' para_set | sed 's/^.*://'`
D_max=`grep '^D_max:' para_set | sed 's/^.*://'`
Sf_acc=`grep '^S_final_acc:' para_set | sed 's/^.*://'`

im_spec_num=1
im_save_step=`grep '^im_save_step:' para_set | sed 's/^.*://'`
im_del_t=`grep '^im_del_t:' para_set | sed 's/^.*://'`
im_termi_step=`grep '^im_termi_step:' para_set | sed 's/^.*://'`

re_spec_num=`grep '^re_spec_num:' para_set | sed 's/^.*://'`
re_save_step=`grep '^re_save_step:' para_set | sed 's/^.*://'`
re_del_t=`grep '^re_del_t:' para_set | sed 's/^.*://'`
re_termi_step=`grep '^re_termi_step:' para_set | sed 's/^.*://'`
im_length=`grep '^im_length:' para_set | sed 's/^.*://'`

tau=`grep '^tau:' para_set | sed 's/^.*://'`
tau=$(echo "scale=1; $tau")
tau=$(echo "scale=1; ${tau}/1" |bc -l |sed -e 's/^\./0./' |sed -e 's/^-\./-0./' |sed -e 's/\.0//' )
tau_init=$tau
bond=`grep '^bond:' para_set | sed 's/^.*://'`

delta_start_list=`grep '^delta_start_list:' para_set | sed 's/^.*://'`
delta_end_list=`grep '^delta_end_list:' para_set | sed 's/^.*://'`
m_start_list=`grep '^m_start_list:' para_set | sed 's/^.*://'`
m_end_list=`grep '^m_end_list:' para_set | sed 's/^.*://'`
minus_plus_list='1 -1'
minus_plus_re=1;

Arnacc=`grep '^tdvp_Arnacc:' para_set | sed 's/^.*://'`
bicg_acc=`grep '^tdvp_bicg_acc:' para_set | sed 's/^.*://'`
Arnstep_max=`grep '^tdvp_Arnstep_max:' para_set | sed 's/^.*://'`
bicg_step_max=`grep '^tdvp_bicg_step_max:' para_set | sed 's/^.*://'`

der_gap=`grep '^der_gap:' para_set | sed 's/^.*://'`
plan_bool=`grep '^plan_bool:' para_set | sed 's/^.*://'`



function dir_create_func(){
	if [ $Dy_bond_bool == 'true' ] || [ $Dy_bond_bool == '1' ]; then
		if ! test -e ./exe_dir/tdvp_exe_dir/Dy_bond; then
			mkdir ./exe_dir/tdvp_exe_dir/Dy_bond
		fi
		if ! test -e ./exe_dir/tdvp_exe_dir/Dy_bond/Dmax"$D_max"; then
			mkdir ./exe_dir/tdvp_exe_dir/Dy_bond/Dmax"$D_max"
		fi
	else
		if ! test -e ./exe_dir/tdvp_exe_dir/Fix_bond; then
			mkdir ./exe_dir/tdvp_exe_dir/Fix_bond
		fi
		if ! test -e ./exe_dir/tdvp_exe_dir/Fix_bond/BondDim"$bond"; then
			mkdir ./exe_dir/tdvp_exe_dir/Fix_bond/BondDim"$bond"
		fi
	fi
	if ! test -e $dir_path; then
		mkdir $dir_path/
		cp -r $My_path/$TDVP_XXZ_path/Diagrams  $dir_path/
	fi
}

function make_func(){
	cd $My_path/$TDVP_XXZ_path && make
	if [ "$im_bool" == "true" ] || [ "$im_bool" == 1 ] ; then
		if [ $minus_plus == 1 ]; then
			cp $My_path/$TDVP_XXZ_path/Thirring_TDVP.e $dir_path/XXZ_"$bond_str"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"_im.e
		else	
			cp $My_path/$TDVP_XXZ_path/Thirring_TDVP.e $dir_path/XXZ_"$bond_str"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"_minus_im.e
		fi
	else
		cp $My_path/$TDVP_XXZ_path/Thirring_TDVP.e $dir_path/XXZ_"$bond_str"m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"_tau"$tau".e
	fi
	make clean && cd $My_path/run/run_XXZ

}


cat $My_path/$TDVP_XXZ_path/temp_TDVP_algorithm.cpp | sed 's+%DATA_DIR+'"$DATA_DIR"'+g' \
>  $My_path/$TDVP_XXZ_path/reTDVP_algorithm.cpp
cat $My_path/$TDVP_XXZ_path/temp_file_path.cpp | sed 's+%DATA_DIR+'"$DATA_DIR"'+g' \
>  $My_path/$TDVP_XXZ_path/file_path.cpp
printf "\n\n"
echo "======================Create corresponding exe files for TDVP algorithm ========================="
printf "\n\n"

for m_start in ${m_start_list}
do
for m_end in ${m_end_list}
do
for delta_start in ${delta_start_list}
do
for delta_end in ${delta_end_list}
do
	if [ $Dy_bond_bool == 'true' ] || [ $Dy_bond_bool == '1' ]; then
		dir_path="$My_path/run/run_XXZ/exe_dir/tdvp_exe_dir/Dy_bond/Dmax"$D_max"/quench_m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end""
		bond_str=""
	else
		dir_path="$My_path/run/run_XXZ/exe_dir/tdvp_exe_dir/Fix_bond/BondDim"$bond"/quench_D"$bond"_m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end""
		bond_str="D${bond}_"
	fi
	dir_create_func
	printf "\n"
	echo "====================create the generattion of figure exe files===================="
	printf "\n"
	tau_f=$(echo "scale=2; ${tau_init}+${im_save_step}*${im_del_t}/1*$(($im_length-1))" |bc -l \
		|sed -e 's/^\./0./' |sed -e 's/^-\./-0./'  )
	del_tau=$(echo "scale=2; ${im_save_step}*${im_del_t}/1" |bc -l \
		|sed -e 's/^\./0./' |sed -e 's/^-\./-0./'  )
	cat $My_path/$TDVP_XXZ_path/temp_fig_gen.cpp | sed 's/%T_num/'"$re_spec_num"'/' \
	| sed 's+%DATA_DIR+'"$DATA_DIR"'+g' \
	| sed 's/%Dy_bond_bool/'"$Dy_bond_bool"'/' |sed 's/%Dmax/'"$D_max"'/' \
	| sed 's/%plan_bool/'"$plan_bool"'/'| sed 's/%del_tau/'"$del_tau"'/' \
	| sed 's/%del_t/'"$re_del_t"'/' | sed 's/%termi_step/'"$re_termi_step"'/' \
	| sed 's/%m_i/'"$m_start"'/' | sed 's/%m_f/'"$m_end"'/' \
	| sed 's/%del_i/'"$delta_start"'/' | sed 's/%del_f/'"$delta_end"'/' \
	| sed 's/%D/'"$bond"'/' | sed 's/%tau_i/'"$tau_init"'/' | sed 's/%tau_f/'"$tau_f"'/' \
	| sed 's/%der_gap/'"$der_gap"'/' \
	>  $My_path/$TDVP_XXZ_path/fig_gen.cpp
	cd $My_path/$TDVP_XXZ_path && make
	if [ $Dy_bond_bool == 'true' ] || [ $Dy_bond_bool == '1' ]; then
		cp $My_path/$TDVP_XXZ_path/fig_gen.e $dir_path/XXZ_m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"_fig_gen.e
	else
		cp $My_path/$TDVP_XXZ_path/fig_gen.e $dir_path/XXZ_D"$bond"_m"$m_start"to"$m_end"_del"$delta_start"to"$delta_end"_fig_gen.e
	fi
	cd $My_path/run/run_XXZ

for im_bool in "true" "false"
do
	if [ "$im_bool" == "true" ] || [ "$im_bool" == 1 ] ; then
		printf "\n\n"
		echo "=========================create imaginary time TDVP exe files======================="
		printf "\n"
		cat $My_path/$TDVP_XXZ_path/temp_Thirring_TDVP.cpp | sed 's/%spec_num/'"$im_spec_num"'/' \
		| sed 's/%im_bool/'"$im_bool"'/'| sed 's/%save_step/'"$im_save_step"'/' \
		| sed 's/%del_t/'"$im_del_t"'/' | sed 's/%termi_step/'"$im_termi_step"'/' \
		| sed 's/%m_i/'"$m_start"'/' | sed 's/%m_f/'"$m_end"'/' \
		| sed 's/%del_i/'"$delta_start"'/' | sed 's/%del_f/'"$delta_end"'/' \
		| sed 's/%Dy_bond_bool/'"$Dy_bond_bool"'/' |sed 's/%Dmax/'"$D_max"'/' | sed 's/%D/'"$bond"'/' \
		| sed 's/%del_D/'"$del_D"'/' | sed 's/%Sf_acc/'"$Sf_acc"'/' \
		| sed 's/%Arnacc/'"$Arnacc"'/' | sed 's/%bicg_acc/'"$bicg_acc"'/' \
		| sed 's/%Arnstep_max/'"$Arnstep_max"'/' | sed 's/%bicg_step_max/'"$bicg_step_max"'/' \
		>  $My_path/$TDVP_XXZ_path/Thirring_TDVP_temp.cpp

		for minus_plus in $minus_plus_list
		do
			cat $My_path/$TDVP_XXZ_path/Thirring_TDVP_temp.cpp \
			| sed 's/%tau/'"$tau"'/' | sed 's/%im_minus_plus/'"$minus_plus"'/' \
			> $My_path/$TDVP_XXZ_path/Thirring_TDVP.cpp
			make_func
		done
	else
		printf "\n\n"
		echo "==========================create real time TDVP exe files==========================="
		cat $My_path/$TDVP_XXZ_path/temp_Thirring_TDVP.cpp | sed 's/%spec_num/'"$re_spec_num"'/' \
		| sed 's/%im_bool/'"$im_bool"'/'| sed 's/%save_step/'"$re_save_step"'/' \
		| sed 's/%del_t/'"$re_del_t"'/' | sed 's/%termi_step/'"$re_termi_step"'/' \
		| sed 's/%m_i/'"$m_start"'/' | sed 's/%m_f/'"$m_end"'/' \
		| sed 's/%del_i/'"$delta_start"'/' | sed 's/%del_f/'"$delta_end"'/' \
		| sed 's/%Dy_bond_bool/'"$Dy_bond_bool"'/' |sed 's/%Dmax/'"$D_max"'/' | sed 's/%D/'"$bond"'/' \
		| sed 's/%del_D/'"$del_D"'/' | sed 's/%Sf_acc/'"$Sf_acc"'/' \
		| sed 's/%Arnacc/'"$Arnacc"'/' | sed 's/%bicg_acc/'"$bicg_acc"'/' \
		| sed 's/%Arnstep_max/'"$Arnstep_max"'/' | sed 's/%bicg_step_max/'"$bicg_step_max"'/' \
		>  $My_path/$TDVP_XXZ_path/Thirring_TDVP_temp.cpp

		for ((i=0; i<$im_length; i=i+1))
		do
			printf "\n\n"
			echo "***************************************************************"
			echo "Create real time evolution exe file for tau=$tau."
			echo "***************************************************************"
			cat $My_path/$TDVP_XXZ_path/Thirring_TDVP_temp.cpp \
			| sed 's/%tau/'"$tau"'/' | sed 's/%im_minus_plus/'"$minus_plus_re"'/' \
			> $My_path/$TDVP_XXZ_path/Thirring_TDVP.cpp
			make_func; 
			tau=$(echo "scale=1; ${tau}+${im_save_step}*${im_del_t}/1" |bc -l \
			|sed -e 's/^\./0./' |sed -e 's/^-\./-0./' |sed -e 's/\.0//' )
		done
		tau=$tau_init	
		fi
		rm $My_path/$TDVP_XXZ_path/Thirring_TDVP_temp.cpp		

done

done
done
done
done

printf "\n\n"
