# Install script for directory: /home/DLIN/thirring/HungHaoTi/uni10/uni10

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/DLIN/thirring/HungHaoTi/uni10Lib")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "libraries" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libuni10.so.1.0.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libuni10.so.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libuni10.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/libuni10.so.1.0.0"
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/libuni10.so.1"
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/libuni10.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libuni10.so.1.0.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libuni10.so.1"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libuni10.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "libraries" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/libuni10.a")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "headers" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/DLIN/thirring/HungHaoTi/uni10/uni10/src/" FILES_MATCHING REGEX "/[^/]*\\.h[^/]*$")
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "examples" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/examples/" TYPE DIRECTORY FILES "/home/DLIN/thirring/HungHaoTi/uni10/uni10/examples/" REGEX "/CMakeLists[^/]*$" EXCLUDE)
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "common" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE FILE FILES
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/INSTALL"
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/README"
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/GPL"
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/LGPL"
    "/home/DLIN/thirring/HungHaoTi/uni10/uni10/ChangeLog"
    )
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "test" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/test/" TYPE DIRECTORY FILES "/home/DLIN/thirring/HungHaoTi/uni10/uni10/test/" REGEX "/CMakeLists[^/]*$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/src/cmake_install.cmake")
  include("/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/examples/cmake_install.cmake")
  include("/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/gtest-1.7.0/cmake_install.cmake")
  include("/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/test/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/DLIN/thirring/HungHaoTi/uni10/uni10/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
