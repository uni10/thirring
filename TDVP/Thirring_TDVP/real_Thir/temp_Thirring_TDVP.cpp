#include <iostream>
#include <ctime>
#include <complex>
#include<cmath>
using namespace std;
#include "uni10.hpp"
#include "uni10_lapack_wrapper.h"
using namespace uni10;

//measurement. Set the values of measurement as global variable
double e, s, Cc, Sz, Sx, err2_v, Arn_err, bicg_err, Sf;
vector<double> T1s, T2s;
int T_num=%spec_num; //The number of the largest spectum of the transfer matrix we want to see.
int N_ffc=%N_ffc; //The length of fermion-fermion correlator we want to see.

//*************************************accuracy********************************************

//Set the acurracy of the Arnoldi method, $$\epsilon=\|Mx-\lambda x\|_2$$
const double Arnacc=%Arnacc;
//Set the accuracy of bi-conjugate gradient stablize method to solve liner eqaution
const double bicgstab_acc=%bicg_acc;
//Acuuracy of mechine precision
const double mech_acc=1.0e-14;
//Set the tolearnce of the smallest element of the density matrix (for Dy_bond_bool==true)
const double Sf_acc=%Sf_acc;

//Set the largest iteration step for BiCGstab
const int Arnstep_max=%Arnstep_max;
//Set the largest iteration step for Arnoldi method
const int bicg_step_max=%bicg_step_max;

//*****************************************************************************************

//imaginary
const complex<double> image=complex<double>(0, 1.0);//imaginary number
//bool value to decide whether TDVP do imaginary time evolution,
//if TDVP do imaginray time evolution, im_bool=true,
//if TDVP do real time evolution, im_bool=false.
bool im_bool=%im_bool;
//bool value to decide whether TDVP expand the bond dimension if the smallest element of 
//the density matrix is too large,
//if Dy_bond_bool=true, TDVP will expand the bond dimension if the smallest element of the
//density matrix is too large (larger than Sf_acc),
//if Dy_bond_bool=false, TDVP will always fix the bond dimension.
bool Dy_bond_bool=%Dy_bond_bool;
complex<double> dir_vec;// evolution direction
int minus_plus=%im_minus_plus;//direction of imaginary time evolution
double tau=%tau;//For real time evolution, 
//If Dy_bond_bool=true, del_D is about the bond dimension we want to increase when
//the bond dimension expand, i.e. if del_D=10 and the bond dimension D=20, when the 
//smallest element of density matrix smaller than Sf_acc, the expand bond dimension
//D=20 --> D=30.
int del_D=%del_D;
//If Dy_bond_bool=true, D_max is the largest bond dimension we expand.
int D_max=%Dmax;

#include "./file_path.cpp"//path to save the tensor
#include "./Thir_TDVP_Tools.cpp"//This file is some tools for algoritm
#include "./MPO.cpp"//Create the Thirring model MPO and the infinite boundary tensor
#include "./measure.cpp"//Some observalbles
#include "./reTDVP_algorithm.cpp"//time evolution of TDVP algorithm by using RK4 to solve DE.

int main(){
//the direction of time evolution, for real time evolution, dir_vec= (0, 1),
//for imaginary time evolution, dir_vec= (1, 0) or (-1, 0).
	if(im_bool==true){dir_vec=complex<double>(1.0, 0)*(double)minus_plus;}
	else{dir_vec=image;}
//Set the bond dimension D of the uMPS
	int D=%D;		//Bond dimention. (If Dy_bond_bool==true, 
				//this is the initial bond dimension).
//Set delta_t and step number
	double del_t=%del_t;			//the length of the time step
	const int termi_step=%termi_step;	//total step we want to evolve
	const int del_step=%save_step;
	//For example, if del_step=20, we will save the current tensor every 20 time step

//Set parameter for Thirring
	double a=1.0;		//lattice constant
	double lambda=minus_plus*%lambda;	//The coefficint of penalty term
	double del_g=%del_f;	//coupling constant \Delta of final Hamiltonian
	double m=%m_f;		//mass of Thirring model for final Hamiltonian
	double mu=0.0;		//chemical potential
	double S_tar=0.0;	//S_target

//Set initial state parameter
	double delg_init=%del_i;//coupling constant \Delta of inital Hamiltonian
	double m_init=%m_i;	//mass of Thirring model for initial Hamiltonian

//print the corresponding parameter
	show_setting(a, lambda, delg_init, del_g, m_init, m, mu, S_tar, D,
	del_t, termi_step, del_step);

//Set the initial state  
	UniTensor A;
	UniTensor A_O_point(f_name("A_L", D, delg_init, m_init));
	UniTensor A_end_point(f_name("A_L", D, del_g, m));
	if(im_bool==true){
		UniTensor A1(f_name("A_L", D, delg_init, m_init));
		UniTensor C(f_name("C", D, delg_init, m_init));
		A=A_sym_fix(A1, C);
	}
	else{
		UniTensor A1(tdvp_f_name("A_tau", D, delg_init, del_g, m_init, m, tau));
		D=A1.bond(1).dim();
		A=A1;
	}

//Creat MPO and MPO_sub
	UniTensor MPO; vector<UniTensor> Ms;
	MPO=MPO_Thir(a, lambda, del_g, m, mu, S_tar);
	MPO_sub(MPO, Ms);
//Do TDVP
	iTDVP(A, A_O_point, A_end_point, MPO, Ms ,del_t, termi_step, del_step, delg_init, del_g, m_init, m);

//Create the file "re_done" if the TDVP has been done.
	string dir; stringstream ss(dir);
	if(im_bool==false){
		ss<<"touch "
		<<tdvp_f_name("others_tau", D, delg_init, del_g, m_init, m, tau)<<"re_done";
		system(ss.str().c_str());
	}
	cout<<"done"<<endl;
return 0;
}
