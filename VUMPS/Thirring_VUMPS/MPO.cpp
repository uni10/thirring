UniTensor MPO_Thir(double a, double lambda, double g, double m, double mu, double S_tar);
void conne(UniTensor& E, UniTensor A);
UniTensor connect(string Side, UniTensor E, UniTensor A, UniTensor O);
Matrix getMs_sub(Matrix MPO_mat, int col, int row, int num);
void MPO_sub(UniTensor& MPO, vector<UniTensor>& M_subs);
void bond_Block_make(UniTensor A_L, UniTensor A_R, vector<UniTensor> Ms, UniTensor& BL, UniTensor& BR, double& e0, Matrix& K_mat);

Matrix Thir_sub_mat(int n, double a, double lambda, double g, double m, double mu, double S_tar){
	double beta_n=(g/a)+(pow(-1.0,n)*m+mu)-2.0*lambda*S_tar; 
	double gamma=lambda*(0.25)+0.25*(g/a)+0.5*mu; 
	double kronecker_delta = (n%2)==0? 1:1;
	double elem[]={
1,      0,      	0,      0,      	0,      0,     		0,      0,      	0,      0,      	0,      0,
0,      1,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,
1,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0,      1,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,
0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0.5,    0,      	0,      0,      	0,      0,      	1,      0,      	0,      0,      	0,      0,
0,   -0.5,      	0,      0,      	0,      0,      	0,      1,      	0,      0,      	0,      0,

0.5,	0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,
0,   -0.5,      	0,      0,      	0,      0,      	0,      0,      	0,      0,      	0,      0,

0.5*beta_n+gamma,   0,  0,   (-1/a)*0.5, 	0,      0,      	lambda,      0,    	0.5*(g/a)*kronecker_delta, 0,1,      0,
0,(-0.5)*beta_n+gamma,	0,      0,	(-1/a)*0.5,     0,		0, (-1)*lambda,		0,  (-0.5)*(g/a)*kronecker_delta,0,      1
};
	Matrix Thir_mat(12, 12);
	Thir_mat.setElem(elem);
return Thir_mat;
}

void MPO_sub(UniTensor& MPO, vector<UniTensor>& M_subs){
	Matrix MPO_mat=MPO.getBlock();
	int num=4;
	int labels_loc[]={0, -1};
	Bond bdi(BD_IN, num); Bond bdo(BD_OUT, num);
	vector<Bond> bonds;
	bonds.push_back(bdi); bonds.push_back(bdo);
	UniTensor T(bonds);
	bonds.clear();
	T.setLabel(labels_loc);
	T.putBlock(getMs_sub(MPO_mat, 0, 0, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 1, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 2, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 3, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 4, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 1, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 2, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 3, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 4, 5, num)); M_subs.push_back(T);
	T.putBlock(getMs_sub(MPO_mat, 0, 5, num)); M_subs.push_back(T);
	RtoC(T);
return;
}

Matrix getMs_sub(Matrix MPO_mat, int col, int row, int num){
	Matrix M(num, num);
	for(int i=0; i<num; i++){for(int j=0; j<num; j++){
		M.at(i,j)=MPO_mat.at(row*num+i,col*num+j);
	}}
return M;
}

UniTensor MPO_Thir(double a, double lambda, double g, double m, double mu, double S_tar){
	Matrix Thir_sub_mat1=Thir_sub_mat(1, a, lambda, g, m, mu, S_tar);
	Matrix Thir_sub_mat2=Thir_sub_mat(2, a, lambda, g, m, mu, S_tar);
	Bond bdiv(BD_IN, 6); Bond bdip(BD_IN, 2); 
	Bond bdov(BD_OUT, 6); Bond bdop(BD_OUT, 2);
	vector<Bond> bonds;
	bonds.push_back(bdiv); bonds.push_back(bdip); 
	bonds.push_back(bdov); bonds.push_back(bdop);
	UniTensor T1(bonds); T1.putBlock(Thir_sub_mat1);
	UniTensor T2(bonds); T2.putBlock(Thir_sub_mat2);
	bonds.clear();
	int labels_or[]={0, 1, 2, 3};
	int labels_T2[]={2, -1, -2, -3};
	T2.setLabel(labels_T2);
	UniTensor T=contract(T1, T2);
	vector<int> T_comb_label1={1, -1};
	vector<int> T_comb_label2={3, -3};
	T.combineBond(T_comb_label1); T.combineBond(T_comb_label2);
	int per[]={0, 1, -2, 3};
	T.permute(per, 2); T.setLabel(labels_or); 
return T;
}

void bond_Block_make(UniTensor A_L, UniTensor A_R, UniTensor C, vector<UniTensor> Ms, UniTensor& BL, UniTensor& BR, double& e0){
	int d=A_L.bond(0).dim(); int D=A_L.bond(1).dim();
	int M=6;
	int labels_ATC[]={3, 4, 0}; 
	int labels_a[]={0, 1}; int labels_l[]={1, 3}; int labels_r[]={2, 4};
	UniTensor ALTC=A_L; ALTC.cTranspose(CTYPE); ALTC.setLabel(labels_ATC);
	
	UniTensor ARTC=A_R; ARTC.cTranspose(CTYPE); ARTC.setLabel(labels_ATC);
	Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D); Bond bdm(BD_IN, M);
	vector<Bond> bonds={bdi, bdo}; 
	UniTensor R(CTYPE, bonds); R.setLabel(labels_r);
	UniTensor L(CTYPE, bonds); L.setLabel(labels_r);
	bonds.clear();
	
	C*=(1.0/C.norm());
	Matrix CC_mat=C.getBlock(); CC_mat.cTranspose(CTYPE);	
	CC_mat=C.getBlock()*CC_mat;

	vector<UniTensor> Es;
	bonds.push_back(bdi); bonds.push_back(bdo);
	UniTensor I(CTYPE, bonds); 
	Matrix I_mat(CTYPE, D, D); I_mat.identity();
	I.putBlock(I_mat); I.setLabel(labels_l);
	bonds.clear();

	R.putBlock(CC_mat); R.setLabel(labels_r);
	I.setLabel(labels_l); //r.setLabel(labels_r);
	UniTensor E=I; Es.push_back(E); //Es.at(0).identity();
	int labels_locMPO[]={0, -1}; int labels_ATC2[]={3, 4, -1};
	Es.push_back(connect("L", Es.at(0), A_L, Ms.at(8)));	
	UniTensor C1; C1=connect("L", Es.at(0), A_L, Ms.at(7)); C1.permute(1);
	C1.setLabel(labels_a);
	Matrix mat=R.getBlock();
	E=BOP_make_BiCGSTAB(mat, A_L, ALTC, C1, I, R);	
	Es.push_back(E);

	Es.push_back(connect("L", Es.at(0), A_L, Ms.at(6)));
	Es.push_back(connect("L", Es.at(0), A_L, Ms.at(5)));
	C1=connect("L", Es.at(4), A_L, Ms.at(1))+connect("L", Es.at(3), A_L, Ms.at(2))+connect("L", Es.at(2), A_L, Ms.at(3))+
	connect("L", Es.at(1), A_L, Ms.at(4))+connect("L", Es.at(0), A_L, Ms.at(9));
	C1.permute(1); C1.setLabel(labels_a);

	E=BOP_make_BiCGSTAB(mat, A_L, ALTC, C1, I, R);
	Es.push_back(E);
 
	CC_mat=C.getBlock(); CC_mat.cTranspose(CTYPE);
	CC_mat=CC_mat*C.getBlock(); CC_mat.transpose();
	L.putBlock(CC_mat);
	I.setLabel(labels_r);
	vector<UniTensor> Fs; 
	 
	UniTensor F=I;
	F.setLabel(labels_r);

	Fs.push_back(F);
 
	Fs.push_back(connect("R", Fs.at(0), A_R, Ms.at(1)));
	Fs.push_back(connect("R", Fs.at(0), A_R, Ms.at(2)));
	UniTensor C2;	
	C2=connect("R", Fs.at(0), A_R, Ms.at(3));
	C2.permute(1); C2.setLabel(labels_a);
	F=BOPT_make_BiCGSTAB(mat, A_R, ARTC, C2, L, I);  //F1.transpose();
	Fs.push_back(F);

	Fs.push_back(connect("R", Fs.at(0), A_R, Ms.at(4)));

	C2=connect("R", Fs.at(0), A_R, Ms.at(9))+connect("R", Fs.at(1), A_R, Ms.at(5))+connect("R", Fs.at(2), A_R, Ms.at(6))+
	connect("R", Fs.at(3), A_R, Ms.at(7))+connect("R", Fs.at(4), A_R, Ms.at(8));
	C2.permute(1); C2.setLabel(labels_a);
	F=BOPT_make_BiCGSTAB(mat, A_R, ARTC, C2, L, I);  
	Fs.push_back(F);
	R.setLabel(labels_a); L.setLabel(labels_a);
	
	e0=0.5*(contract(C1, R).at(CTYPE, 0).real());
	e0+=0.5*(contract(C2, L).at(CTYPE, 0).real());
	e0*=0.5;
	//cout<<setprecision(15)<<e0<<endl;
       
	bonds.push_back(bdm); bonds.push_back(bdo); bonds.push_back(bdo);
	UniTensor Bl_temp(CTYPE, bonds); Bl_temp.permute(2);
	bonds.clear();
	//Bl_temp.printDiagram();
	BL=Bl_temp;
	complex<double> *elem_t;
	elem_t=(complex<double>*)malloc(6*(D*D)*sizeof(complex<double>));
	for(int i=0; i<M; i++){
		memcpy(elem_t+i*(D*D), Es.at(M-1-i).getElem(CTYPE), (D*D)*sizeof(complex<double>));
	}
	Matrix E_mat(CTYPE, M*D, D); E_mat.setElem(elem_t);
	free(elem_t); 
	BL.putBlock(E_mat);
	int per_BL[]={1, 0, 2}; BL.permute(per_BL, 0);
	int labels_BL[]={1, -2, 3}; BL.setLabel(labels_BL);
 	
	BR=Bl_temp;
	elem_t=(complex<double>*)malloc(6*(D*D)*sizeof(complex<double>));
	for(int i=0; i<M; i++){
		memcpy(elem_t+i*(D*D), Fs.at(i).getElem(CTYPE), (D*D)*sizeof(complex<double>));
	}

	Matrix F_mat(CTYPE, M*D, D); F_mat.setElem(elem_t);
	free(elem_t); 
	BR.putBlock(F_mat);
	int per_BR[]={1, 0, 2}; BR.permute(per_BL, 3);
	int labels_BR[]={2, -3, 4}; BR.setLabel(labels_BR);//BR.printDiagram();
return;	
}

UniTensor connect(string Side, UniTensor E, UniTensor A, UniTensor O){
	int labels_l[]={1, 3}; int labels_r[]={2, 4};
	UniTensor ATC=A; ATC.cTranspose(CTYPE);
	int labels_ATC2[]={3, 4, -1}; ATC.setLabel(labels_ATC2);
	UniTensor EOA;
	if(Side == "L"){
		E.setLabel(labels_l);
		EOA=contract(E, A); EOA=contract(EOA, O); EOA=contract(EOA, ATC);
		int per[]={2, 4}; EOA.permute(per, 1); 
	}
	else if(Side == "R"){
		E.setLabel(labels_r);
		EOA=contract(E, A); EOA=contract(EOA, O); EOA=contract(EOA, ATC);
		int per[]={1, 3}; EOA.permute(per, 1); 
	}
	int labels_a[]={0, 1}; EOA.setLabel(labels_a);
return EOA;
}
