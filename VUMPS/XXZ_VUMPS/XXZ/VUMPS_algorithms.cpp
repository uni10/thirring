void VUMPS(UniTensor& A_C, UniTensor& A_L, UniTensor& A_R, UniTensor& C, UniTensor MPO, vector<UniTensor> Ms, double& e, double& del_g, double& m);

void VUMPS(UniTensor& A_C, UniTensor& A_L, UniTensor& A_R, UniTensor& C, UniTensor MPO, vector<UniTensor> Ms, double& e, double& del_g, double& m){
	UniTensor BL, BR;
	double err_p=2.0, err_p_old=10.0, err_H=1.0;
	int dy_bo_step=0; int dy_step=0; bool Dy_bd=true;
	int d=A_L.bond(0).dim(); int D=A_L.bond(1).dim();
	int D_init=D; int D_tilde;
	UniTensor A_L_init, A_R_init, A_C_init, C_init;

	
	cout<<setw(5)<<"step"<<setw(5)<<"D"<<setw(10)<<"error"<<setw(23)<<"<dH>"<<endl;
//Bond dynamic
	while(Dy_bd==true){
		if(D==D_max)dy_bo_step++; 
		D_tilde=D*(d)>D_max?D_max:D*(d);
		int step=0; int step_re=0;
		double e, e_old, err_L, err_R;

	//VUMPS aigorithm
		while(err_p>goal_acc){
			step++;
			e_old=e;
			bond_Block_make(A_L, A_R, C, Ms, BL, BR, e);
		//update
			A_C=update_AC(A_C, BL, BR, MPO, e, err_H); 
			C=update_C(C, BL, BR,  e, err_H); 
			update_AR_opt(A_R, A_C, C, err_R);
			update_AL_opt(A_L, A_C, C, err_L);
			err_p_old=err_p;
			err_p=max(err_L, err_R);

		//reset
			if(abs(err_p)>abs(err_p_old)){step_re++;}
			if(step_re>20){
				cout<<endl<<"******************reset********************"<<endl;
				step_re=0;
				D=D_init; dy_bo_step=0;
				As_init(d, D, A_L, A_R, A_C, C); 
				bond_Block_make(A_L, A_R, C, Ms, BL, BR, e);
			}
			cout<<setw(5)<<step<<setw(5)<<D<<setprecision(15)<<setw(25)<<err_p
			<<setw(25)<<fabs(e-e_old)<<endl;
			err_H=0.01*err_p; 
		}
	//measure
		s=SC_meas(C);
		Cc=single_meas(A_C, Chi_cond_mat());
		Sz=single_meas(A_C, Szt_mat());
		err1_v=err1(A_C, A_R, BL, BR, MPO, e, err_H);
		cout<<endl<<"(e, s, Cc,Sz)= "<<"("<<setprecision(15)<<e<<","<<s<<","<<Cc<<","<<Sz<<")"<<endl;
	//save
		if(D==D_max){
			output_fun(D, del_g, m, e, s, Cc, Sz, err1_v, err2_v, A_L, C);
		}
	
	//dynamic bond logic
		if(dy_step==0){A_L_init=A_L; A_R_init=A_R; A_C_init=A_C; C_init=C;}
		UniTensor ALTC=A_L; ALTC.cTranspose(CTYPE);
		UniTensor ARTC=A_R; ARTC.cTranspose(CTYPE);
		UniTensor NL=NL_make(ALTC);
		UniTensor NR=NR_make(ARTC);
		if(D<D_max)Dy_bond_update(D_tilde, A_C, A_L, A_R, C, MPO, BL, BR, NL, NR);
		err_H*=100; err_p=1.0;
		D=D_tilde;
		Dy_bd=dy_bo_step==1?0:1; 
		dy_step++;
	}
	A_L=A_L_init; A_R=A_R_init; A_C=A_C_init; C=C_init;
	D=D_init; Dy_bd=true;

return;
}
