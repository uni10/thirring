string f_name(string typ, int D, double g, double m);
string tdvp_f_name(string typ, int D, double delg_init, double del_g, double m_init, double m, double t);

string f_name(string typ, int D, double g, double m){
	string s;
	stringstream ss(s);
	if(typ=="dir"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m;
	}
	if(typ=="data"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/data";
	}
	if(typ=="A_L"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/"<<"AL_D"<<D<<"g"<<g<<"m"<<m<<"tens";	
	}
	if(typ=="C"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/"<<"C_D"<<D<<"g"<<g<<"m"<<m<<"tens";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/";
	}
	else{;}
return ss.str();
}

string tdvp_f_name(string typ, int D, double delg_init, double del_g, double m_init, double m, double t){
	string s;
	stringstream ss(s);
	if(Dy_bond_bool==true){
	if(typ=="dir1"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m;
	}
	if(typ=="dir2"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved";
	}
	if(typ=="dir_tau"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau;
	}
	if(typ=="A"){ 
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved"<<"/Dy_bond_del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_t"<<t<<"A_tens";
	}
	if(typ=="A_tau"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau
		<<"/Dy_bond_del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_tau"<<tau<<"A_tens";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	if(typ=="others_tau"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Dy_bond/Dmax"<<D_max<<"/del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	else{;}
	}
	else{

	if(typ=="dir1"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m;
	}
	if(typ=="dir2"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved";
	}
	if(typ=="dir_tau"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau;
	}
	if(typ=="A"){ 
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m
		<<"/tau"<<tau<<"/tens_saved"<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_t"<<t<<"A_tens";
	}
	if(typ=="A_tau"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau
		<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"_tau"<<tau<<"A_tens";
	}
	if(typ=="data"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/t"<<t
 		<<"/data";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	if(typ=="others_tau"){
		ss<<"%DATA_DIR/XXZ_data/tdvp_tens/Fix_bond/D"<<D<<"/D"<<D<<"del_g"<<delg_init<<"to"<<del_g<<"m"<<m_init<<"to"<<m<<"/tau"<<tau<<"/";
	}
	else{;}

	}

return ss.str();
}

