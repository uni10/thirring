UniTensor initial_A(int d, int D);
UniTensor ATC_l_root_make(UniTensor ATC, UniTensor l_root);
UniTensor VL_make(UniTensor ATC_l_root, int D, int d);
UniTensor VR_make(UniTensor ATC, UniTensor r_root);
UniTensor B_make(UniTensor l_root_inv, UniTensor r_root_inv, UniTensor X, UniTensor VL);
void sym_gauge(UniTensor& A, UniTensor& l, UniTensor& r, Matrix& l_mat, Matrix& r_mat, UniTensor& l_root, UniTensor& l_root_inv);
void Canonical(UniTensor A, UniTensor& gamma, UniTensor& lambda, Matrix& l_mat, Matrix& r_mat);
void R_Arn_Tens_meth(string max_type, UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval);
void L_Arn_Tens_meth(string max_type, UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval);
UniTensor root_make(UniTensor T);
UniTensor K_OP(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor r, UniTensor& l);
UniTensor K_OPT(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor& r, UniTensor l);
UniTensor BOP_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r);
UniTensor BOPT_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r);
void eigcal_sub(UniTensor A, vector<UniTensor> Ms, UniTensor& BL, UniTensor& BR, UniTensor l, UniTensor r, double& e0);
UniTensor connect(string Side, UniTensor E, UniTensor A, UniTensor O);
complex<double> unidot(UniTensor a, UniTensor b);
void show_setting( double a, double lambda, double delg_init, double del_g, double m_init, double m,
	double h, double S_tar, double D, double del_t, const int termi_step, const int del_step);

bool step_conti=true;

UniTensor initial_A(int d, int D){
	Bond bdi(BD_IN, d), bdo(BD_OUT, D);
	vector<Bond> bonds;
	bonds.push_back(bdi); bonds.push_back(bdo); bonds.push_back(bdo);
	UniTensor A(CTYPE, bonds);
	UniTensor A2(CTYPE, bonds);
	bonds.clear();
	for(int i=0; i<1; i++){A.randomize(); A2.randomize();}
	A=A+(-1.0)*A2;
return A;
}

UniTensor ATC_l_root_make(UniTensor ATC, UniTensor l_root){
	int label_ATC_2[]={1,2,-1};ATC.setLabel(label_ATC_2);int labels_ATC_l_root[]={2,0,-1};
        UniTensor ATC_l_root;ATC_l_root=contract(l_root,ATC);ATC_l_root.permute(labels_ATC_l_root,1);
return ATC_l_root;
}

UniTensor VL_make(UniTensor ATC_l_root, int D, int d){
	int m=D;
	int n=D*d;
        int info;
        int lwork=-1;//2*min(m,n)+max(m,n);
	complex <double> workt;
	complex<double> *u, *vt, *elem;
	double *s, *rwork;
	u=(complex<double>*)malloc((m*m)*sizeof(complex<double>));
	vt=(complex<double>*)malloc((n*n)*sizeof(complex<double>));
	s=(double*)malloc((m*n)*sizeof(double));
	rwork=(double*)malloc(5*m*sizeof(double));
	elem=(complex<double>*)malloc(m*n*sizeof(complex<double>));
		
        Matrix ATC_l_root_Tmat=ATC_l_root.getBlock(CTYPE).transpose();
        for (int i=0;i<m*n;i++){elem[i]=ATC_l_root_Tmat.at(CTYPE,i);}

        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,&workt,&lwork,rwork,&info);
        lwork = (int)workt.real();
        complex<double> *work = (complex<double>*)malloc(lwork*sizeof(std::complex<double>));
        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,work,&lwork,rwork,&info);

        Matrix U_ATC_l_root(CTYPE,m,m);U_ATC_l_root.setElem(u);U_ATC_l_root.transpose();
        Matrix S_ATC_l_root(CTYPE,m,n,true);S_ATC_l_root.setElem(s);
        Matrix VT_ATC_l_root(CTYPE,n,n);VT_ATC_l_root.setElem(vt);VT_ATC_l_root.transpose();
        Matrix VLT_mat(CTYPE,n-m,n);
        for(int i=0;i<n*(n-m);i++){VLT_mat.at(CTYPE,i)=VT_ATC_l_root.at(CTYPE,m*n+i);}
        Matrix VL_mat=VLT_mat.cTranspose(CTYPE);VLT_mat.cTranspose(CTYPE);
	free(u); free(vt); free(s); free(rwork); free(elem);

        Bond bd_VL_0(BD_IN,D);Bond bd_VL_1(BD_IN,d);Bond bd_VL_2(BD_OUT,D*(d-1));
        vector <Bond> bonds_VL;
        bonds_VL.push_back(bd_VL_0);bonds_VL.push_back(bd_VL_1);bonds_VL.push_back(bd_VL_2);
        UniTensor VL(CTYPE,bonds_VL,"VL");VL.putBlock(VL_mat);
        int label_VL[]={0,-1,3};VL.setLabel(label_VL);

return VL;
}

UniTensor VR_make(UniTensor ATC, UniTensor r_root){
	int D=r_root.bond(0).dim();
	int d=ATC.bond(2).dim();
	int m=D;
	int n=D*d;
        int info;
        int lwork=-1;//2*min(m,n)+max(m,n);
	complex<double> *u, *vt, *elem;
	double *s, *rwork;
	u=(complex<double>*)malloc((m*m)*sizeof(complex<double>));
	vt=(complex<double>*)malloc((n*n)*sizeof(complex<double>));
	s=(double*)malloc((m*n)*sizeof(double));
	rwork=(double*)malloc(5*m*sizeof(double));
	elem=(complex<double>*)malloc(m*n*sizeof(complex<double>));
        complex <double> workt;       
	
	int labels_r_root_temp[]={-1, 4}; r_root.setLabel(labels_r_root_temp);
	UniTensor ATC_r_root; 
	ATC_r_root=contract(ATC, r_root, true);
	int labels_ATC_r_root[]={3, -1, 0};
	ATC_r_root.permute(labels_ATC_r_root, 1);
	
        Matrix ATC_r_root_Tmat=ATC_r_root.getBlock(CTYPE).transpose();
        for (int i=0;i<m*n;i++){elem[i]=ATC_r_root_Tmat.at(CTYPE,i);}

        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,&workt,&lwork,rwork,&info);
        lwork = (int)workt.real();
        complex<double> *work = (complex<double>*)malloc(lwork*sizeof(std::complex<double>));
        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,work,&lwork,rwork,&info);

        Matrix U_ATC_r_root(CTYPE,m,m);U_ATC_r_root.setElem(u);U_ATC_r_root.transpose();
        Matrix S_ATC_r_root(CTYPE,m,n,true);S_ATC_r_root.setElem(s);
        Matrix VT_ATC_r_root(CTYPE,n,n);VT_ATC_r_root.setElem(vt);VT_ATC_r_root.transpose();
        Matrix VRT_mat(CTYPE,n-m,n);
        for(int i=0;i<n*(n-m);i++){VRT_mat.at(CTYPE,i)=VT_ATC_r_root.at(CTYPE,m*n+i);}
        Matrix VR_mat=VRT_mat; VR_mat.cTranspose(CTYPE);
	free(u); free(vt); free(s); free(rwork); free(elem);

        Bond bd_VR_0(BD_IN,D);Bond bd_VR_1(BD_IN,d);Bond bd_VR_2(BD_OUT,D*(d-1));
        vector <Bond> bonds_VR;
        bonds_VR.push_back(bd_VR_0);bonds_VR.push_back(bd_VR_1);bonds_VR.push_back(bd_VR_2);
        UniTensor VR(CTYPE,bonds_VR,"VR");VR.putBlock(VR_mat); VR.transpose();
return VR;
}

UniTensor B_make(UniTensor l_root_inv, UniTensor r_root_inv, UniTensor X, UniTensor VL){
	Network B_net("./Diagrams/B.net");
	B_net.putTensor("l_root_inv",l_root_inv);B_net.putTensor("r_root_inv",r_root_inv);B_net.putTensor("VL",VL);
        B_net.putTensor("X",X);
        UniTensor B;B=B_net.launch();int label_B[]={3,1,6};B.permute(label_B,1);
return B;
}

double err2(UniTensor& A, UniTensor& MPO, UniTensor& BL, UniTensor& BR, UniTensor NL, UniTensor NR){
        int D=A.bond(1).dim();
	UniTensor A2=A;
        int labels_A2[]={-1, 2, 3}; A2.setLabel(labels_A2);
        UniTensor A2_C=contract(A, A2);
        int per1[]={0, 1, -1, 3};
        A2_C.permute(per1, 4);
        Network Lann("./Diagrams/Lanczos_A2C.net");
        Lann.putTensor("BL", BL); Lann.putTensor("WC1", MPO);
        Lann.putTensor("WC2", MPO); Lann.putTensor("BR", BR);
        Lann.putTensor("A2_C", A2_C);
        UniTensor B2=Lann.launch();
        int labels_ATC[]={3, 4, 0};
        UniTensor NLTC=NL; NLTC.cTranspose(CTYPE);
        UniTensor NRTC=NR; NRTC.cTranspose(CTYPE);
        int labels_NLTC[]={1, 3, -3}; int labels_NRTC[]={4, -4, 2};
        NLTC.setLabel(labels_NLTC); NRTC.setLabel(labels_NRTC);
        B2=contract(B2, NLTC); B2=contract(B2, NRTC);//B2.printDiagram();
return pow(B2.norm(),2);
}


void sym_gauge(UniTensor& A, UniTensor& l, UniTensor& r, Matrix& l_mat, Matrix& r_mat, UniTensor& l_root, UniTensor& l_root_inv){
	clock_t Start, Finish; double Duration;
	Start=clock();
	int D=A.bond(1).dim(); int d=A.bond(0).dim();

	UniTensor gamma, lambda;
	Canonical(A, gamma, lambda, l_mat, r_mat);
	int labels_l[]={1, 3};
	int labels_r[]={2, 4};
	double lambda_norm=lambda.norm();
	l=lambda*(1.0/lambda_norm);
	l.setLabel(labels_l);
	r=l; r.setLabel(labels_r);
	
	Matrix lambda_sq_mat(CTYPE, D, D, true);

	for(int i=0; i<D; i++){
		lambda_sq_mat.at(CTYPE, i)=pow(lambda.getBlock().at(CTYPE,i+D*i),0.5);
	}
	vector<Bond> bonds;
	Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);	
	bonds.push_back(bdi); bonds.push_back(bdo);
	UniTensor lambda_sq(CTYPE, bonds, "lambda_sq");
	lambda_sq.putBlock(lambda_sq_mat);
	int labels_lbs1[]={-1, 1}; int labels_lbs2[]={2, -2};
	l_root=lambda_sq; l_root *= pow(lambda_norm, -0.5);
	for(int i=0; i<D; i++){
                lambda_sq_mat.at(CTYPE, i)=pow(lambda_sq_mat.at(CTYPE,i),-1.0);
        }
	l_root_inv=l_root; l_root_inv.putBlock(lambda_sq_mat);
	l_root_inv *= pow(lambda_norm, 0.5);
	lambda_sq.setLabel(labels_lbs1); gamma=contract(gamma, lambda_sq);
	lambda_sq.setLabel(labels_lbs2); gamma=contract(gamma, lambda_sq);
	int label_gamma2[]={0, 1, 2};
	gamma.setLabel(label_gamma2); gamma.permute(1);
	A=gamma; //A.printDiagram();
	Finish=clock();
	Duration =(double)(Finish-Start)/CLOCKS_PER_SEC;
	//cout<<"symmetry gauge spend "<<	Duration<<" seconds."<<endl;
	
return ;
}

void Canonical(UniTensor A, UniTensor& gamma, UniTensor& lambda, Matrix& l_mat, Matrix& r_mat){
//please put in the uni tensor in this order: physical bond(0) => left virtual bond(1) => right virtual bond(2)
	int labels_A[]={0, 1, 2}; A.setLabel(labels_A); A.permute(1);
	int D=A.bond(1).dim(); int d=A.bond(0).dim();
	
//make gamma-lambda representation or A
	int label_gamma1[]={1, 0, 2}; int label_gamma2[]={0, 1, 2};
        int labels_A2[]={1, 2, 0}; A.permute(labels_A2, 1);
        vector<Matrix> SVDs; SVDs=(A.getBlock()).svd();
        UniTensor gamma_temp(CTYPE, A.bond()); gamma=gamma_temp;
	gamma.setLabel(labels_A2); //gamma.printDiagram();
        Matrix gamma_mat; gamma_mat=SVDs.at(2);
        gamma.putBlock(gamma_mat); //gamma.printDiagram();
        gamma.permute(label_gamma1, 2);
        gamma_mat=(gamma.getBlock()*SVDs.at(0));
        gamma.putBlock(gamma_mat); //gamma.printDiagram();
        gamma.permute(label_gamma2, 1);
        Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
	vector<Bond> bonds;
        bonds.push_back(bdi); bonds.push_back(bdo);
        UniTensor lambda_temp(CTYPE, bonds, "lambda"); lambda=lambda_temp;
        bonds.clear();
        lambda.putBlock(SVDs.at(1));

//Make canonical form for A
        int labels_lambda[]={1, 3}; lambda.setLabel(labels_lambda);
	int labels_A3[]={0, 3, 2};
	A=contract(gamma, lambda); A.permute(labels_A3, 1);
	A.setLabel(label_gamma2);

	complex<double> eigval;
	UniTensor ATC; ATC=A; ATC.cTranspose();
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
	bonds.push_back(A.bond(1)); bonds.push_back(A.bond(2));
	UniTensor l(CTYPE, bonds, "l"); l.permute(1); l.putBlock(l_mat); 
	UniTensor r(CTYPE, bonds, "r"); r.permute(1); r.putBlock(r_mat); 
	bonds.clear();
	l *= (1.0)/(l.getBlock().norm());  r *= (1.0)/(r.getBlock().norm());
	int label_l[]={1,3}; int label_r[]={2, 4}; 
	l.setLabel(label_l); r.setLabel(label_r);
	
	L_Arn_Tens_meth("R", A, ATC, l, eigval);	
	l_mat=l.getBlock();
	double ita; ita=real(eigval);

	//Matrix l_mat; l_mat=l.getBlock(); //l_mat.transpose();
	vector <Matrix> Eighs; Eighs=l.getBlock().eigh();
	if(Eighs.at(0).at(D-1,D-1)<0){(Eighs.at(0))*=(-1.0); l*=(-1.0);}
	Matrix Pt=Eighs.at(1); Matrix P=Pt; P.cTranspose(CTYPE);
	RtoC(Eighs.at(0));
        for(int i=0; i<D; i++){
                Eighs.at(0).at(CTYPE, i)=pow(Eighs.at(0).at(CTYPE,i), 0.5); 
        }
	UniTensor L; L=l;
	L.putBlock(P*Eighs.at(0)*Pt); //A.printDiagram();
	int labels_L[]={1, -1};	L.setLabel(labels_L);
	UniTensor L_inv; L_inv=L;
	Matrix L_inv_mat;
	for(int i=0; i<D; i++){Eighs.at(0).at(CTYPE,i)=1.0/(Eighs.at(0).at(CTYPE,i));} 
	L_inv.putBlock(P*Eighs.at(0)*Pt);
	
	int labels_lambda2[]={2, 3}; lambda.setLabel(labels_lambda2);
	A=contract(gamma, lambda); 
	int labels_A4[]={0, 1, 3}; A.permute(labels_A4, 1);
	A.setLabel(label_gamma2);
	ATC=A; ATC.cTranspose(); ATC.setLabel(labels_ATC);
	
	R_Arn_Tens_meth("R", A, ATC, r, eigval);
	r_mat=r.getBlock();
	Eighs.clear();
	Eighs=r.getBlock().eigh();//cout<<Eighs.at(0)<<endl;
	if(Eighs.at(0).at(D-1,D-1)<0){(Eighs.at(0))*=(-1.0); r*=(-1.0);}
	Matrix Tt=Eighs.at(1); Matrix T=Tt; T.cTranspose(CTYPE);
	UniTensor R; R=r;
	RtoC(Eighs.at(0));
        for(int i=0; i<D; i++){
                Eighs.at(0).at(CTYPE, i)=pow(Eighs.at(0).at(CTYPE,i), 0.5); 
        }


	R.putBlock(T*Eighs.at(0)*Tt); //A.printDiagram();
	int labels_R[]={2, -2}; R.setLabel(labels_R);
	UniTensor R_inv; R_inv=R;
	Matrix R_inv_mat;
	for(int i=0; i<D; i++){Eighs.at(0).at(CTYPE,i)=1.0/(Eighs.at(0).at(CTYPE,i));}
	R_inv.putBlock(T*Eighs.at(0)*Tt);

	int labels_lambda3[]={1, 2};lambda.setLabel(labels_lambda3);	
	lambda=contract(lambda, L); lambda=contract(lambda, R); 
	lambda.setLabel(labels_lambda);
	SVDs.clear();
	SVDs=lambda.getBlock().svd();
	lambda.putBlock(SVDs.at(1));
	UniTensor U(CTYPE, l.bond()); UniTensor V(CTYPE, l.bond());
	U.putBlock(SVDs.at(0)); V.putBlock(SVDs.at(2));
	
	Network gamma_net("./Diagrams/gamma.net");
	gamma_net.putTensor("gam", gamma); gamma_net.putTensor("U", U); gamma_net.putTensor("V", V);
	gamma_net.putTensor("R_inv", R_inv); gamma_net.putTensor("L_inv", L_inv);
	gamma=gamma_net.launch();
	gamma.setLabel(labels_A);		
	gamma.permute(1);
	gamma *=pow(ita, -0.5);

return;
}

void R_Arn_Tens_meth(string maxtype, UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval){
	clock_t Start, Finish;	double Duration;
	Start=clock();

	int Minstep=2; 
	int first_step=0;

        bool ArnConverge=false; bool Arn_step2=false;
	double err=1.0;
        int Arnstep=0; int Arniter=0;
        int D=pow(A.bond(1).dim(),2); 
        int idx=0;
        eigval=complex<double> (1.0, 0);
	complex<double> eigval_old;
        vector<Matrix> Qs;
	Matrix eigvec(CTYPE, D, 1); Matrix eigvec_old;
        Matrix Hma(CTYPE, 2, 2);Matrix Hma2 ;
	vector<Matrix> Eigs;
	int labels_A[]{0, 1, 2 }; int labels_ATC[]={3, 4, 0};
	A.setLabel(labels_A); ATC.setLabel(labels_ATC);
        eigTens.permute(2);
	UniTensor eigTens_temp(CTYPE, eigTens.bond());
	eigTens*=(1.0/eigTens.norm());
	eigvec=eigTens.getBlock();
	int labels_r1[]={2, 4};
	eigTens.setLabel(labels_r1);
	eigTens_temp.setLabel(labels_r1);
	Qs.push_back(eigvec);
	while((ArnConverge==false)&&(Arnstep<=Arnstep_max)){
		Arnstep++; //cout<<"Arnstep="<<Arnstep<<endl;
                eigTens.putBlock(eigvec);
                eigTens *= (1.0/eigTens.norm());
                eigval_old=eigval;
		eigTens=contract(A, eigTens, true); eigTens=contract(eigTens, ATC, true); 
		eigTens.setLabel(labels_r1); eigTens.permute(2);
		eigvec=eigTens.getBlock();
		Hma.resize(CTYPE, Arnstep+1, Arnstep+1) ;
                for(int i=0; i<Arnstep; i++){//cout<<"i="<<i<<endl;
                        Hma.at(CTYPE, i*(Arnstep+1)+(Arnstep-1))=((Qs.at(i).cTranspose(CTYPE))*eigvec).at(CTYPE,0);
                        Qs.at(i).cTranspose(CTYPE);
                        eigvec=eigvec+(-1.0)*Hma.at(CTYPE, i*(Arnstep+1)+Arnstep-1)*Qs.at(i);
		}
                Hma.at(CTYPE, (Arnstep+1)*(Arnstep)+Arnstep-1)=eigvec.norm();
                eigvec *= (1.0/Hma.at(CTYPE, (Arnstep+1)*(Arnstep)+Arnstep-1));
		Qs.push_back(eigvec);
		if(Arnstep>=(first_step)){
                        Hma2=Hma; Hma2.resize(CTYPE, Arnstep, Arnstep); //cout<<Hma2<<endl;
                        Eigs=Hma2.eig();
                        eigval=Eigs.at(0).at(CTYPE, 0);
			if(maxtype=="R"){
                        	for(int i=0; i<Eigs.at(0).col(); i++){
                                	if((real(eigval)-real(Eigs.at(0).at(CTYPE, i)))<mech_acc){idx=i; eigval=Eigs.at(0).at(CTYPE, i);}
                        	}
			}
			else if(maxtype=="N"){
                        	for(int i=0; i<Eigs.at(0).col(); i++){
                                	if((norm(eigval)-norm(Eigs.at(0).at(CTYPE, i)))<mech_acc){idx=i; eigval=Eigs.at(0).at(CTYPE, i);}
                        	}
			}

                	if(Arnstep==D){ArnConverge=true;}
                	else if(Arn_step2==true){
				Matrix P; P=Eigs.at(1).inverse(); P.transpose();
	                        Matrix eigvec_temp(CTYPE, D, 1);
        	                eigvec_temp.set_zero();
                	        for(int i=0; i<Arnstep; i++){eigvec_temp+=P.at(CTYPE, i+Arnstep*idx)*Qs.at(i);}
                        	eigvec_temp *=(1.0/eigvec_temp.norm());
				eigTens_temp.putBlock(eigvec_temp);
				eigTens_temp=contract(A, eigTens_temp, true); eigTens_temp=contract(eigTens_temp, ATC, true);
				eigTens_temp.setLabel(labels_r1); eigTens_temp.permute(2);
				err=(eigTens_temp.getBlock()+(-1.0)*eigval*eigvec_temp).norm(); //cout<<err<<endl;
				if(((err<=Arnacc)&&(Arnstep>Minstep))||(Arnstep==D)){ArnConverge=true;}
                	}
                	if(Arn_step2==false){
                        	err=norm(eigval-eigval_old); //cout<<err<<endl;
                       		if(((err<=mech_acc)&&(Arnstep>Minstep))||(Arnstep==D)){Arn_step2=true;}
                	}
                }
        }
        Matrix T; T=Eigs.at(1).inverse(); T.transpose();
        eigvec.set_zero();
	Arn_err=err;
        for(int i=0; i<Arnstep; i++){eigvec+=T.at(CTYPE, i+Arnstep*idx)*Qs.at(i);}
        eigvec *=(1.0/eigvec.norm()); eigTens.putBlock(eigvec); eigTens.permute(1);
	//eigTens_temp=eigTens;
	//eigTens_temp=contract(A, eigTens_temp, true); eigTens_temp=contract(eigTens_temp, ATC, true);
	//eigTens_temp.setLabel(labels_r1); eigTens_temp.permute(2);
	double theta_angle; theta_angle=atan(((eigTens.getBlock().at(CTYPE,0)).imag())/((eigTens.getBlock().at(CTYPE,0)).real()));
	complex<double> phase; phase=complex<double>(cos(theta_angle),(-1.0)*sin(theta_angle));
	eigTens=eigTens*phase;
        //cout<<"eigtest= "<<setprecision(15)<<(eigTens_temp.getBlock()+(-1.0)*eigval*eigvec).norm()<<endl;
        //cout<<"Arnstep= "<<Arnstep<<endl;
        //cout<<"eigval=" <<setprecision(15)<<eigval<<endl;
	
	Finish=clock();
	Duration=(double)(Finish-Start)/CLOCKS_PER_SEC;
	//cout<<"Arnoldi Tensor type spend "<<Duration<<" sconds."<<endl;
return;
}

void L_Arn_Tens_meth(string maxtype, UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval){
	int labels_l1[]={1, 3}; eigTens.setLabel(labels_l1);
	int labels_Al[]={0, 2, 1}; int labels_ATCl[]={4, 3, 0};
	A.permute(labels_Al, 1); ATC.permute(labels_ATCl, 2);
	R_Arn_Tens_meth(maxtype, A, ATC, eigTens, eigval);
	eigTens.setLabel(labels_l1);
return;
}


void R_Arn_Tens_meth2(string maxtype, int eig_ord, UniTensor A, UniTensor ATC, vector<UniTensor>& ls, vector<UniTensor>& rs, vector<complex<double>>& eigval_s){
	clock_t Start, Finish;	double Duration;
	Start=clock();
	double Arnacc2=1.0e-10;
	UniTensor eigTens=rs.at(0); complex<double> eigval=complex<double>(1.0, 0);

	int Minstep=2; 
	int first_step=0;

        bool ArnConverge=false; bool Arn_step2=false;
	double err=1.0;
        int Arnstep=0; int Arniter=0;
        int D=pow(A.bond(1).dim(),2); 
        int idx=0;
	complex<double> eigval_old;
        vector<Matrix> Qs;
	Matrix eigvec(CTYPE, D, 1); Matrix eigvec_old;
        Matrix Hma(CTYPE, 2, 2);Matrix Hma2 ;
	vector<Matrix> Eigs;
	int labels_A[]{0, 1, 2 }; int labels_ATC[]={3, 4, 0};
	A.setLabel(labels_A); ATC.setLabel(labels_ATC);
        eigTens.permute(2);
	UniTensor eigTens_temp(CTYPE, eigTens.bond());
	eigTens*=(1.0/eigTens.norm());
	eigvec=eigTens.getBlock();
	int labels_r1[]={2, 4};
	int labels_l[]={1, 3};
	eigTens.setLabel(labels_r1);
	eigTens_temp.setLabel(labels_r1);
	Qs.push_back(eigvec); 

	while((ArnConverge==false)&&(Arnstep<=Arnstep_max)){
		Arnstep++; //cout<<"Arnstep="<<Arnstep<<endl;
                eigTens.putBlock(eigvec);
                eigTens *= (1.0/eigTens .norm());
                eigval_old=eigval;
		UniTensor eigTens_new=eigTens;
		eigTens_new =contract(A, eigTens, false); eigTens_new =contract(eigTens_new , ATC, false);
		eigTens_new.setLabel(labels_r1);
		if(eig_ord>0){
			eigTens.setLabel(labels_l);
			for(int i=0; i<eig_ord; i++){
				eigTens_new+=(-1.0)*eigval_s.at(i)*contract(eigTens, ls.at(i)).at(CTYPE, 0)*rs.at(i);
			}
		}
		eigTens_new.setLabel(labels_r1);
		eigTens=eigTens_new; eigTens.permute(2);
		eigvec=eigTens .getBlock();
		Hma.resize(CTYPE, Arnstep+1, Arnstep+1) ;
                for(int i=0; i<Arnstep; i++){//cout<<"i="<<i<<endl;
                        Hma.at(CTYPE, i*(Arnstep+1)+(Arnstep-1))=((Qs.at(i).cTranspose(CTYPE))*eigvec).at(CTYPE,0);
                        Qs.at(i).cTranspose(CTYPE);
                        eigvec=eigvec+(-1.0)*Hma.at(CTYPE, i*(Arnstep+1)+Arnstep-1)*Qs.at(i);
		}
                Hma.at(CTYPE, (Arnstep+1)*(Arnstep)+Arnstep-1)=eigvec.norm();
                eigvec *= (1.0/Hma.at(CTYPE, (Arnstep+1)*(Arnstep)+Arnstep-1));
		Qs.push_back(eigvec);
		if(Arnstep>=(first_step)){
                        Hma2=Hma; Hma2.resize(CTYPE, Arnstep, Arnstep); //cout<<Hma2<<endl;
                        Eigs=Hma2.eig();
                        eigval=Eigs.at(0).at(CTYPE, 0);
			if(maxtype=="R"){
                        	for(int i=0; i<Eigs.at(0).col(); i++){
                                	if((real(eigval)-real(Eigs.at(0).at(CTYPE, i)))<mech_acc){idx=i; eigval=Eigs.at(0).at(CTYPE, i);}
                        	}
			}
			else if(maxtype=="N"){
                        	for(int i=0; i<Eigs.at(0).col(); i++){
                                	if((norm(eigval)-norm(Eigs.at(0).at(CTYPE, i)))<mech_acc){idx=i; eigval=Eigs.at(0).at(CTYPE, i);}
                        	}
			}

                	if(Arnstep==D){ArnConverge=true;}
                	else if(Arn_step2==true){
				Matrix P; P=Eigs.at(1).inverse(); P.transpose();
	                        Matrix eigvec_temp(CTYPE, D, 1);
        	                eigvec_temp.set_zero();
                	        for(int i=0; i<Arnstep; i++){eigvec_temp+=P.at(CTYPE, i+Arnstep*idx)*Qs.at(i);}
                        	eigvec_temp *=(1.0/eigvec_temp.norm());
				eigTens_temp.putBlock(eigvec_temp);
				eigTens_new=contract(A, eigTens_temp, false); eigTens_new=contract(eigTens_new, ATC, false);
				if(eig_ord>0){
					eigTens_temp.setLabel(labels_l);
					for(int i=0; i<eig_ord; i++){
						eigTens_new+=(-1.0)*eigval_s.at(i)*contract(eigTens_temp, ls.at(i)).at(CTYPE, 0)*rs.at(i);
					}
				}
				eigTens_new.permute(labels_l, 2);
				eigTens_temp=eigTens_new;
				eigTens_temp.setLabel(labels_r1); eigTens_temp.permute(2);
				err=(eigTens_temp.getBlock()+(-1.0)*eigval*eigvec_temp).norm(); //cout<<err<<endl;
				if(((err<=Arnacc2)&&(Arnstep>Minstep))||(Arnstep==D)){ArnConverge=true;}
                	}
                	if(Arn_step2==false){
                        	err=norm(eigval-eigval_old); //cout<<err<<endl; 
                       		if(((err<=mech_acc)&&(Arnstep>Minstep))||(Arnstep==D)){Arn_step2=true;}
                	}
                }
        }
        Matrix T; T=Eigs.at(1).inverse(); T.transpose();
        eigvec.set_zero();

        for(int i=0; i<Arnstep; i++){eigvec+=T.at(CTYPE, i+Arnstep*idx)*Qs.at(i);}
        eigvec *=(1.0/eigvec.norm()); eigTens.putBlock(eigvec); eigTens.permute(1);
	Arn_err=err;
	//eigTens_temp=eigTens;
	//eigTens_temp=contract(A, eigTens_temp, true); eigTens_temp=contract(eigTens_temp, ATC, true);
	//eigTens_temp.setLabel(labels_r1); eigTens_temp.permute(2);
	double theta_angle; theta_angle=atan(((eigTens.getBlock().at(CTYPE,0)).imag())/((eigTens.getBlock().at(CTYPE,0)).real()));
	complex<double> phase; phase=complex<double>(cos(theta_angle),(-1.0)*sin(theta_angle));
	eigTens =eigTens *phase;
        //cout<<"eigtest= "<<setprecision(15)<<(eigTens_temp.getBlock()+(-1.0)*eigval*eigvec).norm()<<endl;
        //cout<<"Arnstep= "<<Arnstep<<endl;
        //cout<<"eigval=" <<setprecision(15)<<eigval<<endl;
	
	Finish=clock();
	Duration=(double)(Finish-Start)/CLOCKS_PER_SEC;
	//cout<<"Arnoldi Tensor type spend "<<Duration<<" sconds."<<endl;
	ls.resize(eig_ord+1); rs.resize(eig_ord+1); rs.at(eig_ord)=eigTens; 
	eigval_s.resize(eig_ord+1); eigval_s.at(eig_ord)=eigval; //cout<<eig_ord<<"===="<<endl;
	//cout<<norm(eigval)<<endl;
	
return;
}

void L_Arn_Tens_meth2(string maxtype, int eig_ord, UniTensor A, UniTensor ATC, vector<UniTensor>& ls, vector<UniTensor>& rs, vector<complex<double>>& eigval_s){
	int labels_l1[]={1, 3}; int labels_r1[]={2, 4};
	int labels_Al[]={0, 2, 1}; int labels_ATCl[]={4, 3, 0};
	A.permute(labels_Al, 1); ATC.permute(labels_ATCl, 2);
	vector<UniTensor> swap=ls; ls=rs; rs=swap;
	UniTensor rl;
	for(int i=0; i<eig_ord; i++){ls.at(i).setLabel(labels_l1); rs.at(i).setLabel(labels_r1);}
	R_Arn_Tens_meth2(maxtype, eig_ord, A, ATC, ls, rs , eigval_s);
	swap=ls; ls=rs; rs=swap;
	for(int i=0; i<=eig_ord; i++){ls.at(i).setLabel(labels_l1); rs.at(i).setLabel(labels_r1);}
	for(int i=0; i<=eig_ord; i++){
		rl=ls.at(i); rl.setLabel(labels_r1); rl=contract(rs.at(i), rl);
		rs.at(i)*=(1.0/rl.at(CTYPE,0));
		ls.at(i).setLabel(labels_l1); rs.at(i).setLabel(labels_r1);
	}
return;
}

void LR_Arn(string maxtype, int num, UniTensor A, UniTensor ATC, vector<UniTensor>& ls, vector<UniTensor>& rs, vector<complex<double>>& eigval_s){
	//UniTensor E=contract(A, ATC); int per_E[]={1,3,2,4}; E.permute(per_E,2);
	//vector<Matrix> Eigs=E.getBlock().eig(); 
	//for(int i=0; i<4; i++)cout<<Eigs.at(0).at(CTYPE, i)<<endl;
	for(int eig_ord=0; eig_ord<=num; eig_ord++){
		R_Arn_Tens_meth2(maxtype, eig_ord, A, ATC, ls, rs, eigval_s);
		L_Arn_Tens_meth2(maxtype, eig_ord, A, ATC, ls, rs, eigval_s);
	}
return;
}


UniTensor root_make(UniTensor T){
	Matrix M=T.getBlock();
	vector<Matrix>Eigs;
	Eigs=M.eig();
	for(int i=0; i<M.col(); i++){;
		Eigs.at(0).at(CTYPE, i)=pow(Eigs.at(0).at(CTYPE, i), 0.5);
	}
	M=(Eigs.at(1).inverse())*Eigs.at(0)*Eigs.at(1);
	T.putBlock(M);
return T;
}

UniTensor K_OP(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor r, UniTensor& l){
	int labels_K[]={1, 3}; K.setLabel(labels_K);
	r.setLabel(labels_K);
	UniTensor KOP, K_AA; 
	K_AA=contract(K, A); K_AA=contract(K_AA, ATC); 
	KOP=K+ (-1.0)*K_AA+(contract(K, r).getBlock().at(CTYPE,0))*l;
return KOP;
}

UniTensor K_OPT(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor& r, UniTensor l){
        int labels_K[]={2, 4}; K.setLabel(labels_K);
        l.setLabel(labels_K);
        UniTensor KOP, K_AA;
        K_AA=contract(K, A); K_AA=contract(K_AA, ATC);
        KOP=K+ (-1.0)*K_AA+(contract(K, l).getBlock().at(CTYPE,0))*r;
return KOP;
}

complex<double> unidot(UniTensor a, UniTensor b){
	a.cTranspose(CTYPE);
	int labels_a[]={3, 1}; int labels_b[]={1, 3};
	a.setLabel(labels_a); b.setLabel(labels_b);
	complex<double> c;
	c=contract(a, b).getBlock().at(CTYPE, 0);
return c;
}
	
UniTensor BOP_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r){
	clock_t Start, Finish; double Duration;
	Start=clock();
	int D=l.getBlock().row();
	UniTensor K(CTYPE,l.bond());
	K.putBlock(K_mat);
	int label_K[]={1,3}; K.setLabel(label_K);
	
	int labels_C[]={2,4}; C.setLabel(labels_C);
        int label_K2[]={2,4};
	double K_err=1.0;int step_K=0;
	UniTensor constterm(CTYPE, l.bond()); 
	constterm=C+(-1.0)*(contract(C, r).getBlock().at(CTYPE,0))*l;
	//cout<<contract(C,r)<<endl;
	constterm.setLabel(label_K);

        UniTensor K_old=K;
	UniTensor KOP=K_OP(K, A, ATC, r, l);
	UniTensor rat; rat=constterm+(-1.0)*KOP; UniTensor rat_old=rat;
        UniTensor rat_deg=rat;
        
        complex<double> alph, rho, omeg, alph_old, rho_old, omeg_old, bet;
        alph=complex<double> (1.0, 0); rho=alph; omeg=alph;
        alph_old=alph; rho_old=rho; omeg_old=omeg;
	UniTensor p(CTYPE, K.bond()); p.set_zero(); UniTensor p_old=p;
	UniTensor v=p; UniTensor v_old=v; v.setLabel(label_K); p.setLabel(label_K);
	UniTensor h_uniT, s, t, h_uniT_old;

        while((K_err>bicgstab_acc)&&(step_K<=bicg_step_max)){step_K++;
                rho=unidot(rat_deg, rat_old);
                bet=(rho/rho_old)*(alph/omeg_old);
                p=rat_old+bet*(p_old+(-1.0)*omeg_old*v_old);
                v=K_OP(p, A, ATC, r, l); 
                alph=rho/(unidot(rat_deg, v));
                h_uniT=K_old+alph*p;

                s=rat_old+(-1.0)*alph*v;
                t=K_OP(s, A, ATC, r, l);
                omeg=(unidot(t, s))/(unidot(t, t));
                K=h_uniT+omeg*s;
                K_err=(K+(-1.0)*K_old).norm();
                rat=s+(-1.0)*omeg*t;
                rho_old=rho; omeg_old=omeg; rat_old=rat; p_old=p;v_old=v; K_old=K; h_uniT_old=h_uniT;
        }
	bicg_err=K_err;
	K_mat=K.getBlock();//K.printDiagram();
	//cout<<"step_K= "<<step_K<<endl;
	int labels_a[]={0, 1};
	K.setLabel(labels_a);
	
	//cout<<"BO step= "<<step_K<<endl;
	Finish=clock();
	Duration=(double)(Finish-Start)/CLOCKS_PER_SEC; //cout<<"K_make time is "<<Duration<<"seconds. "<<endl;
return K;
}

UniTensor BOPT_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r){
        clock_t Start, Finish; double Duration;
        int D=r.getBlock().row();
        UniTensor K(CTYPE,r.bond());
        K.putBlock(K_mat);
        int label_K[]={2,4}; K.setLabel(label_K);
        int labels_C[]={1,3}; C.setLabel(labels_C);
        int label_K2[]={1,3};
        double K_err=1.0;int step_K=0;
        UniTensor constterm(CTYPE, l.bond());
        Start=clock();
        constterm=C+(-1.0)*(contract(C, l).getBlock().at(CTYPE,0))*r;
	
        Finish=clock();
	//cout<<contract(C,l)<<endl;
        constterm.setLabel(label_K);

        UniTensor K_old=K;
        UniTensor KOP=K_OPT(K, A, ATC, r, l);
        UniTensor rat; rat=constterm+(-1.0)*KOP; UniTensor rat_old=rat;
        UniTensor rat_deg=rat;

        complex<double> alph, rho, omeg, alph_old, rho_old, omeg_old, bet;
        alph=complex<double> (1.0, 0); rho=alph; omeg=alph;
        alph_old=alph; rho_old=rho; omeg_old=omeg;
        UniTensor p(CTYPE, K.bond()); p.set_zero(); UniTensor p_old=p;
        UniTensor v=p; UniTensor v_old=v; v.setLabel(label_K); p.setLabel(label_K);
        UniTensor h_uniT, s, t, h_uniT_old;

        while((K_err>bicgstab_acc)&&(step_K<=bicg_step_max)){step_K++;
                rho=unidot(rat_deg, rat_old);
                bet=(rho/rho_old)*(alph/omeg_old);
                p=rat_old+bet*(p_old+(-1.0)*omeg_old*v_old);
                v=K_OPT(p, A, ATC, r, l);
                alph=rho/(unidot(rat_deg, v));
                h_uniT=K_old+alph*p;

                s=rat_old+(-1.0)*alph*v;
                t=K_OPT(s, A, ATC, r, l);
                omeg=(unidot(t, s))/(unidot(t, t));
                K=h_uniT+omeg*s;
                K_err=(K+(-1.0)*K_old).norm();
                rat=s+(-1.0)*omeg*t;
                rho_old=rho; omeg_old=omeg; rat_old=rat; p_old=p;v_old=v; K_old=K; h_uniT_old=h_uniT;
        }
        K_mat=K.getBlock();//K.printDiagram();//cout<<"step_K= "<<step_K<<endl;
        int labels_a[]={0, 1};
        K.setLabel(labels_a);
	//cout<<"BOT step= "<<step_K<<endl;
        Duration=(double)(Finish-Start)/CLOCKS_PER_SEC; //cout<<"K_make time is "<<Duration<<"seconds. "<<endl;
return K;
}

UniTensor A_sym_fix(UniTensor A_L, UniTensor C){
	vector<Matrix> Eigs=C.getBlock().eig();
	for(int i=0; i<C.bond(0).dim(); i++){
		Eigs.at(0).at(CTYPE, i)=pow(Eigs.at(0).at(CTYPE, i), 0.5);
	}
	Matrix P_inv=Eigs.at(1).inverse();
	Matrix C_root_mat=P_inv*Eigs.at(0)*Eigs.at(1);
	for(int i=0; i<C.bond(0).dim(); i++){
		Eigs.at(0).at(CTYPE, i)=pow(Eigs.at(0).at(CTYPE, i), -1.0);
	}
	Matrix C_root_inv_mat=P_inv*Eigs.at(0)*Eigs.at(1);
	UniTensor C_root(CTYPE, C.bond()), C_root_inv(CTYPE, C.bond());
	C_root.putBlock(C_root_mat); C_root_inv.putBlock(C_root_inv_mat);
	int labels_C_root[]={2, 3}; C_root.setLabel(labels_C_root);
	int labels_C_root_inv[]={-1, 1}; C_root_inv.setLabel(labels_C_root_inv);
	UniTensor A=contract(A_L, C_root); A=contract(A, C_root_inv);
	int per[]={0, -1, 3}; A.permute(per, 1);
	int labels_A[]={0, 1, 2};	
	A.setLabel(labels_A);
return A;
}

void eigcal_sub(UniTensor A, vector<UniTensor> Ms, UniTensor& BL, UniTensor& BR, UniTensor l, UniTensor r, double& e0){
        int d=A.bond(0).dim(); int D=A.bond(1).dim();
        int M=6;
        int labels_ATC[]={3, 4, 0};
        int labels_a[]={0, 1}; int labels_l[]={1, 3}; int labels_r[]={2, 4};
        int labels_locMPO[]={0, -1};
        UniTensor ATC=A; ATC.cTranspose(CTYPE);
        vector<UniTensor> Es;
        UniTensor E=l; Es.push_back(E);
        Bond bdpi_sub(BD_IN, 2); Bond bdpo_sub(BD_OUT, 2);

        Es.push_back(connect("L", Es.at(0), A, Ms.at(8)));

        UniTensor C1; C1=connect("L", Es.at(0), A, Ms.at(7)); C1.permute(1);
        C1.setLabel(labels_a);
        ATC.setLabel(labels_ATC);
        Matrix mat=l.getBlock();
        E=BOP_make_BiCGSTAB(mat, A, ATC, C1, l, r);
        Es.push_back(E);

        Es.push_back(connect("L", Es.at(0), A, Ms.at(6)));
        Es.push_back(connect("L", Es.at(0), A, Ms.at(5)));

        C1=connect("L", Es.at(4), A, Ms.at(1))+connect("L", Es.at(3), A, Ms.at(2))+connect("L", Es.at(2), A, Ms.at(3))+
        connect("L", Es.at(1), A, Ms.at(4))+connect("L", Es.at(0), A, Ms.at(9));
        C1.permute(1); C1.setLabel(labels_a);

        E=BOP_make_BiCGSTAB(mat, A, ATC, C1, l, r);
        Es.push_back(E);

        vector<UniTensor> Fs;
        UniTensor F=r;
        Fs.push_back(F);
        Fs.push_back(connect("R", Fs.at(0), A, Ms.at(1)));
        Fs.push_back(connect("R", Fs.at(0), A, Ms.at(2)));

        UniTensor C2;
        C2=connect("R", Fs.at(0), A, Ms.at(3));
        C2.permute(1); C2.setLabel(labels_a);
        F=BOPT_make_BiCGSTAB(mat, A, ATC, C2, l, r);
        Fs.push_back(F);

        Fs.push_back(connect("R", Fs.at(0), A, Ms.at(4)));

        C2=connect("R", Fs.at(0), A, Ms.at(9))+connect("R", Fs.at(1), A, Ms.at(5))+connect("R", Fs.at(2), A, Ms.at(6))+
        connect("R", Fs.at(3), A, Ms.at(7))+connect("R", Fs.at(4), A, Ms.at(8));
        C2.permute(1); C2.setLabel(labels_a);
        F=BOPT_make_BiCGSTAB(mat, A, ATC, C2, l, r);
        Fs.push_back(F);
	        r.setLabel(labels_a); l.setLabel(labels_a);//e0=0.5*(contract(C1, r).at(CTYPE, 0).real());      //cout<<setprecision(15)<<e0<<endl;
        e0=0.5*(contract(C2, l).at(CTYPE, 0).real());   //cout<<setprecision(15)<<e0<<endl;
        l.setLabel(labels_l); r.setLabel(labels_r);

        Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
        Bond bdm(BD_IN, M);
        vector<Bond> bonds;
        bonds.push_back(bdm); bonds.push_back(bdi); bonds.push_back(bdo);
        UniTensor Bl_temp(CTYPE, bonds);
        bonds.clear();

        BL=Bl_temp;
        complex<double> *elem_t;
        elem_t=(complex<double>*)malloc(6*(D*D)*sizeof(complex<double>));
        for(int i=0; i<M; i++){
                memcpy(elem_t+i*(D*D), Es.at(M-1-i).getElem(CTYPE), (D*D)*sizeof(complex<double>));
        }
        Matrix E_mat(CTYPE, M*D, D); E_mat.setElem(elem_t);
        free(elem_t);
        BL.putBlock(E_mat);
        int per_BL[]={1, 0, 2}; BL.permute(per_BL, 0);
        int labels_BL[]={1, -2, 3}; BL.setLabel(labels_BL);

        BR=Bl_temp;
        elem_t=(complex<double>*)malloc(6*(D*D)*sizeof(complex<double>));
        for(int i=0; i<M; i++){
                memcpy(elem_t+i*(D*D), Fs.at(i).getElem(CTYPE), (D*D)*sizeof(complex<double>));
        }
        Matrix F_mat(CTYPE, M*D, D); F_mat.setElem(elem_t);
        free(elem_t);
        BR.putBlock(F_mat);
        int per_BR[]={1, 0, 2}; BR.permute(per_BL, 3);
        int labels_BR[]={2, -3, 4}; BR.setLabel(labels_BR);
}

UniTensor connect(string Side, UniTensor E, UniTensor A, UniTensor O){
        int labels_l[]={1, 3}; int labels_r[]={2, 4};
        UniTensor ATC=A; ATC.cTranspose(CTYPE);
        int labels_ATC2[]={3, 4, -1}; ATC.setLabel(labels_ATC2);
        UniTensor EOA;
        if(Side == "L"){
                E.setLabel(labels_l);
                EOA=contract(E, A); EOA=contract(EOA, O); EOA=contract(EOA, ATC);
                int per[]={2, 4}; EOA.permute(per, 1); 
        }
        else if(Side == "R"){
                E.setLabel(labels_r);
                EOA=contract(E, A); EOA=contract(EOA, O); EOA=contract(EOA, ATC);
                int per[]={1, 3}; EOA.permute(per, 1); 
        }
        int labels_a[]={0, 1}; EOA.setLabel(labels_a);
return EOA;
}

UniTensor Y_make(UniTensor A, UniTensor& MPO, UniTensor& BL, UniTensor& BR, UniTensor VL, UniTensor VR){
	int D=A.bond(1).dim();
	UniTensor A_R=A;
	int labels_A_R[]={-1, 2, 3}; A_R.setLabel(labels_A_R);
	UniTensor A2_C=contract(A, A_R);
	int per1[]={0, 1, -1, 3}; 
	A2_C.permute(per1, 4);  //A2_C*=(1.0/A2_C.norm()); 
	Network Lann("./Diagrams/Lanczos_A2C.net");
	Lann.putTensor("BL", BL); Lann.putTensor("WC1", MPO);
	Lann.putTensor("WC2", MPO); Lann.putTensor("BR", BR);
	Lann.putTensor("A2_C", A2_C);
	UniTensor B2=Lann.launch();
	int labels_ATC[]={3, 4, 0};
	UniTensor VLTC=VL; VLTC.cTranspose(CTYPE);
	UniTensor VRTC=VR; VRTC.cTranspose(CTYPE);
	int labels_VLTC[]={1, 3, -3}; int labels_VRTC[]={4, -4, 2};
	VLTC.setLabel(labels_VLTC); VRTC.setLabel(labels_VRTC);
	B2=contract(B2, VLTC); B2=contract(B2, VRTC);//B2.printDiagram();
return B2;
}

                                                                                                       
UniTensor B_tilde_make(int D_tilde, UniTensor A, UniTensor B, vector<UniTensor>& Ms, UniTensor MPO, UniTensor& l, UniTensor& r,
	Matrix& l_mat, Matrix& r_mat,  complex<double> dir_vec, double del_t){
	int D=A.bond(1).dim(); int d=A.bond(0).dim();//VL.printDiagram(); VR.printDiagram();
	UniTensor l_root, r_root, l_root_inv, r_root_inv, VL, VR;
	sym_gauge(A, l, r, l_mat, r_mat, l_root, l_root_inv);
	//del_t=del_t*del_t;
	B*=((-1.0)*dir_vec);

	r_root_inv=l_root_inv; r_root=l_root;
	UniTensor ATC=A; ATC.cTranspose(CTYPE); 
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
	UniTensor ATC_l_root=ATC_l_root_make(ATC, l_root); 
	VL=VL_make(ATC_l_root, D, d);
	VR=VR_make(ATC, r_root);
	UniTensor BL, BR;
	double e0=0;
	eigcal_sub( A, Ms, BL, BR, l, r, e0);
	UniTensor Y=Y_make(A, MPO, BL, BR, VL, VR);

	vector<Matrix> SVDs;
	Matrix Z_12_mat, Z_21_mat;
	Matrix Y_mat; Y_mat=Y.getBlock();
	SVDs=Y_mat.svd();
	SVDs.at(0).resize(Y_mat.row(), D_tilde-D);
	SVDs.at(1).resize(D_tilde-D, D_tilde-D);
	SVDs.at(2).resize(D_tilde-D, Y_mat.col());
	for(int i=0; i<D_tilde-D; i++){SVDs.at(1).at(CTYPE, i)=pow(SVDs.at(1).at(CTYPE, i), 0.5);}
	Z_12_mat=SVDs.at(0)*SVDs.at(1); Z_21_mat=SVDs.at(1)*SVDs.at(2);
	SVDs.clear();
	Bond bd1(BD_IN, D*(d-1)); Bond bd2(BD_OUT, D_tilde-D);
	vector<Bond> bonds; bonds.push_back(bd1); bonds.push_back(bd2);
	UniTensor Z_12(CTYPE, bonds, "Z_12");
	bonds.clear();
	UniTensor Z_21; Z_21=Z_12; Z_21.transpose();
	Z_12.putBlock(Z_12_mat); Z_21.putBlock(Z_21_mat);
	int labels_A[]={0, 1, 2};
	int labels_Z[]={3, 2}; Z_12.setLabel(labels_Z); Z_21.setLabel(labels_Z);
	int labels_root_inv_temp[]={0, -2};
	l_root_inv.setLabel(labels_root_inv_temp);r_root_inv.setLabel(labels_root_inv_temp);
	Z_12=contract(Z_12, VL); Z_12=contract(Z_12, l_root_inv);
	int labels_Z12_per[]={-1, -2, 2}; Z_12.permute(labels_Z12_per, 1);
	Z_21=contract(Z_21, VR); Z_21=contract(Z_21, r_root_inv);
	int labels_Z21_per[]={1, 3, -2}; Z_21.permute(labels_Z21_per, 1);
	B=B*del_t;
	B.permute(2);//cout<<B<<endl; 
	Z_12.permute(2); //cout<<Z_12<<endl;
	Z_12_mat=Z_12.getBlock();
	Matrix B1_mat=B.getBlock();
	B1_mat.resize(d*D, D_tilde);
	for(int i=0; i<d*D; i++){
		for(int j=0; j<D_tilde-D; j++){
			B1_mat.at(CTYPE, i*D_tilde+j+D)=Z_12_mat.at(CTYPE, i*(D_tilde-D)+j)*pow(del_t, 0.5);
		}
	}
	Bond bdi1B(BD_IN, d); Bond bdi2B(BD_IN, D);
	Bond bdi3B(BD_IN, D_tilde); Bond bdo1B(BD_OUT, D_tilde);
	bonds.push_back(bdi1B); bonds.push_back(bdi2B); bonds.push_back(bdo1B);
	UniTensor B_temp1(CTYPE, bonds);
	bonds.clear();
	B_temp1.putBlock(B1_mat);
	int B_per[]={0, 2, 1}; B_temp1.permute(B_per, 2);
	B1_mat=B_temp1.getBlock();
	B1_mat.resize(d*D_tilde, D_tilde); 
	Z_21.permute(2);
	Z_21_mat=Z_21.getBlock();
	Z_21_mat.resize(d*(D_tilde-D), D_tilde);
	Bond bdiz(BD_IN, D_tilde-D);
	bonds.push_back(bdi1B); bonds.push_back(bdiz); bonds.push_back(bdo1B);
	UniTensor Z_21_temp(CTYPE, bonds);
	bonds.clear();

	Z_21_temp.putBlock(Z_21_mat);

	Z_21_temp.permute(B_per, 2); //Z_21_temp.printDiagram();
	Z_21_mat=Z_21_temp.getBlock();
	bonds.push_back(bdi1B); bonds.push_back(bdi3B); bonds.push_back(bdo1B);
	UniTensor B_temp2(CTYPE, bonds);
	for(int i=0; i<d*D_tilde; i++){
		for(int j=0; j<D_tilde-D; j++){
			B1_mat.at(CTYPE, i*D_tilde+j+D)=Z_21_mat.at(CTYPE, i*(D_tilde-D)+j)*pow(del_t, 0.5);
		}
	}
	B_temp2.putBlock(B1_mat);
	B=B_temp2; 
	B.permute(B_per, 1);
	l_mat.resize(D_tilde, D_tilde); r_mat.resize(D_tilde, D_tilde);
	bonds.clear();
	bonds.push_back(bdi3B); bonds.push_back(bdo1B);
	UniTensor l_temp(CTYPE, bonds);
	l_temp.randomize();
	l=l_temp; r=l_temp;
	bonds.clear();
return B;
}

void A_expand(UniTensor& A, int D_tilde){
	int D=A.bond(1).dim(); int d=A.bond(0).dim();
	Matrix A_mat;
	int label_A[]={0, 1, 2};
	vector<Bond> bonds;
	Bond bd1(BD_IN, D), bd2(BD_OUT, D_tilde), bd3(BD_IN, D_tilde);
	bonds.push_back(A.bond(0)); bonds.push_back(bd1); bonds.push_back(bd2);
	UniTensor A_Temp2(CTYPE, bonds);
	bonds.clear();
	A.permute(2);
	A_mat=A.getBlock();
	A_mat.resize(d*D, D_tilde); A_Temp2.putBlock(A_mat);
	int A_Temp_labels[]={0, 2, 1};
	A_Temp2.permute(A_Temp_labels, 2);
	A_mat=A_Temp2.getBlock();
	A_mat.resize(d*D_tilde, D_tilde);
	bonds.clear();
	bonds.push_back(A.bond(0)); bonds.push_back(bd3), bonds.push_back(bd2);
	UniTensor A_Temp3(CTYPE, bonds);
	bonds.clear();
	A_Temp3.putBlock(A_mat);
	A_Temp3.permute(A_Temp_labels, 1);A_Temp3.setLabel(label_A);
	A=A_Temp3;
return;
}


void output_func_tau(int& D, double& delg_init, double& del_g, double& m_init, double& m, double& t, UniTensor& A){
	char dir_char[200];
	strcpy(dir_char, tdvp_f_name("dir_tau", D, delg_init, del_g, m_init, m, t).c_str()); 
	system(dir_char);
	A.save(tdvp_f_name("A_tau", D, delg_init, del_g, m_init, m, t));
return;
}

string contract_str(string s1, string s2){
	string s;
	stringstream ss(s);
	ss<<s1<<s2;
return ss.str();
}

string contract_str_int(string s1, int n){
	string s;
	stringstream ss(s);
	ss<<s1<<n;
return ss.str();
}


void show_setting( double a, double lambda, double delg_init, double del_g, double m_init, double m,
	double h, double S_tar, double D, double del_t, const int termi_step, const int del_step){
	for(int i=0; i<80; i++)cout<<"=";
	cout<<endl<<"Algorithm:TDVP\n"<<endl;
	string Dy_bool_str, evo_typ_str;
	Dy_bool_str= Dy_bond_bool==true ? "Expand Bond Dimension Dynamically" 
	: "Bond Dimension Fixed";
	evo_typ_str= im_bool==true ? "Imaginagry time evolution" : "Real time evolution";
	cout<<"**********Bond dimension properties**********"<<endl;
	cout<<Dy_bool_str<<endl;
	if(Dy_bond_bool==true){
		cout<<"initial bond dnimension D= "<<D<<" (at O point)"<<endl;
		cout<<"Maximum bond dimension D_max= "<<D_max<<endl;
		cout<<"del_D= "<<del_D<<endl;
	}
	else{cout<<"Bond dimension D= "<<D<<endl;};
	cout<<"\n***************Evolution Type***************"<<endl;
	cout<<evo_typ_str<<endl<<"The direction for evolution: "<<dir_vec<<endl;
	cout<<"##(0, 1) for real time and (1, 0) or (-1, 0) for imaginary time"<<endl;
	if(im_bool==true){cout<<"tau= "<<tau<<endl;};
	cout<<"\n*****************Parameters*****************"<<endl;
	cout<<"Initial Delta (anisotropy):"<<delg_init<<endl;
	cout<<"Evolving Delta (anisotropy):"<<del_g<<endl;
	cout<<"Initial mass (stagger field):"<<m_init<<endl;
	cout<<"Evolving mass (stagger field):"<<m<<endl;
	cout<<"\n******************Accuracy******************"<<endl;
	cout<<"The tolerance of Arnoli method: "<<setprecision(3)<<Arnacc<<endl;
	cout<<"The maximum iteration for Arnoldi method: "<<Arnstep_max<<endl;
	cout<<"The tolerance of BiCGstab method: "<<setprecision(3)<<bicgstab_acc<<endl;;
	cout<<"The maximum iteration for BiCGstab method: "<<bicg_step_max<<endl;
	cout<<"The tolerance of th smallest element of density matrix: "<<setprecision(3)<<Sf_acc<<endl<<endl;
	cout<<"The number of the spectrum for transfer matrix: "<<T_num<<endl<<endl;
	for(int i=0; i<80; i++)cout<<"=";
	cout<<"\n\n";

return;
}
