My_path=`pwd | sed 's/\/run_XXZ//' | sed 's/\/run//'  `
TDVP_XXZ_path=TDVP/XXZ_TDVP/real_Thir
VUMPS_XXZ_path=VUMPS/XXZ_VUMPS/XXZ

##set the parameter from the file "./para_set"
DATA_DIR=`grep '^DATA_DIR:' para_set | sed 's/^.*://'`

Dy_bond_bool=`grep '^Dy_bond_bool:' para_set | sed 's/^.*://'`

im_per_step_save=`grep '^im_save_step:' para_set | sed 's/^.*://'`
del_t=`grep '^im_del_t:' para_set | sed 's/^.*://'`
im_length=`grep '^im_length:' para_set | sed 's/^.*://'`

tau=`grep '^tau:' para_set | sed 's/^.*://'`
tau=$(echo "scale=1; $tau")
tau=$(echo "scale=1; ${tau}/1" |bc -l |sed -e 's/^\./0./' | sed -e 's/^-\./-0./' |sed -e 's/\.0//' )
tau_init=$tau
bond=`grep '^bond:' para_set | sed 's/^.*://'`
D_max=`grep '^D_max:' para_set | sed 's/^.*://'`

delta_start_list=`grep '^delta_start_list:' para_set | sed 's/^.*://'`
delta_end_list=`grep '^delta_end_list:' para_set | sed 's/^.*://'`
m_start_list=`grep '^m_start_list:' para_set | sed 's/^.*://'`
m_end_list=`grep '^m_end_list:' para_set | sed 's/^.*://'`

max_run_num=`grep '^max_run_num:' para_set | sed 's/^.*://'`
break_bool='false'

###create corresponding exe files and pbs files
./create_tdvp_exe.sh && ./create_tdvp_pbs.sh && ./create_vumps_exe.sh && ./create_vumps_pbs.sh


if [ $Dy_bond_bool == 'true' ] || [ $Dy_bond_bool == '1' ]; then
        Bond_str1="Dy_bond/Dmax$D_max"
        Bond_str2=""
else
        Bond_str1="Fix_bond/BondDim$bond"
        Bond_str2="D$bond"
fi



### the following function checks whether or not the real-time evolution at each imaginary time has been done.
### if the real-time evolution has not been done at any imaginary time, then "done_check" will be set
### to false.
function check_done(){
	done_check="true"
	for ((i=0; i<$im_length; i=i+1))
	do
		if ! test -e "$DATA_DIR/XXZ_data/${param_str2}/tau"$tau"/re_done"; then
			done_check="false"
		fi
	tau=$(echo "scale=1; ${tau}+${im_per_step_save}*${del_t}/1" |bc -l |sed -e 's/^\./0./' | sed -e 's/^-\./-0./'|sed -e 's/\.0//' )
	done
	tau=$tau_init
}



if ! test -e ./Diagrams; then
	cp -r $My_path/TDVP/XXZ_TDVP/real_Thir/Diagrams ./
fi

for m_start  in ${m_start_list}
do
for m_end  in ${m_end_list}
do
for delta_start in ${delta_start_list}
do
for delta_end in ${delta_end_list}
do

if [ $Dy_bond_bool == 'true' ] || [ $Dy_bond_bool == '1' ]; then
	param_str=m${m_start}to${m_end}_del${delta_start}to${delta_end}
	param_str2=tdvp_tens/Dy_bond/Dmax$D_max/del_g${delta_start}to${delta_end}m${m_start}to${m_end}
else
	param_str=D${bond}_m${m_start}to${m_end}_del${delta_start}to${delta_end}
	param_str2=tdvp_tens/Fix_bond/D$bond/D${bond}del_g${delta_start}to${delta_end}m${m_start}to${m_end}
fi
cp $My_path/run/run_XXZ/para_set $DATA_DIR/XXZ_data/${param_str2}/parameter.txt

check_done
while [ "${done_check}" == "false" ] && [ $break_bool == 'false' ]
do
check_done
## loop over imaginary time steps
for ((i=0; i<$im_length; i=i+1))
do
echo "tau=$tau"
run_re_check=`qstat -f |grep "Job_Name" |grep "${param_str}tau${tau}.pbs" | wc -l`
run_im_check=`qstat -f |grep "Job_Name" |grep "${param_str}im.pbs" | wc -l`
run_minus_im_check=`qstat -f |grep "Job_Name" |grep "${param_str}minus_im.pbs" | wc -l`
max_run_check=`qstat |grep "thirring" | wc -l`
## at each imaginary time step, first check if the real-time evolution is done
## if it's done, then that's all for this Im time step
if test -e "$DATA_DIR/XXZ_data/${param_str2}/tau"$tau"/re_done"; then
	echo "The real time evolution for tau=$tau has been done."
## if the real-time evolution at this Im time step is not done yet, first check 
## if it's already running.  if it is, then just sleep and wait.
elif [ ${run_re_check} -ge 1 ]; then
	echo "For tau="$tau", real time evolution is still running."; sleep 1;
## if the real-time evolution at this Im time step is not done, and it's not running,
## then first check if the Im time state is already there, such that the real-time
## evolution can be started --- if so then just submit the real-time evolution job.
elif test -e "$DATA_DIR/XXZ_data/${param_str2}/tau$tau"; then
	if [ ${max_run_check} -lt $max_run_num ]; then
		echo "Submit real time evolution for tau=$tau."
		qsub "./run_dir/tdvp_run_dir/$Bond_str1/quench_${param_str}/${param_str}tau${tau}.pbs"
	fi
	sleep 1;
## if the Im time state is not there for starting the real-time evolution.....
else
## then in this case,we check if the starting point for the Im time evolution is already there.
## if so......
	if test -e "$DATA_DIR/XXZ_data/${param_str2}/tau0"; then
## then check if the Im time evolution is running.  if it is, then sleep and wait.
		if [ ${run_im_check} -ge 1 ] || [ ${run_minus_im_check} -ge 1 ]; then
			echo "Imaginary time evolution still not run to tau=$tau."
			sleep 1;
## if the Im time is not running, but the starting point is there.  that means the Im time evolution
## has been finished, but it's not run long enough.  then the task to do now is find the largest Im time
## that exists, and take it from there to continue the Im time evolution.
		else
			break_bool='true'
			echo "check the imaginary time whether long enough."
		fi
## if even the starting point of the Im time evolution is not there, then that means the Im time 
## evolution has to be started from the beginning....
	else
## in this case, we fisrt check if the ground state (started) is already there.  if so, then just submit the 
## job to start the Im time evolution.
		if test -e "$DATA_DIR/XXZ_data/GS_tens/D${bond}/D${bond}g${delta_start}m${m_start}" ; then
##check whther the Im time evolution is running, if so , then sleep and wait
			if [ ${run_im_check} -ge 1 ]; then
				echo "imaginary time evolution is running"
##else, submit Im time evoltion
			else
				qsub "./run_dir/tdvp_run_dir/$Bond_str1/quench_${param_str}/${param_str}im.pbs"
                        	sleep 1
			fi
##check whther the Im time evolution at reverse direction  is running, if so , then sleep and wait
			if [ ${run_minus_im_check} -ge 1 ]; then
				echo "minus imaginary time evolution is running"
##else, submit Im time evoltion at reverse direction
			else
				qsub "./run_dir/tdvp_run_dir/$Bond_str1/quench_${param_str}/${param_str}minus_im.pbs"
                        	sleep 1
			fi
## if the ground state is not there, 
       	 	else
                	run_gs_start_check=`qstat -f |grep "Job_Name" |grep "BondDim${bond}_m${m_start}_del${delta_start}.pbs" | wc -l`
##if not, then first check if the started ground state job is running.
## if the started ground state job is already running, then just sleep and wait
               		if [ ${run_gs_start_check} -ge 1 ] ; then
                               	sleep 1
                       		echo "Running GS starting point"
## if the started ground state job is not running, then submit it.
               		else
				qsub "./run_dir/vumps_run_dir/BondDim"$bond"/submit_BondDim${bond}_m${m_start}_del${delta_start}.pbs"
                       		echo "Submit GS starting point"
                       		sleep 2
			fi
        	fi

	fi
fi
tau=$(echo "scale=1; ${tau}+${im_per_step_save}*${del_t}/1" |bc -l |sed -e 's/^\./0./' | sed -e 's/^-\./-0./'|sed -e 's/\.0//' )
## end of the for loop over Im time steps
done
tau=$tau_init
echo "$done_check"
sleep 30
echo "============================================================="
## end of the while loop over in checking whether all real-time evolution has been done.
done
if [ $break_bool == 'false' ]; then
## generate the figure
	$My_path/run/run_XXZ/exe_dir/tdvp_exe_dir/$Bond_str1/quench_$param_str/XXZ_${param_str}_fig_gen.e 
	echo "done"
else
	echo "break for some reason."
fi
done
done
done
done
