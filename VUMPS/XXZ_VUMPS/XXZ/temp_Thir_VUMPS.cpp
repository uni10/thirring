#include<iostream>
using namespace std;
#include <uni10.hpp>
#include "uni10_lapack_wrapper.h"
using namespace uni10;

//measurement
double e, s, Cc, Sz, err1_v, err2_v; 

//acurracy and parameter
const double Arnacc=%Arnacc;
const int Arnstep_max=%Arnstep_max;
const double bicgstab_acc=%bicg_acc;
const int bicg_step_max=%bicg_step_max;
const double Lanc_acc=%Lanc_acc;
const int MinLanIter=2;
const int MaxLanIter=%Lanstep_max;
const double goal_acc=%goal_acc;
const double mech_acc=1.0e-14;
const int D_max=%D;

const complex<double> image=complex<double>(0, 1.0);

#include "./VUMPS_Tools.cpp"
#include "./MPO.cpp"
#include "./measure.cpp"
#include "./VUMPS_algorithms.cpp"

int main(){
	int d=4;
	int D=%bondinit; 
	
	double a=1.0;
	double lambda=0.0;
	double del_g=%delg; double del_g_old=del_g;
	double m=%m;
	double h=0.0;
	double S_tar=0.0;

//print the algorithm
	show_setting( a, lambda, del_g, m, h, S_tar, D);
	
//create initail state	
	UniTensor A_L, A_R, A_C, C;
	As_init(d, D, A_L, A_R, A_C, C);
	UniTensor MPO; vector<UniTensor> Ms;
	
//Do VUMPS
	for(int i=0; i<1; i++){for(int j=0; j<1; j++){
		MPO=MPO_Thir(a, lambda, del_g, m, h, S_tar);
		MPO_sub(MPO, Ms);
		VUMPS(A_C, A_L, A_R, C, MPO, Ms, e, del_g, m);
		Ms.clear();
		del_g-=0.1;
	}del_g=del_g_old; m+=0.025;
	}
	cout<<"done"<<endl;
return 0;
}
