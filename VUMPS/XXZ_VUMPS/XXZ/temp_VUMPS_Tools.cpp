UniTensor initial_A(int d, int D, int n);
UniTensor NL_make(UniTensor ATC);
UniTensor NR_make(UniTensor ATC);
void R_Arn_Tens_meth(UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval);
void L_Arn_Tens_meth(UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval);
void Canonical(UniTensor A, UniTensor& gamma, UniTensor& lambda, Matrix& l_mat, Matrix& r_mat);
void Mix_cano(UniTensor& A, UniTensor& A_C, UniTensor& A_L, UniTensor& A_R, UniTensor& C);
UniTensor K_OP(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor r, UniTensor& l);
UniTensor K_OPT(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor& r, UniTensor l);
UniTensor BOP_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r);
UniTensor BOPT_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r);
double Lanczos_AC(UniTensor& GS, UniTensor& EnvL, UniTensor& WC, UniTensor& EnvR, double Error_Lanczos);
UniTensor update_AC(UniTensor& A_C, UniTensor& BL, UniTensor& BR, UniTensor& MPO, double e, double err_H);
double Lanczos_C(UniTensor& C, UniTensor& BL, UniTensor& BR, double Error_Lanczos);
UniTensor update_C(UniTensor& C, UniTensor& BL, UniTensor& BR, UniTensor& MPO, double e, double err_H);
UniTensor B2_make(UniTensor& A_C, UniTensor A_L, UniTensor A_R, UniTensor& MPO, UniTensor& BL, UniTensor& BR, UniTensor NL, UniTensor NR);
void A_expand(UniTensor& A, int D_tilde);
//void Dy_bond_update(UniTensor B2, int D, int D_tilde, UniTensor& A_L, UniTensor& A_R, UniTensor& C);

UniTensor initial_A(int d, int D, int n){
	Bond bd0(BD_IN, d), bd1(BD_OUT, D), bd2(BD_OUT, D);
	vector<Bond> bonds={bd0, bd1, bd2};
	UniTensor A(CTYPE, bonds); UniTensor mi(CTYPE, bonds);
	for(int i=0; i<n; i++){
		A.randomize(); mi.randomize();
	}
	bonds.clear();
	A=A+(-1.0)*mi; A*=(1.0/A.norm());
return A;
}

void As_init(int d, int D, UniTensor& A_L, UniTensor& A_R, UniTensor& A_C, UniTensor& C){
	A_L=initial_A(d, D, 1); A_R=initial_A(d, D, 1); A_C=initial_A(d, D, 1);
	Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
	vector<Bond> bonds={bdi, bdo};
	UniTensor C_temp(CTYPE, bonds);
	C_temp.randomize(); C=C_temp;
	C_temp.randomize(); C=C+(-1.0)*C_temp;
return;
}

UniTensor NL_make(UniTensor ATC){
	int D=ATC.bond(0).dim();
	int d=ATC.bond(2).dim();
	int m=D;
	int n=D*d;
        int info;
        int lwork=-1;
	complex <double> workt;
	complex<double> *u, *vt, *elem;
	double *s, *rwork;
	u=(complex<double>*)malloc((m*m)*sizeof(complex<double>));
	vt=(complex<double>*)malloc((n*n)*sizeof(complex<double>));
	s=(double*)malloc((m*n)*sizeof(double));
	rwork=(double*)malloc(5*m*sizeof(double));
	elem=(complex<double>*)malloc(m*n*sizeof(complex<double>));
	
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
	int per_ATC[]={4, 3, 0}; ATC.permute(per_ATC, 1);
	
        Matrix ATC_T_mat=ATC.getBlock(CTYPE).transpose();
        for (int i=0;i<m*n;i++){elem[i]=ATC_T_mat.at(CTYPE,i);}

        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,&workt,&lwork,rwork,&info);
        lwork = (int)workt.real();
        complex<double> *work = (complex<double>*)malloc(lwork*sizeof(std::complex<double>));
        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,work,&lwork,rwork,&info);

        Matrix U_ATC(CTYPE,m,m);U_ATC.setElem(u);U_ATC.transpose();
        Matrix S_ATC(CTYPE,m,n,true);S_ATC.setElem(s);
        Matrix VT_ATC(CTYPE,n,n);VT_ATC.setElem(vt);VT_ATC.transpose();
        Matrix VLT_mat(CTYPE,n-m,n);
        for(int i=0;i<n*(n-m);i++){VLT_mat.at(CTYPE,i)=VT_ATC.at(CTYPE,m*n+i);}
        Matrix VL_mat=VLT_mat.cTranspose(CTYPE);VLT_mat.cTranspose(CTYPE);
	free(u); free(vt); free(s); free(rwork); free(elem);

        Bond bd_VL_0(BD_IN,D);Bond bd_VL_1(BD_IN,d);Bond bd_VL_2(BD_OUT,D*(d-1));
        vector <Bond> bonds_VL;
        bonds_VL.push_back(bd_VL_0);bonds_VL.push_back(bd_VL_1);bonds_VL.push_back(bd_VL_2);
        UniTensor VL(CTYPE,bonds_VL,"NL");VL.putBlock(VL_mat);
        int label_VL[]={0,1,2};VL.setLabel(label_VL);

return VL;
}

UniTensor NR_make(UniTensor ATC){
	int D=ATC.bond(0).dim();
	int d=ATC.bond(2).dim();
	int m=D;
	int n=D*d;
        int info;
        int lwork=-1;//2*min(m,n)+max(m,n);
	complex<double> *u, *vt, *elem;
	double *s, *rwork;
	u=(complex<double>*)malloc((m*m)*sizeof(complex<double>));
	vt=(complex<double>*)malloc((n*n)*sizeof(complex<double>));
	s=(double*)malloc((m*n)*sizeof(double));
	rwork=(double*)malloc(5*m*sizeof(double));
	elem=(complex<double>*)malloc(m*n*sizeof(complex<double>));
        complex <double> workt;      
 
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
	int per_ATC[]={3, 4, 0}; ATC.permute(per_ATC, 1);
	
        Matrix ATC_T_mat=ATC.getBlock(CTYPE).transpose();
        for (int i=0;i<m*n;i++){elem[i]=ATC_T_mat.at(CTYPE,i);}

        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,&workt,&lwork,rwork,&info);
        lwork = (int)workt.real();
        complex<double> *work = (complex<double>*)malloc(lwork*sizeof(std::complex<double>));
        zgesvd("A","A",&m,&n,elem,&m,s,u,&m,vt,&n,work,&lwork,rwork,&info);

        Matrix U_ATC(CTYPE,m,m);U_ATC.setElem(u);U_ATC.transpose();
        Matrix S_ATC(CTYPE,m,n,true);S_ATC.setElem(s);
        Matrix VT_ATC(CTYPE,n,n);VT_ATC.setElem(vt);VT_ATC.transpose();
        Matrix VRT_mat(CTYPE,n-m,n);
        for(int i=0;i<n*(n-m);i++){VRT_mat.at(CTYPE,i)=VT_ATC.at(CTYPE,m*n+i);}
        Matrix VR_mat=VRT_mat; VR_mat.cTranspose(CTYPE);
	free(u); free(vt); free(s); free(rwork); free(elem);

        Bond bd_VR_0(BD_IN,D);Bond bd_VR_1(BD_IN,d);Bond bd_VR_2(BD_OUT,D*(d-1));
        vector <Bond> bonds_VR;
        bonds_VR.push_back(bd_VR_0);bonds_VR.push_back(bd_VR_1);bonds_VR.push_back(bd_VR_2);
        UniTensor VR(CTYPE,bonds_VR,"NR");VR.putBlock(VR_mat); VR.transpose();
return VR;
}

void NLNR_test(UniTensor  ATC){
	UniTensor NL=NL_make(ATC);
	UniTensor NR=NR_make(ATC); NR.transpose();
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
	int labels_NL[]={3, 0, 1}; NL.setLabel(labels_NL);
	int labels_NR[]={4, 0, 1}; NR.setLabel(labels_NR);
	//NL.printDiagram(); 
	//NR.printDiagram(); 
	cout<<"othgonal test"<<endl;
	cout<<setprecision(15)<<contract(ATC, NL).norm()<<endl;;
	cout<<setprecision(15)<<contract(ATC, NR).norm()<<endl;

	vector<Bond> bonds={NL.bond(2), NL.bond(2)};
	UniTensor I(CTYPE, bonds);
	bonds.clear();
	I.permute(1);I.identity();
	cout<<"normal test"<<endl;
	UniTensor NLTC=NL, NRTC=NR;
	NLTC.cTranspose(CTYPE); NRTC.cTranspose(CTYPE);
	int labels_NLTC[]={-1, 3, 0}; NLTC.setLabel(labels_NLTC);
	int labels_NRTC[]={-1, 4, 0}; NRTC.setLabel(labels_NRTC);
	UniTensor Lte, Rte;
	Lte=contract(NL, NLTC); Lte.permute(1);
	Rte=contract(NR, NRTC); Rte.permute(1);
	cout<<(Lte+(-1.0)*I).norm()<<endl;;
	cout<<(Rte+(-1.0)*I).norm()<<endl;;
return;
}

void R_Arn_Tens_meth(UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval){
	clock_t Start, Finish;	double Duration;
	Start=clock();

	int Minstep=0; //int Maxstep=1000;
	int first_step=0;

        bool ArnConverge=false; bool Arn_step2=false;
	double err=1.0;
        int Arnstep=0; int Arniter=0;
        int D=pow(A.bond(1).dim(),2); 
        int idx=0;
        eigval=complex<double> (1.0, 0);
	complex<double> eigval_old;
        vector<Matrix> Qs;
	Matrix eigvec(CTYPE, D, 1); Matrix eigvec_old;
        Matrix Hma(CTYPE, 2, 2);Matrix Hma2 ;
	vector<Matrix> Eigs;
	int labels_A[]{0, 1, 2 }; int labels_ATC[]={3, 4, 0};
	A.setLabel(labels_A); ATC.setLabel(labels_ATC);
        eigTens.permute(2);
	UniTensor eigTens_temp(CTYPE, eigTens.bond());
	eigTens*=(1.0/eigTens.norm());
	eigvec=eigTens.getBlock();
	int labels_r1[]={2, 4};
	eigTens.setLabel(labels_r1);
	eigTens_temp.setLabel(labels_r1);
	Qs.push_back(eigvec);
	while((ArnConverge==false)&&(Arnstep<=Arnstep_max)){
		Arnstep++; //cout<<"Arnstep="<<Arnstep<<endl;
                eigTens.putBlock(eigvec);
                eigTens *= (1.0/eigTens.norm());
                eigval_old=eigval;
		eigTens=contract(A, eigTens, true); eigTens=contract(eigTens, ATC, true); 
		eigTens.setLabel(labels_r1); eigTens.permute(2);
		eigvec=eigTens.getBlock();
		Hma.resize(CTYPE, Arnstep+1, Arnstep+1) ;
                for(int i=0; i<Arnstep; i++){//cout<<"i="<<i<<endl;
                        Hma.at(CTYPE, i*(Arnstep+1)+(Arnstep-1))=((Qs.at(i).cTranspose(CTYPE))*eigvec).at(CTYPE,0);
                        Qs.at(i).cTranspose(CTYPE);
                        eigvec=eigvec+(-1.0)*Hma.at(CTYPE, i*(Arnstep+1)+Arnstep-1)*Qs.at(i);
		}
                Hma.at(CTYPE, (Arnstep+1)*(Arnstep)+Arnstep-1)=eigvec.norm();
                eigvec *= (1.0/Hma.at(CTYPE, (Arnstep+1)*(Arnstep)+Arnstep-1));
		Qs.push_back(eigvec);
		if(Arnstep>=(first_step)){
                        Hma2=Hma; Hma2.resize(CTYPE, Arnstep, Arnstep); //cout<<Hma2<<endl;
                        Eigs=Hma2.eig();
                        eigval=Eigs.at(0).at(CTYPE, 0);
                        for(int i=0; i<Eigs.at(0).col(); i++){
                                if((real(eigval)-real(Eigs.at(0).at(CTYPE, i)))<mech_acc){idx=i; eigval=Eigs.at(0).at(CTYPE, i);}
                        }

                	if(Arnstep==D){ArnConverge=true;}
                	else if(Arn_step2==true){
				Matrix P; P=Eigs.at(1).inverse(); P.transpose();
	                        Matrix eigvec_temp(CTYPE, D, 1);
        	                eigvec_temp.set_zero();
                	        for(int i=0; i<Arnstep; i++){eigvec_temp+=P.at(CTYPE, i+Arnstep*idx)*Qs.at(i);}
                        	eigvec_temp *=(1.0/eigvec_temp.norm());
				eigTens_temp.putBlock(eigvec_temp);
				eigTens_temp=contract(A, eigTens_temp, true); eigTens_temp=contract(eigTens_temp, ATC, true);
				eigTens_temp.setLabel(labels_r1); eigTens_temp.permute(2);
				err=(eigTens_temp.getBlock()+(-1.0)*eigval*eigvec_temp).norm(); //cout<<err<<endl;
				if(((err<=Arnacc)&&(Arnstep>Minstep))||(Arnstep==D)){ArnConverge=true;}
                	}
                	if(Arn_step2==false){
                        	err=norm(eigval-eigval_old); //cout<<err<<endl;
                       		if(((err<=mech_acc)&&(Arnstep>Minstep))||(Arnstep==D)){Arn_step2=true;}
                	}
                }
        }
	if (ArnConverge==false)cout<<"Arnoldi not converge !!!"<<endl;
        Matrix T; T=Eigs.at(1).inverse(); T.transpose();
        eigvec.set_zero();

        for(int i=0; i<Arnstep; i++){eigvec+=T.at(CTYPE, i+Arnstep*idx)*Qs.at(i);}
        eigvec *=(1.0/eigvec.norm()); eigTens.putBlock(eigvec); eigTens.permute(1);
	//eigTens_temp=eigTens;
	//eigTens_temp=contract(A, eigTens_temp, true); eigTens_temp=contract(eigTens_temp, ATC, true);
	//eigTens_temp.setLabel(labels_r1); eigTens_temp.permute(2);
	double theta_angle; theta_angle=atan(((eigTens.getBlock().at(CTYPE,0)).imag())/((eigTens.getBlock().at(CTYPE,0)).real()));
	complex<double> phase; phase=complex<double>(cos(theta_angle),(-1.0)*sin(theta_angle));
	eigTens=eigTens*phase;
        //cout<<"eigtest= "<<setprecision(15)<<(eigTens_temp.getBlock()+(-1.0)*eigval*eigvec).norm()<<endl;
        //cout<<"Arnstep= "<<Arnstep<<endl;
        //cout<<"eigval=" <<setprecision(15)<<eigval<<endl;
	
	Finish=clock();
	Duration=(double)(Finish-Start)/CLOCKS_PER_SEC;
	//cout<<"Arnoldi Tensor type spend "<<Duration<<" sconds."<<endl;
return;
}

void L_Arn_Tens_meth(UniTensor A, UniTensor ATC, UniTensor& eigTens, complex<double>& eigval){
	int labels_l1[]={1, 3}; eigTens.setLabel(labels_l1);
	int labels_Al[]={0, 2, 1}; int labels_ATCl[]={4, 3, 0};
	A.permute(labels_Al, 1); ATC.permute(labels_ATCl, 2);
	R_Arn_Tens_meth(A, ATC, eigTens, eigval);
	eigTens.setLabel(labels_l1);
return;
}

void Canonical(UniTensor A, UniTensor& gamma, UniTensor& lambda, Matrix& l_mat, Matrix& r_mat){
//please put in the uni tensor in this order: physical bond(0) => left virtual bond(1) => right virtual bond(2)
	int labels_A[]={0, 1, 2}; A.setLabel(labels_A); A.permute(1);
	int D=A.bond(1).dim(); int d=A.bond(0).dim();
	
//make gamma-lambda representation or A
	int label_gamma1[]={1, 0, 2}; int label_gamma2[]={0, 1, 2};
        int labels_A2[]={1, 2, 0}; A.permute(labels_A2, 1);
        vector<Matrix> SVDs; SVDs=(A.getBlock()).svd();
        UniTensor gamma_temp(CTYPE, A.bond()); gamma=gamma_temp;
	gamma.setLabel(labels_A2); //gamma.printDiagram();
        Matrix gamma_mat; gamma_mat=SVDs.at(2);
        gamma.putBlock(gamma_mat); //gamma.printDiagram();
        gamma.permute(label_gamma1, 2);
        gamma_mat=(gamma.getBlock()*SVDs.at(0));
        gamma.putBlock(gamma_mat); //gamma.printDiagram();
        gamma.permute(label_gamma2, 1);
        Bond bdi(BD_IN, D); Bond bdo(BD_OUT, D);
	vector<Bond> bonds;
        bonds.push_back(bdi); bonds.push_back(bdo);
        UniTensor lambda_temp(CTYPE, bonds, "lambda"); lambda=lambda_temp;
        bonds.clear();
        lambda.putBlock(SVDs.at(1));

//Make canonical form for A
        int labels_lambda[]={1, 3}; lambda.setLabel(labels_lambda);
	int labels_A3[]={0, 3, 2};
	A=contract(gamma, lambda); A.permute(labels_A3, 1);
	A.setLabel(label_gamma2);

	complex<double> eigval;
	UniTensor ATC; ATC=A; ATC.cTranspose();
	int labels_ATC[]={3, 4, 0}; ATC.setLabel(labels_ATC);
	bonds.push_back(A.bond(1)); bonds.push_back(A.bond(2));
	UniTensor l(CTYPE, bonds, "l"); l.permute(1); l.putBlock(l_mat); 
	UniTensor r(CTYPE, bonds, "r"); r.permute(1); r.putBlock(r_mat); 
	bonds.clear();
	l *= (1.0)/(l.getBlock().norm());  r *= (1.0)/(r.getBlock().norm());
	int label_l[]={1,3}; int label_r[]={2, 4}; 
	l.setLabel(label_l); r.setLabel(label_r);
	
	L_Arn_Tens_meth(A, ATC, l, eigval);	
	l_mat=l.getBlock();
	double ita; ita=real(eigval);

	//Matrix l_mat; l_mat=l.getBlock(); //l_mat.transpose();
	vector <Matrix> Eighs; Eighs=l.getBlock().eigh();
	if(Eighs.at(0).at(0,0)<0){(Eighs.at(0))*=(-1.0); l*=(-1.0);}
	Matrix Pt=Eighs.at(1); Matrix P=Pt; P.cTranspose(CTYPE);
	RtoC(Eighs.at(0));
	for(int i=0; i<D; i++){
		Eighs.at(0).at(RTYPE,i)=pow(Eighs.at(0).at(RTYPE,i), 0.5);
	}
	UniTensor L; L=l;
	L.putBlock(P*Eighs.at(0)*Pt); //A.printDiagram();
	int labels_L[]={1, -1};	L.setLabel(labels_L);
	UniTensor L_inv; L_inv=L;
	Matrix L_inv_mat;
	for(int i=0; i<D; i++){Eighs.at(0).at(RTYPE,i)=1.0/(Eighs.at(0).at(RTYPE,i));} 
	L_inv.putBlock(P*Eighs.at(0)*Pt);
	
	int labels_lambda2[]={2, 3}; lambda.setLabel(labels_lambda2);
	A=contract(gamma, lambda); 
	int labels_A4[]={0, 1, 3}; A.permute(labels_A4, 1);
	A.setLabel(label_gamma2);
	ATC=A; ATC.cTranspose(); ATC.setLabel(labels_ATC);
	
	R_Arn_Tens_meth(A, ATC, r, eigval);
	r_mat=r.getBlock();
	Eighs.clear();
	Eighs=r.getBlock().eigh();//cout<<Eighs.at(0)<<endl;
	if(Eighs.at(0).at(0,0)<0){(Eighs.at(0))*=(-1.0); r*=(-1.0);}
	Matrix Tt=Eighs.at(1); Matrix T=Tt; T.cTranspose(CTYPE);
	RtoC(Eighs.at(0));
        for(int i=0; i<D; i++){
                Eighs.at(0).at(RTYPE,i)=pow(Eighs.at(0).at(RTYPE,i), 0.5);
        }

	UniTensor R; R=r;
	R.putBlock(T*Eighs.at(0)*Tt); //A.printDiagram();
	int labels_R[]={2, -2}; R.setLabel(labels_R);
	UniTensor R_inv; R_inv=R;
	Matrix R_inv_mat;
	for(int i=0; i<D; i++){Eighs.at(0).at(RTYPE,i)=1.0/(Eighs.at(0).at(RTYPE,i));}
	R_inv.putBlock(T*Eighs.at(0)*Tt);

	int labels_lambda3[]={1, 2};lambda.setLabel(labels_lambda3);	
	lambda=contract(lambda, L); lambda=contract(lambda, R); 
	lambda.setLabel(labels_lambda);
	SVDs.clear();
	SVDs=lambda.getBlock().svd();
	lambda.putBlock(SVDs.at(1));
	UniTensor U(CTYPE, l.bond()); UniTensor V(CTYPE, l.bond());
	U.putBlock(SVDs.at(0)); V.putBlock(SVDs.at(2));
	
	Network gamma_net("./Diagrams/gamma.net");
	gamma_net.putTensor("gam", gamma); gamma_net.putTensor("U", U); gamma_net.putTensor("V", V);
	gamma_net.putTensor("R_inv", R_inv); gamma_net.putTensor("L_inv", L_inv);
	gamma=gamma_net.launch();
	gamma.setLabel(labels_A);		
	gamma.permute(1);
	gamma *=pow(ita, -0.5);

return;
}

void Mix_cano(UniTensor& A, UniTensor& A_C, UniTensor& A_L, UniTensor& A_R, UniTensor& C){
	int labels_A[]={0, 1, 2};
	UniTensor gamma;
	int D=A.bond(1).dim();
	Bond bdi(BD_IN, D), bdo(BD_OUT, D);
	vector<Bond> bonds={bdi, bdo};
	UniTensor temp(CTYPE, bonds); temp.randomize();
	C=temp;
	bonds.clear();
	Matrix mat(CTYPE, D, D); mat.randomize();
	UniTensor A_old=A;
	Canonical(A, gamma, C, mat, mat); 
	int labels_C1[]={1, 4};	
	int labels_C2[]={2, 3};	
	A_L=contract(gamma, C); 
	int per_AL[]={0, 3, 2}; A_L.permute(per_AL, 1);
	A_L.setLabel(labels_A);
	
	C.setLabel(labels_C2);
	A_R=contract(gamma, C);
	int per_AR[]={0, 1, 3}; A_R.permute(per_AR, 1);
	A_R.setLabel(labels_A);

	A_C=contract(gamma, C);
	C.setLabel(labels_C1);
	A_C=contract(A_C, C);
	int per_AC[]={0, 4, 3}; A_C.permute(per_AC, 1);
	A_C.setLabel(labels_A);
return;
}

UniTensor K_OP(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor r, UniTensor& l){
	int labels_K[]={1, 3}; K.setLabel(labels_K);
	r.setLabel(labels_K);
	UniTensor KOP, K_AA; 
	K_AA=contract(K, A); K_AA=contract(K_AA, ATC); 
	KOP=K+ (-1.0)*K_AA+(contract(K, r).getBlock().at(CTYPE,0))*l;
return KOP;
}

UniTensor K_OPT(UniTensor K, UniTensor& A, UniTensor& ATC, UniTensor& r, UniTensor l){
        int labels_K[]={2, 4}; K.setLabel(labels_K);
        l.setLabel(labels_K);
        UniTensor KOP, K_AA;
        K_AA=contract(K, A); K_AA=contract(K_AA, ATC);
        KOP=K+ (-1.0)*K_AA+(contract(K, l).getBlock().at(CTYPE,0))*r;
return KOP;
}

complex<double> unidot(UniTensor a, UniTensor b){
	a.cTranspose(CTYPE);
	int labels_a[]={3, 1}; int labels_b[]={1, 3};
	a.setLabel(labels_a); b.setLabel(labels_b);
	complex<double> c;
	c=contract(a, b).getBlock().at(CTYPE, 0);
return c;
}
	
UniTensor BOP_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r){
	clock_t Start, Finish; double Duration;
	Start=clock();
	int D=l.getBlock().row();
	UniTensor K(CTYPE,l.bond());
	K.putBlock(K_mat);
	int label_K[]={1,3}; K.setLabel(label_K);
	
	int labels_C[]={2,4}; C.setLabel(labels_C);
        int label_K2[]={2,4};
	double K_err=1.0;int step_K=0;
	UniTensor constterm(CTYPE, l.bond());
	complex<double> h0= (contract(C, r).getBlock()).at(CTYPE,0);
	UniTensor lE=contract(l, A); lE=contract(lE, ATC);
	lE.setLabel(l.label());
	constterm=C+(-2.0)*h0*l+h0*lE;
	constterm.setLabel(label_K);

        UniTensor K_old=K;
	UniTensor KOP=K_OP(K, A, ATC, r, l);
	UniTensor rat; rat=constterm+(-1.0)*KOP; UniTensor rat_old=rat;
        UniTensor rat_deg=rat;
        
        complex<double> alph, rho, omeg, alph_old, rho_old, omeg_old, bet;
        alph=complex<double> (1.0, 0); rho=alph; omeg=alph;
        alph_old=alph; rho_old=rho; omeg_old=omeg;
	UniTensor p(CTYPE, K.bond()); p.set_zero(); UniTensor p_old=p;
	UniTensor v=p; UniTensor v_old=v; v.setLabel(label_K); p.setLabel(label_K);
	UniTensor h_uniT, s, t, h_uniT_old;

        while((K_err>bicgstab_acc)&&(step_K<=bicg_step_max)){step_K++; //cout<<K_err<<endl;
                rho=unidot(rat_deg, rat_old);
                bet=(rho/rho_old)*(alph/omeg_old);
                p=rat_old+bet*(p_old+(-1.0)*omeg_old*v_old);
                v=K_OP(p, A, ATC, r, l); 
                alph=rho/(unidot(rat_deg, v));
                h_uniT=K_old+alph*p;

                s=rat_old+(-1.0)*alph*v;
                t=K_OP(s, A, ATC, r, l);
                omeg=(unidot(t, s))/(unidot(t, t));
                K=h_uniT+omeg*s;
                K_err=(K+(-1.0)*K_old).norm();
                rat=s+(-1.0)*omeg*t;
                rho_old=rho; omeg_old=omeg; rat_old=rat; p_old=p;v_old=v; K_old=K; h_uniT_old=h_uniT;
        }
	if(K_err>bicgstab_acc)cout<<"bicgstab not converge !!!"<<endl;
	K_mat=K.getBlock();//K.printDiagram();
	//cout<<"step_K= "<<step_K<<endl;
	int labels_a[]={0, 1};
	K.setLabel(labels_a);
	
	//cout<<"BO step= "<<step_K<<endl;
	Finish=clock();
	Duration=(double)(Finish-Start)/CLOCKS_PER_SEC; //cout<<"K_make time is "<<Duration<<"seconds. "<<endl;
return K;
}

UniTensor BOPT_make_BiCGSTAB(Matrix& K_mat, UniTensor A, UniTensor ATC, UniTensor C, UniTensor& l, UniTensor& r){
        clock_t Start, Finish; double Duration;
        int D=r.getBlock().row();
        UniTensor K(CTYPE,r.bond());
        K.putBlock(K_mat);
        int label_K[]={2,4}; K.setLabel(label_K);
        int labels_C[]={1,3}; C.setLabel(labels_C);
        int label_K2[]={1,3};
        double K_err=1.0;int step_K=0;
        UniTensor constterm(CTYPE, l.bond());
        Start=clock();
	complex<double> h0= (contract(C, l).getBlock()).at(CTYPE,0);
	UniTensor Er=contract(r, A); Er=contract(Er, ATC);
	Er.setLabel(r.label());
        constterm=C+(-2.0)*h0*r+h0*Er;
	
        Finish=clock();
        constterm.setLabel(label_K);

        UniTensor K_old=K;
        UniTensor KOP=K_OPT(K, A, ATC, r, l);
        UniTensor rat; rat=constterm+(-1.0)*KOP; UniTensor rat_old=rat;
        UniTensor rat_deg=rat;

        complex<double> alph, rho, omeg, alph_old, rho_old, omeg_old, bet;
        alph=complex<double> (1.0, 0); rho=alph; omeg=alph;
        alph_old=alph; rho_old=rho; omeg_old=omeg;
        UniTensor p(CTYPE, K.bond()); p.set_zero(); UniTensor p_old=p;
        UniTensor v=p; UniTensor v_old=v; v.setLabel(label_K); p.setLabel(label_K);
        UniTensor h_uniT, s, t, h_uniT_old;

        while((K_err>bicgstab_acc)&&(step_K<=bicg_step_max)){step_K++;
                rho=unidot(rat_deg, rat_old); //cout<<K_err<<endl;
                bet=(rho/rho_old)*(alph/omeg_old);
                p=rat_old+bet*(p_old+(-1.0)*omeg_old*v_old);
                v=K_OPT(p, A, ATC, r, l);
                alph=rho/(unidot(rat_deg, v));
                h_uniT=K_old+alph*p;

                s=rat_old+(-1.0)*alph*v;
                t=K_OPT(s, A, ATC, r, l);
                omeg=(unidot(t, s))/(unidot(t, t));
                K=h_uniT+omeg*s;
                K_err=(K+(-1.0)*K_old).norm();
                rat=s+(-1.0)*omeg*t;
                rho_old=rho; omeg_old=omeg; rat_old=rat; p_old=p;v_old=v; K_old=K; h_uniT_old=h_uniT;
        }
	if(K_err>bicgstab_acc)cout<<"bicgstab not converge !!!"<<endl;
        K_mat=K.getBlock();//K.printDiagram();//cout<<"step_K= "<<step_K<<endl;
        int labels_a[]={0, 1};
        K.setLabel(labels_a);
	//cout<<"BOT step= "<<step_K<<endl;
        Duration=(double)(Finish-Start)/CLOCKS_PER_SEC; //cout<<"K_make time is "<<Duration<<"seconds. "<<endl;
return K;
}

double Lanczos_AC(UniTensor& A_C, UniTensor& BL, UniTensor& MPO, UniTensor& BR, double Error_Lanczos){
	//Error_Lanczos=1.0e-10;
	int LanIter=0; 
	Error_Lanczos=Error_Lanczos<Lanc_acc?Lanc_acc:Error_Lanczos;
	double Error_Lanczos_E=Error_Lanczos*0.01;
	bool LanConverge_E=false; bool LanConverge=false;
	size_t M=MaxLanIter;

	vector<double> Es;
	Matrix V = A_C.getBlock();
	Matrix OldV(V.row(), V.col()); OldV.set_zero();
	complex<double> *V_Elem=V.getElem(CTYPE);
	vector<complex<double>> Vs_Elem(V_Elem, V_Elem+V.row());
	vector<complex<double>> Vs_Elem2=Vs_Elem;
	double alpha = 0.0; double beta = 0.0;
	double *Alphas=(double*)malloc((M+1)*sizeof(double));
	double *Betas=(double*)malloc((M+1)*sizeof(double));
	double *As=(double*)malloc((M+1)*sizeof(double));
	double *Bs=(double*)malloc((M+1)*sizeof(double));
	memset(Alphas, 0, (M+1)*sizeof(double));
	memset(Betas, 0, (M+1)*sizeof(double));

	Network Lann("./Diagrams/Lanczos_AC.net");
	Lann.putTensor("BL", BL); Lann.putTensor("WC", MPO); Lann.putTensor("BR", BR);
	Network Lann2("./Diagrams/Lanczos_AC.net");
	Lann2.putTensor("BL", BL); Lann2.putTensor("WC", MPO); Lann2.putTensor("BR", BR);
	UniTensor A_C2=A_C;

	double E_error_Lan = 1.0;
	double error_Lan = 1.0;
	while(LanIter < MaxLanIter && LanConverge == false){
		Lann.putTensor("AC", A_C); //cout<<E_error_Lan<<endl;
		A_C = Lann.launch();
		Matrix W = A_C.getBlock(); Matrix WTC = W; WTC.cTranspose(CTYPE);
		alpha = real((WTC * V).at(CTYPE, 0)); Alphas[LanIter]=alpha;
		W = W + (-1.0)*alpha*V + (-1.0)*beta*OldV;
		beta = W.norm(); Betas[LanIter]=beta; 
		LanIter++;
		OldV = V;
		V = (1.0/beta) * W; A_C.putBlock(V);
		complex<double> *V_Elem=V.getElem(CTYPE); Vs_Elem.insert(Vs_Elem.end(), V_Elem, V_Elem+V.row());
		int info;
		memcpy(As, Alphas, LanIter*sizeof(double));
		memcpy(Bs, Betas, (LanIter-1)*sizeof(double));
		if(LanConverge_E==false){
			double *z=NULL; double *work=NULL;
			dstev((char*)"N", &LanIter, As, Bs, z, &LanIter, work, &info);
			Es.push_back(As[0]);
			if(LanIter>=MinLanIter)
				E_error_Lan=abs((As[0]-Es[LanIter-MinLanIter])/(abs(As[0])>1.0 ? abs(As[0]):1.0));
			if(E_error_Lan<Error_Lanczos_E && LanIter>=MinLanIter)LanConverge_E=true;	
		}
		else if(LanConverge_E==true){
			double *z=(double*)malloc(LanIter*LanIter*sizeof(double));
			double *work=(double*)malloc(4*LanIter*sizeof(double));
			dstev((char*)"V", &LanIter, As, Bs, z, &LanIter, work, &info);
			Es.push_back(As[0]);
			Matrix Tri(LanIter, LanIter);
			Tri.setElem(z); Tri.resize(1, LanIter);
			Matrix Vs(CTYPE, LanIter, V.row());
			Vs_Elem2=Vs_Elem;
			Vs_Elem2.erase(Vs_Elem2.begin()+LanIter*V.row(), Vs_Elem2.end());
			Vs.setElem(Vs_Elem2);
			Matrix AC_mat=Tri*Vs; AC_mat*=(1.0/AC_mat.norm());
			AC_mat.transpose(); 
			A_C2.putBlock(AC_mat); 
			Lann2.putTensor("AC", A_C2);
			UniTensor te=Lann2.launch();	
			error_Lan=(te+(-1.0)*As[0]*A_C2).norm();//cout<<error_Lan<<endl;
			free(z); free(work); 
			if(error_Lan<Error_Lanczos)LanConverge=true;
		}
	}
	int info;
	memcpy(As, Alphas, LanIter*sizeof(double));
	memcpy(Bs, Betas, (LanIter-1)*sizeof(double));
	double *z=(double*)malloc(LanIter*LanIter*sizeof(double));
	double *work=(double*)malloc(4*LanIter*sizeof(double));
	dstev((char*)"V", &LanIter, As, Bs, z, &LanIter, work, &info);
	Matrix Tri(LanIter, LanIter);
	Tri.setElem(z); Tri.resize(1, LanIter);
	Matrix Vs(CTYPE, LanIter, V.row());
	Vs_Elem.erase(Vs_Elem.begin()+LanIter*V.row(), Vs_Elem.end());
	Vs.setElem(Vs_Elem);	
	Matrix AC_mat=Tri*Vs; AC_mat*=(1.0/AC_mat.norm());
	AC_mat.transpose(); 
	A_C.putBlock(AC_mat); 
	double E=As[0];

	free(z); free(work); free(As); free(Bs); free(Alphas); free(Betas);
	Vs_Elem.clear(); Vs_Elem2.clear();

/*	Network Lannte("./Diagrams/Lanczos_AC.net");
	Lannte.putTensor("BL", BL); Lannte.putTensor("WC", MPO); Lannte.putTensor("BR", BR);
	Lannte.putTensor("AC", A_C);
	UniTensor test=Lannte.launch();
	cout<<"test= "<<setprecision(15)<<E<<" " <<(test+(-1.0)*E*A_C).norm()<<endl;*/

	if(LanConverge_E==false)cout<<"A_C Lanczos not converges"<<E_error_Lan<<endl;
return E;
}

UniTensor update_AC(UniTensor& A_C, UniTensor& BL, UniTensor& BR, UniTensor& MPO, double e, double err_H){
	A_C.permute(3);
	int per[]={-1, 3, 4};
	A_C*=(1.0/A_C.norm());
	e=Lanczos_AC(A_C, BL, MPO, BR, err_H); //cout<<e<<endl;
	A_C.permute(per,1);
	int labels_A[]={0, 1, 2};
	A_C.setLabel(labels_A);
return A_C;
}

double Lanczos_C(UniTensor& C, UniTensor& BL, UniTensor& BR, double Error_Lanczos){
	//Error_Lanczos=1.0e-10;
	int LanIter=0; 
	Error_Lanczos=Error_Lanczos<Lanc_acc?Lanc_acc:Error_Lanczos;
	double Error_Lanczos_E=Error_Lanczos*0.01;
	bool LanConverge_E=false; bool LanConverge=false;
	size_t M=MaxLanIter;

	vector<double> Es;
	Matrix V = C.getBlock();
	Matrix OldV(V.row(), V.col()); OldV.set_zero();
	complex<double> *V_Elem=V.getElem(CTYPE);
	vector<complex<double>> Vs_Elem(V_Elem, V_Elem+V.row());
	vector<complex<double>> Vs_Elem2=Vs_Elem;
	double alpha = 0.0; double beta = 0.0;
	double *Alphas=(double*)malloc((M+1)*sizeof(double));
	double *Betas=(double*)malloc((M+1)*sizeof(double));
	double *As=(double*)malloc((M+1)*sizeof(double));
	double *Bs=(double*)malloc((M+1)*sizeof(double));
	memset(Alphas, 0, (M+1)*sizeof(double));
	memset(Betas, 0, (M+1)*sizeof(double));

	Network Lann("./Diagrams/Lanczos_C.net");
	Lann.putTensor("BL", BL); Lann.putTensor("BR", BR);
	Network Lann2("./Diagrams/Lanczos_C.net");
	Lann2.putTensor("BL", BL); Lann2.putTensor("BR", BR);
	UniTensor C2=C;

	double E_error_Lan = 1.0;
	double error_Lan = 1.0;
	while(LanIter < MaxLanIter && LanConverge == false){
		Lann.putTensor("C", C); //cout<<E_error_Lan<<endl;
		C = Lann.launch();
		Matrix W = C.getBlock(); Matrix WTC = W; WTC.cTranspose(CTYPE);
		alpha = real((WTC * V).at(CTYPE, 0)); Alphas[LanIter]=alpha;
		W = W + (-1.0)*alpha*V + (-1.0)*beta*OldV;
		beta = W.norm(); Betas[LanIter]=beta; 
		LanIter++;
		OldV = V;
		V = (1.0/beta) * W; C.putBlock(V);
		complex<double> *V_Elem=V.getElem(CTYPE); Vs_Elem.insert(Vs_Elem.end(), V_Elem, V_Elem+V.row());
		int info;
		memcpy(As, Alphas, LanIter*sizeof(double));
		memcpy(Bs, Betas, (LanIter-1)*sizeof(double));
		if(LanConverge_E==false){
			double *z=NULL; double *work=NULL;
			dstev((char*)"N", &LanIter, As, Bs, z, &LanIter, work, &info);
			Es.push_back(As[0]);
			if(LanIter>=MinLanIter)
				E_error_Lan=abs((As[0]-Es[LanIter-MinLanIter])/(abs(As[0])>1.0 ? abs(As[0]):1.0));
			if(E_error_Lan<Error_Lanczos_E && LanIter>=MinLanIter)LanConverge_E=true;	
		}
		else if(LanConverge_E==true){
			double *z=(double*)malloc(LanIter*LanIter*sizeof(double));
			double *work=(double*)malloc(4*LanIter*sizeof(double));
			dstev((char*)"V", &LanIter, As, Bs, z, &LanIter, work, &info);
			Es.push_back(As[0]);
			Matrix Tri(LanIter, LanIter);
			Tri.setElem(z); Tri.resize(1, LanIter);
			Matrix Vs(CTYPE, LanIter, V.row());
			Vs_Elem2=Vs_Elem;
			Vs_Elem2.erase(Vs_Elem2.begin()+LanIter*V.row(), Vs_Elem2.end());
			Vs.setElem(Vs_Elem2);
			Matrix C_mat=Tri*Vs; C_mat*=(1.0/C_mat.norm());
			C_mat.transpose(); 
			C2.putBlock(C_mat); 
			Lann2.putTensor("C", C2);
			UniTensor te=Lann2.launch();	
			error_Lan=(te+(-1.0)*As[0]*C2).norm();//cout<<error_Lan<<endl;
			free(z); free(work); 
			if(error_Lan<Error_Lanczos)LanConverge=true;
		}
	}
	int info;
	memcpy(As, Alphas, LanIter*sizeof(double));
	memcpy(Bs, Betas, (LanIter-1)*sizeof(double));
	double *z=(double*)malloc(LanIter*LanIter*sizeof(double));
	double *work=(double*)malloc(4*LanIter*sizeof(double));
	dstev((char*)"V", &LanIter, As, Bs, z, &LanIter, work, &info);
	Matrix Tri(LanIter, LanIter);
	Tri.setElem(z); Tri.resize(1, LanIter);
	Matrix Vs(CTYPE, LanIter, V.row());
	Vs_Elem.erase(Vs_Elem.begin()+LanIter*V.row(), Vs_Elem.end());
	Vs.setElem(Vs_Elem);	
	Matrix C_mat=Tri*Vs; C_mat*=(1.0/C_mat.norm());
	C_mat.transpose(); 
	C.putBlock(C_mat); 
	double E=As[0];

	free(z); free(work); free(As); free(Bs); free(Alphas); free(Betas);
	Vs_Elem.clear(); Vs_Elem2.clear();

/*	Network Lannte("./Diagrams/Lanczos_AC.net");
	Lannte.putTensor("BL", BL); Lannte.putTensor("WC", MPO); Lannte.putTensor("BR", BR);
	Lannte.putTensor("AC", A_C);
	UniTensor test=Lannte.launch();
	cout<<"test= "<<setprecision(15)<<E<<" " <<(test+(-1.0)*E*A_C).norm()<<endl;*/

	if(LanConverge_E==false)cout<<"A_C Lanczos not converges"<<E_error_Lan<<endl;
return E;
}

UniTensor update_C(UniTensor& C, UniTensor& BL, UniTensor& BR,  double e, double err_H){
	C.permute(2);
	int per[]={3, 4};
	C*=(1.0/C.norm());
	e=Lanczos_C(C, BL, BR, err_H); //cout<<e<<endl;
	C.permute(per,1); 
	//C*=(1.0/C.norm());
	int labels_a[]={0, 1};C.setLabel(labels_a);
return C;
}

double Lanczos_A2C(UniTensor& A2_C, UniTensor& BL, UniTensor& MPO, UniTensor& BR, double Error_Lanczos){
	int MinLanIter=2; int MaxLanIter=2000;
	Error_Lanczos=1.0e-10;
	int LanIter=0; 
	Error_Lanczos=Error_Lanczos<1.0e-10?1.0e-10:Error_Lanczos;
	double Error_Lanczos_E=Error_Lanczos*0.01;
	bool LanConverge_E=false; bool LanConverge=false;
	size_t M=MaxLanIter;

	vector<double> Es;
	Matrix V = A2_C.getBlock();
	Matrix OldV(V.row(), V.col()); OldV.set_zero();
	complex<double> *V_Elem=V.getElem(CTYPE);
	vector<complex<double>> Vs_Elem(V_Elem, V_Elem+V.row());
	vector<complex<double>> Vs_Elem2=Vs_Elem;
	double alpha = 0.0; double beta = 0.0;
	double *Alphas=(double*)malloc((M+1)*sizeof(double));
	double *Betas=(double*)malloc((M+1)*sizeof(double));
	double *As=(double*)malloc((M+1)*sizeof(double));
	double *Bs=(double*)malloc((M+1)*sizeof(double));
	memset(Alphas, 0, (M+1)*sizeof(double));
	memset(Betas, 0, (M+1)*sizeof(double));

	Network Lann("./Diagrams/Lanczos_A2C.net");
	Lann.putTensor("BL", BL); Lann.putTensor("WC1", MPO);  
	Lann.putTensor("WC2", MPO);Lann.putTensor("BR", BR);
	Network Lann2("./Diagrams/Lanczos_A2C.net");
	Lann2.putTensor("BL", BL); Lann2.putTensor("WC1", MPO); 
	Lann2.putTensor("WC2", MPO); Lann2.putTensor("BR", BR);
	UniTensor A2_C2=A2_C;

	double E_error_Lan = 1.0;
	double error_Lan = 1.0;
	while(LanIter < MaxLanIter && LanConverge == false){
		Lann.putTensor("A2_C", A2_C); //cout<<E_error_Lan<<endl;
		A2_C = Lann.launch();
		Matrix W = A2_C.getBlock(); Matrix WTC = W; WTC.cTranspose(CTYPE);
		alpha = real((WTC * V).at(CTYPE, 0)); Alphas[LanIter]=alpha;
		W = W + (-1.0)*alpha*V + (-1.0)*beta*OldV;
		beta = W.norm(); Betas[LanIter]=beta; 
		LanIter++;
		OldV = V;
		V = (1.0/beta) * W; A2_C.putBlock(V);
		complex<double> *V_Elem=V.getElem(CTYPE); Vs_Elem.insert(Vs_Elem.end(), V_Elem, V_Elem+V.row());
		int info;
		memcpy(As, Alphas, LanIter*sizeof(double));
		memcpy(Bs, Betas, (LanIter-1)*sizeof(double));
		if(LanConverge_E==false){
			double *z=NULL; double *work=NULL;
			dstev((char*)"N", &LanIter, As, Bs, z, &LanIter, work, &info);
			Es.push_back(As[0]);
			if(LanIter>=MinLanIter)
				E_error_Lan=abs((As[0]-Es[LanIter-MinLanIter])/(abs(As[0])>1.0 ? abs(As[0]):1.0));
			if(E_error_Lan<Error_Lanczos_E && LanIter>=MinLanIter)LanConverge_E=true;	
		}
		else if(LanConverge_E==true){
			double *z=(double*)malloc(LanIter*LanIter*sizeof(double));
			double *work=(double*)malloc(4*LanIter*sizeof(double));
			dstev((char*)"V", &LanIter, As, Bs, z, &LanIter, work, &info);
			Es.push_back(As[0]);
			Matrix Tri(LanIter, LanIter);
			Tri.setElem(z); Tri.resize(1, LanIter);
			Matrix Vs(CTYPE, LanIter, V.row());
			Vs_Elem2=Vs_Elem;
			Vs_Elem2.erase(Vs_Elem2.begin()+LanIter*V.row(), Vs_Elem2.end());
			Vs.setElem(Vs_Elem2);
			Matrix A2C_mat=Tri*Vs; A2C_mat*=(1.0/A2C_mat.norm());
			A2C_mat.transpose(); 
			A2_C2.putBlock(A2C_mat); 
			Lann2.putTensor("A2_C", A2_C2);
			UniTensor te=Lann2.launch();	
			error_Lan=(te+(-1.0)*As[0]*A2_C2).norm();//cout<<error_Lan<<endl;
			free(z); free(work); 
			if(error_Lan<Error_Lanczos)LanConverge=true;
		}
	}
	int info;
	memcpy(As, Alphas, LanIter*sizeof(double));
	memcpy(Bs, Betas, (LanIter-1)*sizeof(double));
	double *z=(double*)malloc(LanIter*LanIter*sizeof(double));
	double *work=(double*)malloc(4*LanIter*sizeof(double));
	dstev((char*)"V", &LanIter, As, Bs, z, &LanIter, work, &info);
	Matrix Tri(LanIter, LanIter);
	Tri.setElem(z); Tri.resize(1, LanIter);
	Matrix Vs(CTYPE, LanIter, V.row());
	Vs_Elem.erase(Vs_Elem.begin()+LanIter*V.row(), Vs_Elem.end());
	Vs.setElem(Vs_Elem);	
	Matrix AC_mat=Tri*Vs; AC_mat*=(1.0/AC_mat.norm());
	AC_mat.transpose(); 
	A2_C.putBlock(AC_mat); 
	double E=As[0];

	free(z); free(work); free(As); free(Bs); free(Alphas); free(Betas);
	Vs_Elem.clear(); Vs_Elem2.clear();

/*	Network Lannte("./Diagrams/Lanczos_AC.net");
	Lannte.putTensor("BL", BL); Lannte.putTensor("WC", MPO); Lannte.putTensor("BR", BR);
	Lannte.putTensor("AC", A_C);
	UniTensor test=Lannte.launch();
	cout<<"test= "<<setprecision(15)<<E<<" " <<(test+(-1.0)*E*A_C).norm()<<endl;*/

	if(LanConverge_E==false)cout<<"A2_C Lanczos not converges"<<E_error_Lan<<endl;
return E;
}

double err1(UniTensor& A_C,UniTensor A_R,  UniTensor& BL, UniTensor& BR, UniTensor& MPO, double e, double err_H){
	double err=1.0;
	int D=A_C.bond(1).dim();
	int labels_A_R[]={-1, 2, 3}; A_R.setLabel(labels_A_R);
	UniTensor A2_C=contract(A_C, A_R);
	int per1[]={0, 1, -1, 3};
	A2_C.permute(per1, 4);
	int per2[]={-3, 3, -4, 4};
	A2_C*=(1.0/A2_C.norm()); 
	e=Lanczos_A2C(A2_C, BL, MPO, BR, err_H); //cout<<e<<endl;
	A2_C.permute(per2,2);
	vector<Matrix> SVDs; SVDs=A2_C.getBlock().svd();
	for(int i=0; i<D; i++)err-=norm(SVDs.at(1).at(CTYPE, i));
return err;
}

void update_AL(UniTensor& A_L, UniTensor A_C, UniTensor C, double& err_L){
	A_L*=1.0/(A_L.norm());
	int labels_A[]={0, 1, 2};
	int labels_C[]={2, 3};
	UniTensor CTC=C; CTC.cTranspose(CTYPE);
	A_L.setLabel(labels_A);
	C.setLabel(labels_C);
	UniTensor ACC=A_C;
	ACC.permute(2);
	ACC.putBlock(ACC.getBlock()*CTC.getBlock());
	ACC.setLabel(labels_A);
	Matrix ACC_mat=ACC.getBlock();
	vector<Matrix> SVDs;
	SVDs=ACC_mat.svd();
	A_L.permute(2);
	A_L.putBlock(SVDs.at(0)*SVDs.at(2));
	SVDs.clear();
	A_L.permute(1);
	ACC=contract(A_L,C);
	int per[]={0, 1, 3};;
	ACC.permute(per,1);
	err_L=(A_C+(-1.0)*ACC).norm();
return;
}

void update_AR(UniTensor& A_R, UniTensor A_C, UniTensor C, double& err_R){
	A_R*=1.0/(A_R.norm());
	int labels_A[]={0, 1, 2};
	int labels_C[]={3, 1}; 
	UniTensor CTC=C; CTC.cTranspose(CTYPE); 
	UniTensor ACC=A_C;
	int per[]={1, 0, 2 }; 
	ACC.permute(per, 1);
	Matrix ACC_mat=CTC.getBlock()*ACC.getBlock();
	vector<Matrix> SVDs;
	SVDs=ACC_mat.svd();
	A_R.permute(per, 1);
	A_R.putBlock(SVDs.at(0)*SVDs.at(2));
	SVDs.clear();
	A_R.permute(labels_A, 1);
	C.setLabel(labels_C);
	ACC=contract(A_R, C);
	int per2[]={0, 3, 2};
	ACC.permute(per2, 1);
	err_R=(A_C+(-1.0)*ACC).norm();
return;
}

void update_AL_opt(UniTensor& A_L, UniTensor A_C, UniTensor C, double& err_L){
	A_L*=1.0/(A_L.norm());
	int labels_A[]={0, 1, 2};
	int labels_C[]={2, 3};
	A_L.setLabel(labels_A);
	C.setLabel(labels_C);
	A_C.permute(2);
	A_C.setLabel(labels_A);
	vector<Matrix> SVD1s;
	SVD1s=A_C.getBlock().svd();
	vector<Matrix> SVD2s;
	SVD2s=C.getBlock().svd();
	SVD2s.at(0).cTranspose(); 
	SVD2s.at(2).cTranspose(); 
	A_L.permute(2);
	A_L.putBlock(SVD1s.at(0)*SVD1s.at(2)*SVD2s.at(2)*SVD2s.at(0));
	SVD1s.clear(); SVD2s.clear();
	A_L.permute(1);
	A_C.permute(1);
	UniTensor ACC=contract(A_L,C);
	int per[]={0, 1, 3};
	ACC.permute(per,1);
	err_L=(A_C+(-1.0)*ACC).norm();
return;
}

void update_AR_opt(UniTensor& A_R, UniTensor A_C, UniTensor C, double& err_R){
	A_R*=1.0/(A_R.norm());
	int labels_A[]={0, 1, 2};
	int labels_C[]={3, 1}; 
	UniTensor ACC=A_C;
	int per[]={1, 0, 2 }; 
	ACC.permute(per, 1);
	vector<Matrix> SVD1s;
	SVD1s=ACC.getBlock().svd();
	vector<Matrix> SVD2s;
	SVD2s=C.getBlock().svd();
	SVD2s.at(0).cTranspose(CTYPE); 
	SVD2s.at(2).cTranspose(CTYPE); 
	A_R.permute(per, 1);
	A_R.putBlock(SVD2s.at(2)*SVD2s.at(0)*SVD1s.at(0)*SVD1s.at(2));
	SVD1s.clear(); SVD2s.clear();
	A_R.permute(labels_A, 1);
	C.setLabel(labels_C);
	ACC=contract(A_R, C);
	int per2[]={0, 3, 2};
	ACC.permute(per2, 1);
	err_R=(A_C+(-1.0)*ACC).norm();
return;
}

UniTensor B_make(UniTensor& BL, UniTensor& BR, UniTensor& MPO, UniTensor A_C, UniTensor C, UniTensor A_L){
	UniTensor B;
	A_C.permute(3); C.permute(2);
	Network Lann_AC("./Diagrams/Lanczos_AC.net");
	Lann_AC.putTensor("BL", BL); Lann_AC.putTensor("WC", MPO); 
	Lann_AC.putTensor("BR", BR); Lann_AC.putTensor("AC", A_C);
	UniTensor A_C_prime=Lann_AC.launch();
	Network Lann_C("./Diagrams/Lanczos_C.net");
	Lann_C.putTensor("BL", BL); Lann_C.putTensor("BR", BR); Lann_C.putTensor("C", C);
	UniTensor C_prime=Lann_C.launch();
	A_C.permute(1); C.permute(1);
	int labels_C[]={2, 3}; C.setLabel(labels_C);
	int labels_AL_C[]={0, 1, 3}; 
	B=contract(A_L, C); B.permute(labels_AL_C, 1);
	B=(-1.0)*B+A_C;
return B;
}

UniTensor B2_make(UniTensor& A_C, UniTensor A_L, UniTensor A_R, UniTensor& MPO, UniTensor& BL, UniTensor& BR, UniTensor NL, UniTensor NR){	
	int D=A_C.bond(1).dim();
	int labels_A_R[]={-1, 2, 3}; A_R.setLabel(labels_A_R);
	UniTensor A2_C=contract(A_C, A_R);
	int per1[]={0, 1, -1, 3};
	A2_C.permute(per1, 4);
	//A2_C*=(1.0/A2_C.norm()); 
	Network Lann("./Diagrams/Lanczos_A2C.net");
	Lann.putTensor("BL", BL); Lann.putTensor("WC1", MPO); 
	Lann.putTensor("WC2", MPO); Lann.putTensor("BR", BR);
	Lann.putTensor("A2_C", A2_C);
	UniTensor B2=Lann.launch();
	int labels_ATC[]={3, 4, 0};
	//A_L.cTranspose(CTYPE); 	A_R.cTranspose(CTYPE);
	//A_L.setLabel(labels_ATC) ;
	//A_R.setLabel(labels_ATC) ;
	UniTensor NLTC=NL; NLTC.cTranspose(CTYPE);
	UniTensor NRTC=NR; NRTC.cTranspose(CTYPE);
	int labels_NLTC[]={1, 3, -3}; int labels_NRTC[]={4, -4, 2};
	NLTC.setLabel(labels_NLTC); NRTC.setLabel(labels_NRTC); 
	//NLTC.printDiagram(); NRTC.printDiagram();B2.printDiagram();
	B2=contract(B2, NLTC); B2=contract(B2, NRTC);//B2.printDiagram();
return B2;
}

void Dy_bond_update(int D_tilde, UniTensor& A_C, UniTensor& A_L, UniTensor& A_R, UniTensor& C,
UniTensor& MPO, UniTensor& BL, UniTensor& BR, UniTensor& NL, UniTensor& NR){	
	int d=A_L.bond(0).dim();
	int D=A_L.bond(1).dim(); int del_D=D_tilde-D;
	UniTensor B2=B2_make(A_C, A_L, A_R, MPO, BL, BR, NL, NR);
	err2_v=pow(B2.norm(), 2.0);
	vector<Matrix> SVDs;
	SVDs=B2.getBlock().svd();
	int labels_A[]={0, 1, 2};
	SVDs.at(0).resize(B2.bond(0).dim(), del_D);
	SVDs.at(1).resize(del_D, del_D);
	SVDs.at(2).resize(del_D, B2.bond(1).dim());
	
	Bond bd_com(BD_IN, (d-1)*D); Bond bd_del(BD_OUT, del_D); 
	vector<Bond> bonds={bd_com, bd_del};
	UniTensor U(CTYPE, bonds);
	UniTensor V_deg(CTYPE, bonds); V_deg.transpose();
	bonds.clear();
	bonds={bd_del, bd_del};
	UniTensor S(CTYPE, bonds); S.permute(1);
	bonds.clear();
	U.putBlock(SVDs.at(0));
	S.putBlock(SVDs.at(1));
	V_deg.putBlock(SVDs.at(2));	
	int labels_NU[]={2, 3};	int labels_NV[]={-1, 2};
	U.setLabel(labels_NU); V_deg.setLabel(labels_NV);
	U=contract(U, NL); V_deg=contract(V_deg, NR);
	int per_U[]={1, 0, 3}; U.permute(per_U, 2);
	A_L.permute(1);
	A_expand(A_L, D_tilde);	A_expand(A_R, D_tilde);
	A_L.permute(2);
	Matrix mat=A_L.getBlock();
	for(int s=0; s<d; s++)
	for(int i=0; i<D;i++)
	for(int j=0; j<del_D; j++)mat.at(CTYPE, (s*D_tilde*D_tilde+D)+i*D_tilde+j)=U.at(CTYPE, s*(del_D*D)+(i)*del_D+j);	
	A_L.putBlock(mat);
	A_L.permute(1);

	int per_V[]={1, -1, 0};
	V_deg.permute(per_V, 2);
	A_R.permute(2);
	mat=A_R.getBlock();
	for(int s=0; s<d; s++)
	for(int i=0; i<del_D;i++)
	for(int j=0; j<D; j++)mat.at(CTYPE, (s*D_tilde+D)*D_tilde+i*D_tilde+j )=V_deg.at(CTYPE, s*(del_D*D)+(i)*D+j);
	A_R.putBlock(mat);
	A_R.permute(1);
	
	mat=C.getBlock();
	mat.resize(D_tilde, D_tilde);
	Bond bdi_D_tilde(BD_IN, D_tilde); Bond bdo_D_tilde(BD_OUT, D_tilde);
	bonds={bdi_D_tilde, bdo_D_tilde};
	UniTensor C_expand(CTYPE, bonds);
	bonds.clear();
	C_expand.putBlock(mat);
	C=C_expand;
	A_R.permute(2);
	mat=A_R.getBlock()*C.getBlock();
	UniTensor AC_temp(CTYPE, A_R.bond());
	AC_temp.putBlock(mat);
	AC_temp.permute(1);
	A_C=AC_temp;
	A_R.permute(1);
	A_L.setLabel(labels_A);
	A_R.setLabel(labels_A);
	A_C.setLabel(labels_A);
	
return;
}

void A_expand(UniTensor& A, int D_tilde){
	int D=A.bond(1).dim(); int d=A.bond(0).dim();
	Matrix A_mat;
	int label_A[]={0, 1, 2};
	vector<Bond> bonds;
	Bond bd1(BD_IN, D), bd2(BD_OUT, D_tilde), bd3(BD_IN, D_tilde);
	bonds.push_back(A.bond(0)); bonds.push_back(bd1); bonds.push_back(bd2);
	UniTensor A_Temp2(CTYPE, bonds);
	bonds.clear();
	A.permute(2);
	A_mat=A.getBlock();
	A_mat.resize(d*D, D_tilde); A_Temp2.putBlock(A_mat);
	int A_Temp_labels[]={0, 2, 1};
	A_Temp2.permute(A_Temp_labels, 2);
	A_mat=A_Temp2.getBlock();
	A_mat.resize(d*D_tilde, D_tilde);
	bonds.clear();
	bonds.push_back(A.bond(0)); bonds.push_back(bd3), bonds.push_back(bd2);
	UniTensor A_Temp3(CTYPE, bonds);
	bonds.clear();
	A_Temp3.putBlock(A_mat);
	A_Temp3.permute(A_Temp_labels, 1);A_Temp3.setLabel(label_A);
	A=A_Temp3;
return;
}

UniTensor A_sym_fix(UniTensor A_L, UniTensor C){
	vector<Matrix> Eigs=C.getBlock().eig();
	for(int i=0; i<C.bond(0).dim(); i++){
		Eigs.at(0).at(CTYPE, i)=pow(Eigs.at(0).at(CTYPE, i), 0.5);
	}
	Matrix P_inv=Eigs.at(1).inverse();
	Matrix C_root_mat=P_inv*Eigs.at(0)*Eigs.at(1);
	for(int i=0; i<C.bond(0).dim(); i++){
		Eigs.at(0).at(CTYPE, i)=pow(Eigs.at(0).at(CTYPE, i), -1.0);
	}
	Matrix C_root_inv_mat=P_inv*Eigs.at(0)*Eigs.at(1);
	UniTensor C_root(CTYPE, C.bond()), C_root_inv(CTYPE, C.bond());
	C_root.putBlock(C_root_mat); C_root_inv.putBlock(C_root_inv_mat);
	int labels_C_root[]={2, 3}; C_root.setLabel(labels_C_root);
	int labels_C_root_inv[]={-1, 1}; C_root_inv.setLabel(labels_C_root_inv);
	UniTensor A=contract(A_L, C_root); A=contract(A, C_root_inv);
	int per[]={0, -1, 3}; A.permute(per, 1);
	int labels_A[]={0, 1, 2};
	A.setLabel(labels_A);
return A;
}


string f_name(string typ, int& D, double& g, double& m){
	string s;
	stringstream ss(s);
	if(typ=="dir"){
		ss<<"mkdir ";
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m;
	}
	if(typ=="data"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/data";
	}
	if(typ=="A_L"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/"<<"AL_D"<<D<<"g"<<g<<"m"<<m<<"tens";
	}
	if(typ=="C"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/"<<"C_D"<<D<<"g"<<g<<"m"<<m<<"tens";
	}
	if(typ=="others"){
		ss<<"%DATA_DIR/XXZ_data/GS_tens/D"<<D<<"/D"<<D<<"g"<<g<<"m"<<m<<"/";
	}
	else{;}
return ss.str();
}

string contract_str(string s1, string s2){
	string s; stringstream ss(s);
	ss<<s1<<s2;
return ss.str();
}


void output_fun(int& D,double& g,double& m,double& e,double& s,double& Cc,double& Sz,double& err1_v,double& err2_v, UniTensor& A_L, UniTensor& C){
	string s_dir;
	stringstream ss(s_dir);
	ss<<"mkdir %DATA_DIR/XXZ_data/GS_tens/D"<<D;
	system(ss.str().c_str());

	char dir_char[200];
	g=abs(g)<1.0e-5?0:g;
	strcpy(dir_char, f_name("dir", D, g, m).c_str());
	system(dir_char);
	ofstream output_data;
	output_data.open(f_name("data", D, g, m));
	output_data<<"(D, g, m)= "<<setprecision(3)<<"("<<D<<","<<g<<","<<m<<")"<<endl;
	output_data<<"Energy, entropy, condensate, <Sz>, error, error="<<endl;
	output_data<<setprecision(15)<<e<<endl<<s<<endl<<Cc<<endl<<Sz<<endl<<err1_v<<endl<<err2_v;
	output_data.close();
	A_L.save(f_name("A_L", D,g,m));
	C.save(f_name("C", D,g,m));
return;
}

void show_setting( double a, double lambda,  double del_g,  double m, double h, double S_tar, double D){
        for(int i=0; i<80; i++)cout<<"=";
        cout<<endl<<"Algorithm:VUMPS\n"<<endl;
        cout<<"\n*****************Parameters*****************"<<endl;
        cout<<"Bond dimension D= "<<D<<endl;
        cout<<"Delta (anisotropy) del_g:"<<del_g<<endl;
        cout<<"Mass (stagger field)m:"<<m<<endl;
        cout<<"Magnetic field h:"<<h<<endl;
        cout<<"Penalty term (stagger field) lambda:"<<lambda<<endl;
        cout<<"\n******************Accuracy******************"<<endl;
        cout<<"The tolerance of Lanczos method: "<<setprecision(3)<<Lanc_acc<<endl;
        cout<<"The maximum iteration for Lanczos method: "<<MaxLanIter<<endl;
        cout<<"The tolerance of Arnoli method: "<<setprecision(3)<<Arnacc<<endl;
        cout<<"The maximum iteration for Arnoldi method: "<<Arnstep_max<<endl;
        cout<<"The tolerance of BiCGstab method: "<<setprecision(3)<<bicgstab_acc<<endl;;
        cout<<"The maximum iteration for BiCGstab method: "<<bicg_step_max<<endl;
        for(int i=0; i<80; i++)cout<<"=";
        cout<<"\n\n";

return;
}

